package com.mathpar.parallel.dap.test;

import com.mathpar.NAUKMA.masters_1.Morenets.TriangularInversionTools;
import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;

import java.io.IOException;

import mpi.MPI;
import mpi.MPIException;

/**
 * @author maria
 */
public class InversionTestManual {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(InversionTestManual.class);

    public static void main(String[] args) throws MPIException,
            InterruptedException,
            IOException,
            ClassNotFoundException {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();

        int n = 16;
        int task = 3;
        int numberOfMatrixes = 50;
        Ring ring = new Ring("Z[]");

        DispThread disp = new DispThread(1, args, ring);

        for (int i = 0; i < numberOfMatrixes; i++) {

            MatrixS matrix = null;
            if (rank == 0)
                matrix = TriangularInversionTools.generateMatrix(n, 100, 5, ring);
            LOGGER.info("Matrix for test = " + matrix);

            Element[] init = new MatrixS[]{matrix};

            disp.execute(task, args, init, ring);

            if (rank == 0) {
                MatrixS res1 = matrix.inverse(ring);
                MatrixS res2 = (MatrixS) disp.getResult()[0];
                LOGGER.info("Test number " + i + " --- " + res1.subtract(res2, ring).isZero(ring));
            }
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();

        MPI.Finalize();
    }

}
