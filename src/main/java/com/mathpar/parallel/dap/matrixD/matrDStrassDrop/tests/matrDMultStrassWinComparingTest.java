package com.mathpar.parallel.dap.matrixD.matrDStrassDrop.tests;

import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

public class matrDMultStrassWinComparingTest {
    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        final int n = Integer.parseInt(args[0]);
        final int maxRandomValue = 100;
        final int dropType = 25;
        Ring ring = new Ring("R[]");
        DispThread disp = new DispThread(1, args, ring);

        disp.execute(dropType, args, new MatrixD[]{getRandomMatrix(n, 1, maxRandomValue, ring),
                getRandomMatrix(n, 3, maxRandomValue, ring)}, ring);

        if (rank == 0) {
            MatrixD result = (MatrixD) disp.getResult()[0];
            System.out.println("N=" + n);
            System.out.println("Result[0][0]=");
            System.out.println(result.M[0][0]);
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();
        MPI.Finalize();
    }

    private static MatrixD getRandomMatrix(int n, int randomSeed, int maxValue, Ring r) {
        Element one = Ring.oneOfType(r.algebra[0]);
        Element[][] matr = new Element[n][n];
        Random rand = new Random(randomSeed);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matr[i][j] = one.valOf(rand.nextBoolean() ? rand.nextInt(maxValue) : -rand.nextInt(maxValue), r);
            }
        }

        return new MatrixD(matr);
    }
}
