package com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.tests.java;

import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.matrDMultStrassWin;
import com.mathpar.parallel.dap.matrixD.matrDStrassDrop.tests.SequentialAlgorithmsTest;

public class matrDMultStrassWinTestSq {

    static long test(MatrixD a, MatrixD b, Ring ring) {
        long start_ms = System.currentTimeMillis();
        start_ms = System.currentTimeMillis();
        MatrixD res_def = matrDMultStrassWin.multiply_sequential(a, b, ring);
        long end_ms = System.currentTimeMillis();

        System.out.printf("ms time %6d | ", end_ms - start_ms);
        return  end_ms - start_ms;
    }

    static long test_2(MatrixD a, MatrixD b, Ring ring) {
        long start_def = System.currentTimeMillis();
//        MatrixD res_ms = Multiply_v8.multiply_func(a, b, ring);
        MatrixD res_ms = a.multiplyMatr(b, ring);
        long end_def = System.currentTimeMillis();
        System.out.printf("def time %8d\n", end_def - start_def);
        return  end_def - start_def;
    }

    static void compare_multiply_algorithms() {
        Ring ring = new Ring("R[]");
        int size = 64;
        long s1 = 0;
        long s2 = 0;
        do {
            size *= 2;
            s1 = 0;
            s2 = 0;
            MatrixD a = new MatrixD(size, size, 10, ring);
            MatrixD b = new MatrixD(size, size, 10, ring);
            for (int i = 0; i < 10; ++i) {
                System.out.printf("size %4d : ", size);
                test(a, b, ring);
                test_2(a, b, ring);
            }
            for (int i = 0; i < 100; ++i) {
                System.out.printf("size %4d : ", size);
                s1 += test(a, b, ring);
                s2 += test_2(a, b, ring);
            }
            System.out.printf("s1 : %d  s2 : %d\n", s1, s2);
        } while (s1 > s2);
        System.out.println("size = " + size);
    }

    public static void average_time_Vynograd() {
        int[] sizes = new int[] { 64, 128, 256, 512 };
        Ring ring = new Ring("R[]");
        for (int i = 0; i < sizes.length; ++i) {
            int size = sizes[i];
            MatrixD a = new MatrixD(size, size, 10, ring);
            MatrixD b = new MatrixD(size, size, 10, ring);
            long sum = 0;
            for (int j = 0; j < 10; ++j) {
                long start_ms = System.currentTimeMillis();
                start_ms = System.currentTimeMillis();
                MatrixD res_def = matrDMultStrassWin.multiply_func(a, b, ring);
                long end_ms = System.currentTimeMillis();
                sum += end_ms - start_ms;
            }
            System.out.printf("size %4d : average time %6.2f ms\n", size, sum / 10.0);
        }
    }

    public static void average_time_Strassen() {
        int[] sizes = new int[] { 64, 128, 256, 512 };
        Ring ring = new Ring("R[]");
        for (int i = 0; i < sizes.length; ++i) {
            int size = sizes[i];
            MatrixD a = new MatrixD(size, size, 10, ring);
            MatrixD b = new MatrixD(size, size, 10, ring);
            long sum = 0;
            for (int j = 0; j < 10; ++j) {
                long start_ms = System.currentTimeMillis();
                start_ms = System.currentTimeMillis();
                MatrixD res_def = matrDMultStrassWin.multiply_func(a, b, ring);
                long end_ms = System.currentTimeMillis();
                sum += end_ms - start_ms;
            }
            System.out.printf("size %4d : average time %6.2f ms\n", size, sum / 10.0);
        }
    }

    public static void average_time_Mult() {
        int[] sizes = new int[] { 64, 128, 256, 512 };
        Ring ring = new Ring("R[]");
        for (int i = 0; i < sizes.length; ++i) {
            int size = sizes[i];
            MatrixD a = new MatrixD(size, size, 10, ring);
            MatrixD b = new MatrixD(size, size, 10, ring);
            long sum = 0;
            for (int j = 0; j < 10; ++j) {
                long start_ms = System.currentTimeMillis();
                start_ms = System.currentTimeMillis();
                MatrixD res_def = matrDMultStrassWin.multiply_func(a, b, ring);
                long end_ms = System.currentTimeMillis();
                sum += end_ms - start_ms;
            }
            System.out.printf("size %4d : average time %6.2f ms\n", size, sum / 10.0);
        }
    }

    public static void compare_sq_multyplies() {
        int[] sizes = new int[] { 512, 1024 };
        Ring ring = new Ring("R[]");
        for (int i = 0; i < sizes.length; ++i) {
            int size = sizes[i];
            MatrixD a = new MatrixD(size, size, 10, ring);
            MatrixD b = new MatrixD(size, size, 10, ring);
            long sum_default = 0;
            long sum_strass = 0;
            long sum_vynograd = 0;
            for (int j = 0; j < 5; ++j) {
                long start_ms = System.currentTimeMillis();
                start_ms = System.currentTimeMillis();
//                a.multCU(b, ring);
                long end_ms = System.currentTimeMillis();
                sum_default += end_ms - start_ms;

                start_ms = System.currentTimeMillis();
                SequentialAlgorithmsTest.multStrassen(a, b, ring);
                end_ms = System.currentTimeMillis();
                sum_strass += end_ms - start_ms;

                start_ms = System.currentTimeMillis();
//                matrDMultStrassWin.multiply_func(a, b, ring);
                end_ms = System.currentTimeMillis();
                sum_vynograd += end_ms - start_ms;
            }
            System.out.printf("size %4d : average time def %6.2f ms | strassen %6.2f ms | vynograd %6.2f ms\n", size,
                    sum_default / 10.0, sum_strass / 10.0,sum_vynograd / 10.0);
        }
    }

    public static void main(String[] args) {
        compare_sq_multyplies();
    }

}
