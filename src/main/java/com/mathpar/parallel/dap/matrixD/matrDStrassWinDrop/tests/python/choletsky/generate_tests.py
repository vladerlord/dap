import json
import argparse

if __name__ == "__main__":
    class_ = 'matrDCholFactStrassWin'
    test_obj = 'matrDStrassWinDrop/matrDCholFactStrassWin'
    proc = [4]
    matrix_size = [1024]
    leaf_size = [64]
    id = 1
    tests = []
    timeout = [720, 720, 720, 720, 720, 720, 720, 720, 720, ]

    for i in range(len(matrix_size)):
        for j in range(len(leaf_size)):
            for k in range(len(proc)):
                tests.append( { 'proc_count' : proc[k], 'name' : 'test_case_' + str(id), 'matrix_size' : matrix_size[i], 'leaf_size' : leaf_size[j], 'timeout' : timeout[i] } )
                id += 1

    data = { 'class_name' : class_, 'proc_count' : proc, 'test_obj' : test_obj, 'test_cases' : tests }
    json.dump(data, open('cholesky_tests.json', 'w'), indent=4)