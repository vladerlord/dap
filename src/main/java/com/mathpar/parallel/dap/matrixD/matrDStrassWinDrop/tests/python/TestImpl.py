import json
import os
import datetime
import subprocess
import shutil

class TestImplementation(object):

    def __init__(self):
        super().__init__()
        try:
            config = TestImplementation._read_user_config()
        except:
            TestImplementation._init_user_data()
            config = TestImplementation._read_user_config()

        self.tester_name = config["tester_name"]
        self.path_to_dap = config["path_to_dap"]
        self.init_test_time = TestImplementation.current_time()

            
    @staticmethod
    def _init_user_data():
        conf_file = open(os.environ["HOME"] + '/java_tests_user_conf.json', 'w', encoding='utf-8')
        path_to_dap = input('path to dap project\nexample : /home/kolya/workspace/dap\n')
        tester_name = input('Tester name : ')

        data = { "path_to_dap" : path_to_dap, "tester_name" : tester_name }
        json.dump(data, conf_file, indent=2)

    @staticmethod
    def _read_user_config():
        conf_file = open(os.environ["HOME"] + '/java_tests_user_conf.json', 'r', encoding='utf-8')
        return json.load(conf_file)
    
    @staticmethod
    def current_time() -> str:
        return datetime.datetime.now().strftime("%d/%m/%Y %H:%M:%S")

    def run_test(self, class_name, test_case_name, test_case_data, timeout=300):
        try:
            print('[{}] running test {}:{}'.format(TestImplementation.current_time(), class_name, test_case_name))
            base_dir = os.path.abspath('.')
            os.chdir(self.path_to_dap + '/src/main/java')
            p = subprocess.Popen(test_case_data, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate(timeout=timeout)
            out = out.decode()
            err = err.decode()
            if err:
                print('Error:', err)
            print('[{}] test {}:{} finished successfully'.format(TestImplementation.current_time(), class_name, test_case_name))
            os.chdir(base_dir)
            return out
        except subprocess.TimeoutExpired:
            p.kill()
            out, err = p.communicate()
            out = out.decode()
            print(out)
            err = err.decode()
            print(out)
            print('[{}] test {}:{} terminated'.format(TestImplementation.current_time(), class_name, test_case_name))
            os.chdir(base_dir)
            return None
        except KeyboardInterrupt:
            p.kill()
            out, _ = p.communicate()
            out = out.decode()
            print(out)
            print('[{}] test {}:{} interrupted'.format(TestImplementation.current_time(), class_name, test_case_name))
            os.chdir(base_dir)
            return 'interrupted'
        except Exception as e:
            print('[{}] exception ocurred during test {}:{}'.format(TestImplementation.current_time(), class_name, test_case_name))
            print(e)
            os.chdir(base_dir)
            return None

    def run(self, test_run_count=10, output_file_name='test_run_results.json'):
        try:
            class_name = self.test_config["class_name"]
            test_obj = self.test_config["test_obj"]
            base_cmd = """mpirun -hostfile hostfile -np $proc_count java -cp 
            $path_to_dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar 
            com.mathpar.NAUKMA/""" + test_obj
            run_results = []
            try:
                os.mkdir('logs')
            except:
                pass
            for test_case in self.test_config["test_cases"]:
                matrix_size = test_case['matrix_size']
                leaf_size = test_case['leaf_size']
                timeout = test_case['timeout']
                proc_count = test_case["proc_count"]
                cmd = base_cmd.replace('$HOME', os.environ['HOME']).replace('$proc_count', str(proc_count)).replace('$path_to_dap', self.path_to_dap)
                cmd = "{} -size {} -leaf {}".format(cmd, matrix_size, leaf_size).split()
                time_results = []
                min_time = 10000000000
                max_time = 0
                sum_time = 0
                run_id = 0
                while run_id < test_run_count:
                    run_id += 1
                    print('[{}] running test {}:{} [{}/{}]'.format(TestImplementation.current_time(), class_name, test_case['name'], str(len(time_results)+1), str(test_run_count)))
                    out = self.run_test(class_name, test_case['name'], cmd, timeout)
                    status = 'failed'
                    if out is not None:
                        if out == 'interrupted':
                            break
                        log_file_name = 'logs/{}_{}_{}_{}_{}.txt'.format(class_name, matrix_size, leaf_size, proc_count, str(run_id))
                        f = open(log_file_name, 'w', encoding='utf-8')
                        f.writelines(out)
                        f.close()
                        for line in out.splitlines():
                            if 'Rank[0] time elapsed : ' in line:
                                time = int(line.split(' : ')[-1])
                                time_results.append(time)
                                sum_time += time
                                if time < min_time:
                                    min_time = time
                                elif time > max_time:
                                    max_time = time
                else:
                    status = 'ok'
                if len(time_results) > 0:
                    avarage_time = sum_time / test_run_count
                    run_results.append(
                        { 
                            'test' : '{}:{}'.format(class_name, test_case['name']),
                            'proc_count' : proc_count,
                            'matrix_size' : matrix_size, 
                            'leaf_size' : leaf_size, 
                            'avarage_time' : avarage_time,
                            'best_time' : min_time,
                            'worst_time' : max_time,
                            'all_time_results' :  time_results
                        })
                if status != 'ok':
                    break
        except Exception as e:
            print(e)
        finally:
            out_data = { 'tester_name' : self.tester_name, 'tests_started' : self.init_test_time, 'tests_finished' : TestImplementation.current_time(),'test_results' : run_results }
            json.dump(out_data, open(output_file_name, 'w', encoding='utf-8'), indent=5)
            print('results saved')


if __name__ == "__main__":
    cmd = ['ping', '8.8.8.8']
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
    while True:
        try:
            out, err = p.communicate(timeout=5)
        except subprocess.TimeoutExpired:
            p.kill()
            out, err = p.communicate()
            out = out.decode()
            print(out)
            break