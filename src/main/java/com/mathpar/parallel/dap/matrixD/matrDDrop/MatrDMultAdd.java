package com.mathpar.parallel.dap.matrixD.matrDDrop;

import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;

public class MatrDMultAdd extends MatrDMult
{
    public static int Drop_Number = 11;
    private static int inputMatrixSize = 9;

    private static int[][] Matrix_Arcs = new int[][]
    {
        {
            1, 0, 0,   1, 4, 1,   2, 1, 0,   2, 6, 1,   3, 0, 0,   3, 5, 1,   4, 1, 0,   4, 7, 1,
            5, 2, 0,   5, 4, 1,   6, 3, 0,   6, 6, 1,   7, 2, 0,   7, 5, 1,   8, 3, 0,   8, 7, 1,
            9, 8, 4
        },
        {2, 0, 2}, {9, 0, 0}, {4, 0, 2}, {9, 0, 1},
        {6, 0, 2}, {9, 0, 2}, {8, 0, 2}, {9, 0, 3},
        {}
    };

    public MatrDMultAdd()
    {
        inData = new Element[3];
        outData = new Element[1];

        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 16;

        number = cnum++;
        type = Drop_Number;

        resultForOutFunctionLength = 5;
        inputDataLength = 3;

        arcs = Matrix_Arcs;
    }

    @Override
    public void sequentialCalc(Ring ring)
    {
        outData[0] = inData[0].multiply(inData[1], ring).add(inData[2], ring);
    }
    @Override
    public Element[] inputFunction(Element[] input, int inputKey, Ring ring)
    {
        MatrixD[] resultM = new MatrixD[inputMatrixSize];
        if (inputKey == 3)
        {
            resultM[8] = (MatrixD) input[0];
        }
        else
        {
            MatrixD [] m0 = ((MatrixD) input[0]).splitTo4();
            MatrixD [] m1 = ((MatrixD) input[1]).splitTo4();

            System.arraycopy(m0, 0, resultM, 0, m0.length);
            System.arraycopy(m1, 0, resultM, m0.length, m1.length);
            //Tools.concatTwoArrays(m0, m1, resultM);

            if (inputKey == 2)
            {
                resultM[8] = null;
            }
            if (inputKey == 1)
            {
                resultM[8] = (MatrixD) input[2];
            }
        }
        return resultM;
    }

    @Override
    public Element[] outputFunction(Element[] input, Ring ring)
    {
        MatrixD[] resultM = new MatrixD[input.length];
        for (int i = 0; i < input.length; i++)
        {
            resultM[i] = (MatrixD) input[i];
        }

        return new MatrixD[] {MatrixD.joinMatr(resultM).add((MatrixD)input[4], ring)};
    }
}
