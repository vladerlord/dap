package com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.tests.java;

import com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.ParserMatrix;
import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;

public class ParallelInverseDefTest {

    private final static MpiLogger LOGGER = MpiLogger.getLogger(ParallelInverseDefTest.class);

    /*
    mpirun -hostfile hostfile -np 2 java -cp \
    $HOME/workspace/new/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar \
    com.mathpar.NAUKMA/matrDStrassWinDrop/tests/java/ParallelStrassenTest -size 128
    */

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException, Exception {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int n = 512;
        int task = 28;
        long start = 0;

        for (int i = 0; i < args.length; i += 2) {
            if (args[i].equals("-size")) {
                n = Integer.parseInt(args[i+1]);
            }
        }
        LOGGER.info("size = " + n);

        Ring ring = new Ring("R[]");
        MatrixD res = null;
        DispThread disp = new DispThread(1, args, ring);
        MatrixD a = null;
        MatrixD b = null;
        Element[] init = {};
        if (rank == 0) {
            a = ParserMatrix.read_matrixD("matrixA.txt", n);
            b = ParserMatrix.read_matrixD("matrixB.txt", n);
            init = new MatrixD[] { a, b };
            start = System.currentTimeMillis();
            LOGGER.info("a size " + a.M.length);
        }

        disp.execute(task, args, init, ring);

        if (rank == 0) {
            res = (MatrixD) disp.getResult()[0];
            long timeElapsed = System.currentTimeMillis() - start;
            LOGGER.info("ParallelInverseDefTest time elapsed : " + timeElapsed + " matrix size : " + n);
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();
        MPI.Finalize();
    }

}
