package com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop;

import com.mathpar.func.FuncNumberR;
import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberR;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.core.DropTask;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class matrDCholFactStrassWin extends DropTask {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(matrDCholFactStrassWin.class);
    private static int leafSize = 256;

    private static int[][] _arcs = {
            {1, 0, 0, 2, 1, 1, 3, 2, 1}, // input function
            {7, 0, 0, 2, 1, 0, 5, 1, 1, 7, 1, 3},
            {3, 0, 0},
            {4, 0, 0, 7, 1, 1, 5, 1, 0},
            {7, 0, 2, 7, 1, 5, 6, 1, 0},
            {6, 0, 1},
            {7, 0, 4},
            {} // output function
    };

    public matrDCholFactStrassWin() {
        inData = new Element[1];
        outData = new Element[2];
        numberOfMainComponents = 1;
        numberOfMainComponentsAtOutput = 3;
        type = 16;
        resultForOutFunctionLength = 6;
        inputDataLength = 1;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public ArrayList<DropTask> doAmin() {
        ArrayList<DropTask> amin = new ArrayList<DropTask>();
        amin.add(new matrDCholFactStrassWin());
        amin.add(new matrDMultStrassWin());
        amin.add(new matrDMultStrassWinSub()); ///
        amin.add(new matrDCholFactStrassWin());
        amin.add(new matrDMultStrassWin());
        amin.add(new matrDMultStrassWinNegate());
        return amin;
    }

    public static MatrixD[] CholeskyDecomp(MatrixD A, Ring ring) {
        if (A.M.length == 1) {
            Element v = A.getElement(0, 0);
            if (v.isNegative()) {
                MatrixD zero = MatrixD.zeroMatrixD(1, 1, ring);
                return new MatrixD[] { zero, zero };
            }
            FuncNumberR fnr = new FuncNumberR(ring);
            Element sqrt_val = fnr.sqrt((NumberR) v);
            return new MatrixD[] { new MatrixD(new MatrixS(sqrt_val)),
                            new MatrixD(new MatrixS(ring.numberONE().divide(sqrt_val, ring)))};
        }
        // split a -> ( alpha, beta, 0, gamma)
        MatrixD[] splitted = A.splitTo4();
        // a a1 = CholeskyDecomp(alpha)
        MatrixD[] decomposedA = CholeskyDecomp(splitted[0], ring);
        // bt = a1 * beta
        MatrixD bT = matrDMultStrassWin.multiply_func(decomposedA[1], splitted[1], ring);
        // b = bt transp
        MatrixD b = bT.transpose(ring);
        // delta = gamma - b * bT
        MatrixD delta = (MatrixD) splitted[3].subtract(matrDMultStrassWin.multiply_func(b, bT, ring), ring);
        MatrixD[] decomposedC = CholeskyDecomp(delta, ring);
        MatrixD z = matrDMultStrassWin.multiply_func(matrDMultStrassWin.multiply_func(decomposedC[1].negate(ring), b, ring), decomposedA[1], ring);

        MatrixD[] LBlocks = new MatrixD[4];
        MatrixD[] LinvBlocks = new MatrixD[4];

        LBlocks[0] = decomposedA[0];
        LBlocks[1] = MatrixD.zeroMatrixD(splitted[0].M.length, splitted[0].M.length, ring);
        LBlocks[2] = b;
        LBlocks[3] = decomposedC[0];

        LinvBlocks[0] = decomposedA[1];
        LinvBlocks[1] = MatrixD.zeroMatrixD(splitted[0].M.length, splitted[0].M.length, ring);
        LinvBlocks[2] = z;
        LinvBlocks[3] = decomposedC[1];

        MatrixD L = MatrixD.joinMatr(LBlocks);
        MatrixD Linv = MatrixD.joinMatr(LinvBlocks);
        return new MatrixD[]{L, Linv};
    }

    @Override
    public void sequentialCalc(Ring r) {
        MatrixD A = (MatrixD) inData[0];
//        LOGGER.info("decomp seq " + A.M.length + " " + A.M[0].length);
        MatrixD[] arr = CholeskyDecomp(A, r);
        outData[0] = arr[0];
        outData[1] = arr[1];
    }

    @Override
    public MatrixD[] inputFunction(Element[] input, int inputKey, Ring ring) {
        MatrixD a = (MatrixD) input[0];
        MatrixD[] a_splitted = a.splitTo4();
//        LOGGER.info(a.M.length + " " + a.M[0].length);
        MatrixD[] input_m = new MatrixD[] {a_splitted[0], a_splitted[1], a_splitted[3]};
        return input_m;
    }

    @Override
    public MatrixD[] outputFunction(Element[] input, Ring r) {
        MatrixD a = (MatrixD) input[0];
        MatrixD b = (MatrixD) input[1];
        MatrixD c = (MatrixD) input[2];
        MatrixD a1 = (MatrixD) input[3];
        MatrixD z = (MatrixD) input[4];
        MatrixD c1 = (MatrixD) input[5];

        MatrixD L = MatrixD.joinMatr( new MatrixD[] {a, MatrixD.zeroMatrixD(a.M.length, a.M.length, r), b, c} );
        MatrixD L1 = MatrixD.joinMatr( new MatrixD[] {a1, MatrixD.zeroMatrixD(a.M.length, a.M.length, r), z, c1} );
        
        return new MatrixD[] {L, L1};
    }

    @Override
    public boolean isItLeaf() {
        MatrixD m = (MatrixD) inData[0];
        return  m.M.length <= leafSize;
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }

    public static void setLeafSizeStatic(int dataSize) {
        leafSize = dataSize;
    }

    public static long[][] random_matrix(int n) {
        long[][] arr = new long[n][n];
        Random r = new Random();
        for (int i = 0; i < n; ++i)
            for (int j = 0; j < n; ++j)
                arr[i][j] = r.nextInt(10) + 1;
        return arr;
    }

    public static long[][] positive_matrix(int size) {
        Random random = new Random();
        long[][] m = new long[size][size];
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                m[i][j] = random.nextInt(50) + 1;
            }
        }
        return m;
    }

    /*
    time mpirun -hostfile hostfile -np 2 java -cp \
    $HOME/workspace/new/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar\
    com.mathpar.NAUKMA/v8_2020/semylitko/matrDCholFactStrassWin
     */

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException, Exception {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        // temporary values
        int n = 128;
        int task = 29;
        long start = 0;

        for (int i = 0; i < args.length; i += 2) {
            if (args[i].equals("-size")) {
                n = Integer.parseInt(args[i+1]);
            } else if (args[i].equals("-leaf")) {
                leafSize = Integer.parseInt(args[i+1]);
                matrDCholFactStrassWin.setLeafSizeStatic(Integer.parseInt(args[i+1]));
            }
        }
        LOGGER.info("n = " + n + " leaf = " + leafSize);

        Ring ring = new Ring("R[]");
        MatrixD L = null;
        MatrixD L1 = null;
        DispThread disp = new DispThread(1, args, ring);
        Element[] init = {};

        MatrixD a = null;
        if (rank == 0) {
            a = new MatrixD(positive_matrix(n), ring);
            init = new MatrixD[] { a };
            if (a.M.length <= 16) {
                matrDMultStrassWin.print_matrix(a);
            }
            start = System.currentTimeMillis();
        }

        disp.execute(task, args, init, ring);

        if (rank == 0) {
            L = (MatrixD) disp.getResult()[0];
            L1 = (MatrixD) disp.getResult()[1];
            long timeElapsed = System.currentTimeMillis() - start;
            LOGGER.info("time elapsed : " + timeElapsed);
            if (L.M.length <= 16) {
                matrDMultStrassWin.print_matrix(L);
                matrDMultStrassWin.print_matrix(L1);
            }
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();
        MPI.Finalize();
    }

}
