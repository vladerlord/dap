package com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.core.DropTask;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class matrDTriangInvStrassWin extends DropTask {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(matrDTriangInvStrassWin.class);
    private static int leafSize = 16;

    private static int[][] _arcs = {
            {1, 0, 0, 3, 1, 0, 2, 2, 0}, // input function
            {3, 0, 1, 5, 0, 0},
            {4, 0, 0, 5, 0, 2},
            {4, 0, 1},
            {5, 0, 1},
            {} // output function
    };

    public matrDTriangInvStrassWin() {
        inData = new Element[1];
        outData = new Element[1];
        numberOfMainComponents = 1;
        numberOfMainComponentsAtOutput = 3;
        type = 15;
        resultForOutFunctionLength = 3;
        inputDataLength = 1;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public ArrayList<DropTask> doAmin() {
        ArrayList<DropTask> amin = new ArrayList<DropTask>();
        amin.add(new matrDTriangInvStrassWin());
        amin.add(new matrDTriangInvStrassWin());
        amin.add(new matrDMultStrassWin());
        amin.add(new matrDMultStrassWinNegate());
        return amin;
    }

    public static MatrixD inverse_sequential(MatrixD m, Ring r) {
        if (m.M.length == 1) {
            return new MatrixD(new MatrixS(m.getElement(0, 0).inverse(r)));
        }

        MatrixD[] splitted = m.splitTo4();
        MatrixD inv_a = inverse_sequential(splitted[0], r);
        MatrixD inv_d = inverse_sequential(splitted[3], r);
        MatrixD inv_c_complex = matrDMultStrassWin.multiply_func(matrDMultStrassWin.multiply_func(inv_d.negate(r), splitted[2], r), inv_a, r);

        return MatrixD.joinMatr( new MatrixD[] { inv_a, MatrixD.zeroMatrixD(inv_a.M.length, inv_a.M.length, r), inv_c_complex, inv_d } );
    }

    @Override
    public void sequentialCalc(Ring r) {
        MatrixD m = (MatrixD)inData[0];
        outData[0] = inverse_sequential(m, r);
    }

    @Override
    public MatrixD[] inputFunction(Element[] input, int inputKey, Ring ring) {
        MatrixD a = (MatrixD) input[0];
//        LOGGER.info(a.M.length + " " + a.M[0].length);
        MatrixD[] a_splitted = a.splitTo4();
        MatrixD[] input_m = new MatrixD[] {a_splitted[0], a_splitted[2], a_splitted[3]};
//        LOGGER.info(a_splitted[0].M.length + " " + a_splitted[0].M[0].length + " " +
//                a_splitted[2].M.length + " " + a_splitted[2].M[0].length + " " +
//                a_splitted[3].M.length + " " + a_splitted[3].M[0].length);
        return input_m;
    }

    @Override
    public MatrixD[] outputFunction(Element[] input, Ring r) {
        MatrixD[] input_m = new MatrixD[input.length];
        for (int i = 0; i < input.length; ++i) {
            input_m[i] = (MatrixD) input[i];
        }

        MatrixD[] C_matrix = new MatrixD[] {
                input_m[0], MatrixD.zeroMatrixD(input_m[0].M.length, input_m[0].M.length, r), input_m[1], input_m[2]
        };

        return new MatrixD[] { MatrixD.joinMatr(C_matrix) };
    }

    @Override
    public boolean isItLeaf() {
        MatrixD m = (MatrixD) inData[0];
        return  m.M.length <= leafSize;
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }

    public static void setLeafSizeStatic(int dataSize) {
        leafSize = dataSize;
    }

    public static long[][] triangle_matrix(int size) {
        Random random = new Random();
        long[][] m = new long[size][size];
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j <= i; ++j) {
                m[i][j] = random.nextInt(50) + 1;
            }
            for (int j = i + 1; j < size; ++j) {
                m[i][j] = 0;
            }
        }
        return m;
    }
    /*
       mpirun -hostfile hostfile -np 2 java -cp \
       $HOME/workspace/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar\
       com.mathpar.NAUKMA/v8_2020/benyukh/matrDTriangInvStrassWin
        */

     /*
       time mpirun -hostfile hostfile -np 3 java -cp \
       $HOME/workspace/new/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar\
       com.mathpar.NAUKMA/v8_2020/benyukh/matrDTriangInvStrassWin
      */

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException, Exception {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        // temporary values
        int n = 128;
        int task = 28;
        long start = 0;


        for (int i = 0; i < args.length; i += 2) {
            if (args[i].equals("-size")) {
                n = Integer.parseInt(args[i+1]);
            } else if (args[i].equals("-leaf")) {
                matrDTriangInvStrassWin.setLeafSizeStatic(Integer.parseInt(args[i+1]));
            }
        }
        LOGGER.info("n = " + n + " leaf = " + leafSize);


        Ring ring = new Ring("R[]");
        MatrixD res = null;
        DispThread disp = new DispThread(1, args, ring);
        Element[] init = {};

        MatrixD a = null;
        if (rank == 0) {
            a = new MatrixD(triangle_matrix(n), ring);
            init = new MatrixD[] { a };
            if (a.M.length <= 16) {
                matrDMultStrassWin.print_matrix(a);
            }
            start = System.currentTimeMillis();
        }

        disp.execute(task, args, init, ring);

        if (rank == 0) {
            res = (MatrixD) disp.getResult()[0];
            long timeElapsed = System.currentTimeMillis() - start;
            LOGGER.info("time elapsed : " + timeElapsed);
            if (a.M.length <= 16) {
                matrDMultStrassWin.print_matrix(res);
            }
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();
        MPI.Finalize();
    }

}
