package com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.util.Scanner;

import com.mathpar.matrix.MatrixD;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Ring;

public class ParserMatrix {
    public static MatrixD read_matrixD(String filename, int size) throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader(filename)));
        int rows = size;
        int columns = size;
        long [][] myArray = new long[rows][columns];
        while(sc.hasNextLine()) {
            for (int i=0; i<myArray.length; i++) {
                String[] line = sc.nextLine().trim().split(" ");
                for (int j=0; j<line.length; j++) {
                    myArray[i][j] = Integer.parseInt(line[j]);
                }
            }
        }
        return new MatrixD(myArray, new Ring("R[]"));
    }

    public static MatrixS read_matrixS(String filename, int size) throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader(filename)));
        int rows = size;
        int columns = size;
        long [][] myArray = new long[rows][columns];
        while(sc.hasNextLine()) {
            for (int i=0; i<myArray.length; i++) {
                String[] line = sc.nextLine().trim().split(" ");
                for (int j=0; j<line.length; j++) {
                    myArray[i][j] = Integer.parseInt(line[j]);
                }
            }
        }
        return new MatrixS(myArray, new Ring("R[]"));
    }

}
