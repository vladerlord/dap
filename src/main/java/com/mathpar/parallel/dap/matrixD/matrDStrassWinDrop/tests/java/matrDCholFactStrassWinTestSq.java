package com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.tests.java;
import com.mathpar.matrix.MatrixD;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.matrDCholFactStrassWin;


import java.util.Random;

public class matrDCholFactStrassWinTestSq {

    public static int[][] generate_matrix(int size, int s) {
        Random r = new Random();
        int[][] out = new int[size][size];
        for (int i = 0; i < size; ++i) {
            for (int j = 0; j < size; ++j) {
                int val = r.nextInt(100);
                if (val > s && i > j) {
                    out[i][j] = 0;
                } else {
                    out[i][j] = r.nextInt(100) + 1;
                }
            }
        }
        return out;
    }

    public static void main(String[] args) throws InterruptedException {
        int[] sizes = {64, 128, 256, 512, 1024};
        int[] dens = { 100, 50, 25, 10, 5 };
        Ring ring = new Ring("R[]");
        long start_ms = System.currentTimeMillis();
        Thread.sleep(1000, 0);
        long end_ms = System.currentTimeMillis();
        long start_def = System.currentTimeMillis();
        long end_def = System.currentTimeMillis();
        System.out.printf("time elapsed %d\n", end_ms - start_ms);
        for (int i = 0; i < sizes.length; ++i) {
            for (int j = 0; j < dens.length; ++j) {
                MatrixS input_s = new MatrixS(generate_matrix(sizes[i], dens[j]), ring);
                MatrixD input_marix = new MatrixD(input_s, ring);
//                matrDMultStrassWin.print_matrix(input_marix);
                start_def = System.currentTimeMillis();
 //               matrSoCholFactStrass.choleskyDecomp(input_s, ring);
                end_def = System.currentTimeMillis();
                start_ms = System.currentTimeMillis();
                matrDCholFactStrassWin.CholeskyDecomp(input_marix, ring);
                end_ms = System.currentTimeMillis();
                System.out.printf("size %5d dence %3d : time elapsed my %6d | other %6d\n", sizes[i], dens[j], end_ms - start_ms, end_def - start_def);
            }
        }
    }
}
