import json

if __name__ == "__main__":
    class_ = 'matrDMultStrassWin'
    test_obj = 'bitaeva/matrDMultStrassWin'
    proc = [2]
    matrix_size = [128]
    leaf_size = [16, 32]
    id = 1
    tests = []
    timeout = [15, 30, 60, 300]

    for i in range(len(matrix_size)):
        for j in range(len(leaf_size)):
            for k in range(len(proc)):
                tests.append( { 'proc_count' : proc[k], 'name' : 'test_case_' + str(id), 'matrix_size' : matrix_size[i], 'leaf_size' : leaf_size[j], 'timeout' : timeout[i] } )
                id += 1

    data = { 'class_name' : class_, 'proc_count' : proc, 'test_obj' : test_obj, 'test_cases' : tests }
    json.dump(data, open('multiply_tests.json', 'w'), indent=4)