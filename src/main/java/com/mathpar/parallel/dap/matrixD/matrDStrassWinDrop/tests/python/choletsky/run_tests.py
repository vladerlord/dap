import json
import os
import sys
sys.path.append(os.path.abspath('.'))
sys.path.append(os.path.abspath('..'))
from TestImpl import TestImplementation

class CholetskyTestImplementation(TestImplementation):

    def __init__(self, tests_config_file):
        super().__init__()
        self.test_config = CholetskyTestImplementation._read_tests_config(tests_config_file)

    @staticmethod
    def _read_tests_config(path_to_file):
        return json.load(open(path_to_file, 'r', encoding='utf-8'))

if __name__ == "__main__":
    inverseTests = CholetskyTestImplementation("cholesky_tests.json")
    inverseTests.run(1)
