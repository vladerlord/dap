package com.mathpar.parallel.dap.matrixD.matrDStrassDrop.tests;

import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Random;

public class SequentialAlgorithmsTest {
    // mpirun -hostfile hostfile -np 1 java -cp $HOME/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar com.mathpar.NAUKMA/matrDStrassDrop/tests/SequentialAlgorithmsTest
    public static void main(String... args) throws MPIException, IOException {
        MPI.Init(args);
        final int maxRandomValue = 100;
        Ring ring = new Ring("R[]");
        Files.write(Paths.get("testResults.txt"), "".getBytes(), StandardOpenOption.TRUNCATE_EXISTING);

        for (int n = 16; n <= 1024; n *= 2) {
            System.out.println();
            System.out.println("N=" + n);
            MatrixD aMD = getRandomMatrix(n, 1, maxRandomValue, ring);
            MatrixD bMD = getRandomMatrix(n, 2, maxRandomValue, ring);
            long curTime;
            long executeTime;
            MatrixD resSyncMult;
            Files.write(Paths.get("testResults.txt"), ("\nN=" + n).getBytes(), StandardOpenOption.APPEND);

            curTime = System.currentTimeMillis();
            resSyncMult = multStrassen(aMD, bMD, ring);
            executeTime = System.currentTimeMillis() - curTime;
            Files.write(Paths.get("testResults.txt"), ("; Strassen mult: " + executeTime).getBytes(), StandardOpenOption.APPEND);
            System.out.println("Strassen mult: " + executeTime);
            printResult(resSyncMult, n);

            curTime = System.currentTimeMillis();
            resSyncMult = aMD.multCU(bMD, ring);
            executeTime = System.currentTimeMillis() - curTime;
            Files.write(Paths.get("testResults.txt"), ("; Usual mult: " + executeTime).getBytes(), StandardOpenOption.APPEND);
            System.out.println("Usual mult: " + executeTime);
            printResult(resSyncMult, n);
        }

        MPI.Finalize();
    }

    private static void printResult(MatrixD res, int n) {
        if (n <= 8)
            System.out.println(res);
        else
            System.out.println(res.toString().substring(0, 100) + "...");
        System.out.println();
    }

    private static MatrixD getRandomMatrix(int n, int randomSeed, int maxValue, Ring r) {
        Element one = Ring.oneOfType(r.algebra[0]);
        Element[][] matr = new Element[n][n];
        Random rand = new Random(randomSeed);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matr[i][j] = one.valOf(rand.nextBoolean() ? rand.nextInt(maxValue) : -rand.nextInt(maxValue), r);
            }
        }

        return new MatrixD(matr);
    }

    private static final int strassenUsageLengthLimit = 32;

    public static MatrixD multStrassen(MatrixD a, MatrixD b, Ring ring) {
        if (a.M.length <= strassenUsageLengthLimit) {
            return a.multCU(b, ring);
        }

        MatrixD[] aSplit = a.splitTo4();
        MatrixD A11 = aSplit[0];
        MatrixD A12 = aSplit[1];
        MatrixD A21 = aSplit[2];
        MatrixD A22 = aSplit[3];

        MatrixD[] bSplit = b.splitTo4();
        MatrixD B11 = bSplit[0];
        MatrixD B12 = bSplit[1];
        MatrixD B21 = bSplit[2];
        MatrixD B22 = bSplit[3];

        MatrixD P1 = multStrassen(A11.add(A22, ring), B11.add(B22, ring), ring);
        MatrixD P2 = multStrassen(A21.add(A22, ring), B11, ring);
        MatrixD P3 = multStrassen(A11, B12.subtract(B22, ring), ring);
        MatrixD P4 = multStrassen(A22, B21.subtract(B11, ring), ring);
        MatrixD P5 = multStrassen(A11.add(A12, ring), B22, ring);
        MatrixD P6 = multStrassen(A21.subtract(A11, ring), B11.add(B12, ring), ring);
        MatrixD P7 = multStrassen(A12.subtract(A22, ring), B21.add(B22, ring), ring);

        MatrixD[] res = new MatrixD[]{
                P1.add(P4, ring).subtract(P5, ring).add(P7, ring), // C11
                P3.add(P5, ring), // C12
                P2.add(P4, ring), // C21
                P1.subtract(P2, ring).add(P3, ring).add(P6, ring) // C22
        };

        return MatrixD.joinMatr(res);
    }
}
