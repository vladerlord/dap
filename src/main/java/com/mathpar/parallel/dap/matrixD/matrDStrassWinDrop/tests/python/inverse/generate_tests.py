import json

if __name__ == "__main__":
    class_ = 'matrDTriangInvStrassWin'
    test_obj = 'matrDStrassWinDrop/matrDTriangInvStrassWin'
    proc = [2,4]
    matrix_size = [16, 32, 64, 128, 256, 512, 1024]
    leaf_size = [16]
    id = 1
    tests = []
    timeout = [1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000]

    for i in range(len(matrix_size)):
        for j in range(len(leaf_size)):
            for k in range(len(proc)):
                tests.append( { 'proc_count' : proc[k], 'name' : 'test_case_' + str(id), 'matrix_size' : matrix_size[i], 'leaf_size' : leaf_size[j], 'timeout' : timeout[i] } )
                id += 1

    data = { 'class_name' : class_, 'proc_count' : proc, 'test_obj' : test_obj, 'test_cases' : tests }
    json.dump(data, open('inverse_tests.json', 'w'), indent=4)