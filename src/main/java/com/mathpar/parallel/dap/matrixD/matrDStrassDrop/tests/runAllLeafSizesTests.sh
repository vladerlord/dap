#!/bin/bash
procCount=1
procCountMax=8
leafSizeMin=8
nMin=16
nMax=1024

truncate -s 0 testResults.txt

while [ $procCount -le $procCountMax ]
do
echo "procCount=$procCount\n" >> "testResults.txt"
echo "N\tLeaf size\tcalculation time(ms)" >> "testResults.txt"
n=$nMin
while [ $n -le $nMax ]
do
leafSize=$leafSizeMin
while [ $leafSize -le $n ]
do
(echo -n "$n\t$leafSize\t") >> "testResults.txt"

mpirun -hostfile hostfile -np $procCount java -cp $HOME/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar com.mathpar.NAUKMA/matrDStrassDrop/tests/matrDMultStrassTest $n $leafSize

leafSize=$(($leafSize * 2))
echo "" >> "testResults.txt"
done
n=$(($n * 2))
echo "" >> "testResults.txt"
done
procCount=$(($procCount * 2))
echo "" >> "testResults.txt"
done
