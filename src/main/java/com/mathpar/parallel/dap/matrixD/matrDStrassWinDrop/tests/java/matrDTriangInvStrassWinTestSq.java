package com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.tests.java;

import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.matrDTriangInvStrassWin;
import com.mathpar.parallel.dap.matrixD.matrDDrop.MatrixInverse;
public class matrDTriangInvStrassWinTestSq {

    public static long inverseSpeedTestDef(MatrixD a, Ring r) {
        long start_ms = System.currentTimeMillis();
        MatrixD inv = MatrixInverse.inverse_sequential_static_public(a, r);
        long end_ms = System.currentTimeMillis();
        return end_ms - start_ms;
    }

    public static long inverseSpeedTestVyn(MatrixD a, Ring r) {
        long start_ms = System.currentTimeMillis();
        MatrixD inv = matrDTriangInvStrassWin.inverse_sequential(a, r);
        long end_ms = System.currentTimeMillis();
        return end_ms - start_ms;
    }

    public static void main(String[] args) {
        System.currentTimeMillis();
        Ring ring = new Ring("R[]");

        int[] sizes = { 16, 32, 64, 128, 256, 512, 1024 };
        int count = 1;
        for (int i = 0; i < sizes.length; ++i) {
            MatrixD a = new MatrixD(matrDTriangInvStrassWin.triangle_matrix(sizes[i]), ring);
            for (int j = 0; j < count; ++j) {
                long time_def = inverseSpeedTestDef(a, ring);
                long time_vyn= inverseSpeedTestVyn(a, ring);
                System.out.printf("size %4d : def time %6d | Vyn time %6d\n", sizes[i], time_def, time_vyn);
            }
        }

    }

}
