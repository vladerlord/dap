package com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.core.DropTask;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.ArrayList;

public class matrDMultStrassWinNegate extends matrDMultStrassWin {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(matrDMultStrassWinNegate.class);

    private static int[][] _arcs = {
            {       2, 0, 0,    3, 1, 0,    7, 2, 0,    2, 3, 1,
                    3, 4, 1,    6, 5, 1,    5, 6, 0,    1, 7, 0,
                    4, 8, 0,    6, 9, 0,    5, 10, 1,   1, 11, 1,
                    4, 12, 1,   7, 13, 1}, // input function
            {8, 0, 0}, // p1
            {8, 0, 1}, // p2
            {8, 0, 2}, // p3
            {8, 0, 3}, // p4
            {8, 0, 4}, // p5
            {8, 0, 5}, // p6
            {8, 0, 6}, // p7
            {} // output function
    };

    public matrDMultStrassWinNegate() {
        inData = new Element[2];
        outData = new Element[1];
        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 14;
        type = 13;
        resultForOutFunctionLength = 7;
        inputDataLength = 2;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public ArrayList<DropTask> doAmin() {
        ArrayList<DropTask> amin = new ArrayList<DropTask>();
        amin.add(new matrDMultStrassWinNegate()); // p1
        amin.add(new matrDMultStrassWinNegate()); // p2
        amin.add(new matrDMultStrassWinNegate()); // p3
        amin.add(new matrDMultStrassWinNegate()); // p4
        amin.add(new matrDMultStrassWinNegate()); // p5
        amin.add(new matrDMultStrassWinNegate()); // p6
        amin.add(new matrDMultStrassWinNegate()); // p7
        return amin;
    }

    @Override
    public void sequentialCalc(Ring r) {
        MatrixD a = (MatrixD) inData[0];
        MatrixD b = (MatrixD) inData[1];
        outData[0] = multiply_func(a, b, r).negate(r);
    }

    /*
    mpirun -hostfile hostfile -np 2 java -cp \
    $HOME/Desktop/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar\
    com.mathpar.NAUKMA/v8_2020/bitaeva/matrDMultStrassWinInverse
     */

    /*
    mpirun -hostfile hostfile -np 2 java -cp \
    $HOME/workspace/new/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar\
    com.mathpar.NAUKMA/v8_2020/bitaeva/matrDMultStrassWinInverse
     */

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException, Exception {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int n = 512;
        int task = 26;

        if (rank == 0) {
            for (int i = 0; i < args.length; i += 2) {
                if (args[i].equals("-size")) {
                    n = Integer.parseInt(args[i+1]);
                } else if (args[i].equals("-leaf")) {
                    matrDMultStrassWin.setLeafSizeStatic(Integer.parseInt(args[i+1]));
                }
                else if (args[i].equals("-defs")) {
                    matrDMultStrassWin.setDefaultMultSize(Integer.parseInt(args[i+1]));
                }
            }
            LOGGER.info("n = " + n + " leaf = " + leafSize + " default mult at = " + defaultMultSize);
        }

        Ring ring = new Ring("R[]");
        MatrixD res = null;
        DispThread disp = new DispThread(1, args, ring);
        MatrixD a = null;
        MatrixD b = null;
        Element[] init = {};
        if (rank == 0) {
            a = new MatrixD(n, n, 100, ring);
            b = new MatrixD(n, n, 100, ring);
            init = new MatrixD[] { a, b };
            if (a.M.length <= 16) {
                print_matrix(a);
                print_matrix(b);
            }
        }

        disp.execute(task, args, init, ring);

        if (rank == 0) {
            res = (MatrixD) disp.getResult()[0];
            if (res.M.length <= 16) {
                print_matrix(res);
            }
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();
        MPI.Finalize();
    }

}
