#!/bin/bash
procCount=2
procCountMax=4
nMin=16
nMax=1024

truncate -s 0 testResults.txt

while [ $procCount -le $procCountMax ]
do
echo "procCount=$procCount\n" >> "testResults.txt"
echo "N\tMult\tStras\tStrasWin" >> "testResults.txt"
n=$nMin
while [ $n -le $nMax ]
do
(echo -n "$n\t") >> "testResults.txt"

mpirun -hostfile hostfile -np $procCount java -cp $HOME/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar com.mathpar.NAUKMA/matrDStrassDrop/tests/MatrDMultComparingTest $n

(echo -n "\t") >> "testResults.txt"

mpirun -hostfile hostfile -np $procCount java -cp $HOME/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar com.mathpar.NAUKMA/matrDStrassDrop/tests/matrDMultStrassTest $n 32

(echo -n "\t") >> "testResults.txt"

mpirun -hostfile hostfile -np $procCount java -cp $HOME/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar com.mathpar.NAUKMA/matrDStrassDrop/tests/matrDMultStrassWinComparingTest $n

n=$(($n * 2))
echo "" >> "testResults.txt"
done
procCount=$(($procCount * 2))
echo "" >> "testResults.txt"
done
