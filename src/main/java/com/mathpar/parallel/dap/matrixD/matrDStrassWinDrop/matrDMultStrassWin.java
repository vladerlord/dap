package com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.core.DropTask;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.ArrayList;

public class matrDMultStrassWin extends DropTask {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(matrDMultStrassWin.class);
    protected static int leafSize = 32;
    protected static int defaultMultSize = 32;

    private static int[][] _arcs = {
            {       2, 0, 0,    3, 1, 0,    7, 2, 0,    2, 3, 1,
                    3, 4, 1,    6, 5, 1,    5, 6, 0,    1, 7, 0,
                    4, 8, 0,    6, 9, 0,    5, 10, 1,   1, 11, 1,
                    4, 12, 1,   7, 13, 1}, // input function
            {8, 0, 0}, // p1
            {8, 0, 1}, // p2
            {8, 0, 2}, // p3
            {8, 0, 3}, // p4
            {8, 0, 4}, // p5
            {8, 0, 5}, // p6
            {8, 0, 6}, // p7
            {} // output function
    };

    public matrDMultStrassWin() {
        inData = new Element[2];
        outData = new Element[1];
        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 14;
        type = 12;
        resultForOutFunctionLength = 7;
        inputDataLength = 2;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public ArrayList<DropTask> doAmin() {
        ArrayList<DropTask> amin = new ArrayList<DropTask>();
        amin.add(new matrDMultStrassWin()); // p1
        amin.add(new matrDMultStrassWin()); // p2
        amin.add(new matrDMultStrassWin()); // p3
        amin.add(new matrDMultStrassWin()); // p4
        amin.add(new matrDMultStrassWin()); // p5
        amin.add(new matrDMultStrassWin()); // p6
        amin.add(new matrDMultStrassWin()); // p7
        return amin;
    }

    public static MatrixD multiply_sequential(MatrixD a, MatrixD b, Ring r) {
        if (a.M.length == 1) {
            Element[][] res = new Element[a.M.length][a.M.length];
            res[0][0] = a.getElement(0, 0).multiply(b.getElement(0, 0), r);
            return new MatrixD(res);
        }
        if (a.M.length == 2) {
            Element[][] res = new Element[a.M.length][a.M.length];
            res[0][0] = a.getElement(0, 0).multiply(b.getElement(0, 0), r).add(a.getElement(0, 1).multiply(b.getElement(1, 0), r), r);
            res[0][1] = a.getElement(0, 0).multiply(b.getElement(0, 1), r).add(a.getElement(0, 1).multiply(b.getElement(1, 1), r), r);
            res[1][0] = a.getElement(1, 0).multiply(b.getElement(0, 0), r).add(a.getElement(1, 1).multiply(b.getElement(1, 0), r), r);
            res[1][1] = a.getElement(1, 0).multiply(b.getElement(0, 1), r).add(a.getElement(1, 1).multiply(b.getElement(1, 1), r), r);
            return new MatrixD(res);
        }

        MatrixD[] splitted_a = a.splitTo4();
        MatrixD[] splitted_b = b.splitTo4();

        MatrixD s1 = splitted_a[2].add(splitted_a[3], r); // a21 + a22
        MatrixD s2 = s1.subtract(splitted_a[0], r); // s1 - a11
        MatrixD s3 = splitted_a[0].subtract(splitted_a[2], r); // a11 - a21
        MatrixD s4 = splitted_a[1].subtract(s2, r); // a12 - s2

        MatrixD s5 = splitted_b[1].subtract(splitted_b[0], r); // b12 - b11
        MatrixD s6 = splitted_b[3].subtract(s5, r); // b22 - s5
        MatrixD s7 = splitted_b[3].subtract(splitted_b[1], r); // b22 - b12
        MatrixD s8 = s6.subtract(splitted_b[2], r); // s6 - b21

        MatrixD p1 = multiply_func(s2, s6, r); // s2 * s6
        MatrixD p2 = multiply_func(splitted_a[0], splitted_b[0], r); // a11 * b11
        MatrixD p3 = multiply_func(splitted_a[1], splitted_b[2], r); // a12 * b21
        MatrixD p4 = multiply_func(s3, s7, r); // s3 * s7
        MatrixD p5 = multiply_func(s1, s5, r); // s1 * s5
        MatrixD p6 = multiply_func(s4, splitted_b[3], r); // s4 * b22
        MatrixD p7 = multiply_func(splitted_a[3], s8, r); // a22 * s8

        MatrixD T1 = p1.add(p2, r);
        MatrixD T2 = T1.add(p4, r);

        MatrixD C11 = p2.add(p3, r);
        MatrixD C12 = T1.add(p5, r).add(p6, r);
        MatrixD C21 = T2.subtract(p7, r);
        MatrixD C22 = T2.add(p5, r);

        return MatrixD.joinMatr(new MatrixD[] {C11, C12, C21, C22} );
    }

    public static MatrixD multiply_func(MatrixD a, MatrixD b, Ring r) {
        if (a.M.length <= defaultMultSize) {
            return a.multiplyMatr(b, r);
        }
        return multiply_sequential(a, b, r);
    }

    @Override
    public void sequentialCalc(Ring r) {
        MatrixD a = (MatrixD)inData[0];
        MatrixD b = (MatrixD)inData[1];
        outData[0] = multiply_func(a, b, r);
    }

    @Override
    public MatrixD[] inputFunction(Element[] input, int inputKey, Ring r) {

        MatrixD a = (MatrixD) input[0];
        MatrixD b = (MatrixD) input[1];

//        LOGGER.info("input mul " + a.M.length + " " + a.M[0].length+ " " + b.M.length + " " + b.M[0].length);

        MatrixD[] input_m = new MatrixD[14];

        MatrixD[] a_splitted = a.splitTo4();
        MatrixD[] b_splitted = a.splitTo4();

        input_m[0] = a_splitted[0];
        input_m[1] = a_splitted[1];
        input_m[2] = a_splitted[3];

        input_m[3] = b_splitted[0];
        input_m[4] = b_splitted[2];
        input_m[5] = b_splitted[3];

        input_m[6] = a_splitted[2].add(a_splitted[3], r);       // s1 = a21 + a22
        input_m[7] = input_m[6].subtract(a_splitted[0], r);     // s2 = s1 - a11
        input_m[8] = a_splitted[0].subtract(a_splitted[2], r);  // s3 = a11 - a21
        input_m[9] = a_splitted[1].subtract(input_m[7], r);     // s4 = a12 - s2
        input_m[10] = b_splitted[1].subtract(b_splitted[0], r); // s5 = b12 - b11
        input_m[11] = b_splitted[3].subtract(input_m[10], r);   // s6 = b22 - s5
        input_m[12] = b_splitted[3].subtract(b_splitted[1], r); // s7 = b22 - b12
        input_m[13] = input_m[11].subtract(b_splitted[2], r);   // s8 = s6 - b21

        return input_m;
    }

    @Override
    public MatrixD[] outputFunction(Element[] input, Ring r) {
        MatrixD[] input_m = new MatrixD[input.length];
        for (int i = 0; i < input.length; ++i) {
            input_m[i] = (MatrixD) input[i];
        }

        MatrixD T1 = input_m[0].add(input_m[1], r);
        MatrixD T2 = T1.add(input_m[3], r);

        MatrixD[] C_matrix = new MatrixD[] {
                input_m[1].add(input_m[2], r), // C11 = P1 + P2
                T1.add(input_m[4].add(input_m[5], r), r), // C12 = T1 + P5 +P6
                T2.subtract(input_m[6], r), // C21 = T2 - P7
                T2.add(input_m[4], r) // C22 = T2 + P5
        };

        return new MatrixD[] { MatrixD.joinMatr(C_matrix) };
    }

    @Override
    public boolean isItLeaf() {
        MatrixD m = (MatrixD) inData[0];
        return  m.M.length <= leafSize;
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }

    public static void setLeafSizeStatic(int dataSize) {
        leafSize = dataSize;
    }

    public static void setDefaultMultSize(int size) { defaultMultSize = size; }

    public static int getLeafSize() { return leafSize; }

    static public void print_matrix(MatrixD m) {
        for (int i = 0; i < m.M.length; ++i) {
            for (int j = 0; j < m.M.length; ++j)
                System.out.printf("%3.4f ", m.M[i][j].doubleValue());
            System.out.println("");
        }
        System.out.println("");
    }

    /* mpirun -hostfile hostfile -np 2 java -cp \
    $HOME/Desktop/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar\
    com.mathpar.NAUKMA/v8_2020/bitaeva/matrDMultStrassWin
     */

    /* time mpirun -hostfile hostfile -np 3 java -cp \
    $HOME/workspace/new/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar\
    com.mathpar.NAUKMA/v8_2020/bitaeva/matrDMultStrassWin -size 1024 -leaf 64
     */

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException, Exception {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int n = 512;
        int task = 25;
        long start = 0;

        for (int i = 0; i < args.length; i += 2) {
            if (args[i].equals("-size")) {
                n = Integer.parseInt(args[i+1]);
            } else if (args[i].equals("-leaf")) {
                matrDMultStrassWin.setLeafSizeStatic(Integer.parseInt(args[i+1]));
            }
            else if (args[i].equals("-defs")) {
                matrDMultStrassWin.setDefaultMultSize(Integer.parseInt(args[i+1]));
            }
        }
        LOGGER.info("n = " + n + " leaf = " + leafSize + " default mult at = " + defaultMultSize);

        Ring ring = new Ring("R[]");
        MatrixD res = null;
        DispThread disp = new DispThread(1, args, ring);
        MatrixD a = null;
        MatrixD b = null;
        Element[] init = {};
        if (rank == 0) {
            a = new MatrixD(n, n, 100, ring);
            b = new MatrixD(n, n, 100, ring);
            init = new MatrixD[] { a, b };
            if (a.M.length <= 16) {
                print_matrix(a);
                print_matrix(b);
            }
            start = System.currentTimeMillis();
        }

        disp.execute(task, args, init, ring);

        if (rank == 0) {
            res = (MatrixD) disp.getResult()[0];
            long timeElapsed = System.currentTimeMillis() - start;
            LOGGER.info("time elapsed : " + timeElapsed);
            if (res.M.length <= 16) {
                print_matrix(res);
            }
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();
        MPI.Finalize();
    }

}
