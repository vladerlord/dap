
import sys
import random

if __name__ == "__main__":
    filenames = [ 'matrixA.txt', 'matrixB.txt' ]
    size = int(sys.argv[1])
    for f in filenames:
        with open(f, 'w', encoding='utf-8') as out:
            for i in range(size):
                for j in range(size):
                    out.write(str(random.randint(0, 50)) + ' ')
                out.write('\n')