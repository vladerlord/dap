#!/bin/bash

test_lst="bitaeva benyukh semylitko"

for test in $test_lst ; do
    pushd $test || exit 1
        [ -d logs ] && rm -rf logs
        mkdir logs
        python3 generate_tests.py
        python3 run_tests.py 
    popd || exit 1
done

echo -e "\n\nall test finished\n\n"