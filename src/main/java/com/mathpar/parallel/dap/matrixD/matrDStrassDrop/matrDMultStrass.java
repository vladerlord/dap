package com.mathpar.parallel.dap.matrixD.matrDStrassDrop;

import com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.ParserMatrix;
import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.core.DropTask;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

/**
 * @author Maksym Kyian
 */
public class matrDMultStrass extends DropTask {
    public static int LeafSize = 128;

    private final static int[][] _arcs = {
            {       3, 0, 0,    4, 1, 0,    2, 2, 1,    5, 3, 1,
                    1, 4, 0,    1, 5, 1,    2, 6, 0,    3, 7, 1,
                    4, 8, 1,    5, 9, 0,    6, 10, 0,   6, 11, 1,
                    7, 12, 0,   7, 13, 1}, // input function
            {8, 0, 0}, // P1
            {8, 0, 1}, // P2
            {8, 0, 2}, // P3
            {8, 0, 3}, // P4
            {8, 0, 4}, // P5
            {8, 0, 5}, // P6
            {8, 0, 6}, // P7
            {} // output function
    };

    public matrDMultStrass() {
        inData = new Element[2];
        outData = new Element[1];
        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 14;
        type = 9;
        resultForOutFunctionLength = 7;
        inputDataLength = 2;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public ArrayList<DropTask> doAmin() {
        ArrayList<DropTask> amin = new ArrayList<>();
        amin.add(new matrDMultStrass()); // P1
        amin.add(new matrDMultStrass()); // P2
        amin.add(new matrDMultStrass()); // P3
        amin.add(new matrDMultStrass()); // P4
        amin.add(new matrDMultStrass()); // P5
        amin.add(new matrDMultStrass()); // P6
        amin.add(new matrDMultStrass()); // P7
        return amin;
    }

    @Override
    public void sequentialCalc(Ring r) {
        MatrixD a = (MatrixD) inData[0];
        MatrixD b = (MatrixD) inData[1];
        outData[0] = a.multCU(b, r);
    }

    @Override
    public MatrixD[] inputFunction(Element[] input, int inputKey, Ring r) {
        MatrixD[] aSplit = ((MatrixD) input[0]).splitTo4();
        MatrixD A11 = aSplit[0];
        MatrixD A12 = aSplit[1];
        MatrixD A21 = aSplit[2];
        MatrixD A22 = aSplit[3];

        MatrixD[] bSplit = ((MatrixD) input[1]).splitTo4();
        MatrixD B11 = bSplit[0];
        MatrixD B12 = bSplit[1];
        MatrixD B21 = bSplit[2];
        MatrixD B22 = bSplit[3];

        MatrixD[] inputMatr = new MatrixD[14];
        inputMatr[0] = A11;
        inputMatr[1] = A22;
        inputMatr[2] = B11;
        inputMatr[3] = B22;
        inputMatr[4] = A11.add(A22, r); // S1
        inputMatr[5] = B11.add(B22, r); // S2
        inputMatr[6] = A21.add(A22, r); // S3
        inputMatr[7] = B12.subtract(B22, r); // S4
        inputMatr[8] = B21.subtract(B11, r); // S5
        inputMatr[9] = A11.add(A12, r); // S6
        inputMatr[10] = A21.subtract(A11, r); // S7
        inputMatr[11] = B11.add(B12, r); // S8
        inputMatr[12] = A12.subtract(A22, r); // S9
        inputMatr[13] = B21.add(B22, r); // S10

        return inputMatr;
    }

    @Override
    public MatrixD[] outputFunction(Element[] input, Ring r) {
        MatrixD P1 = (MatrixD) input[0];
        MatrixD P2 = (MatrixD) input[1];
        MatrixD P3 = (MatrixD) input[2];
        MatrixD P4 = (MatrixD) input[3];
        MatrixD P5 = (MatrixD) input[4];
        MatrixD P6 = (MatrixD) input[5];
        MatrixD P7 = (MatrixD) input[6];

        MatrixD[] C_matrix = new MatrixD[]{
                P1.add(P4, r).subtract(P5, r).add(P7, r), // C11
                P3.add(P5, r), // C12
                P2.add(P4, r), // C21
                P1.subtract(P2, r).add(P3, r).add(P6, r) // C22
        };

        return new MatrixD[]{MatrixD.joinMatr(C_matrix)};
    }

    @Override
    public boolean isItLeaf() {
        return ((MatrixD) inData[0]).M.length <= LeafSize;
    }

    @Override
    public void setLeafSize(int dataSize) {
        LeafSize = dataSize;
    }

    // mpirun -hostfile hostfile -np 4 java -cp $HOME/new/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar com.mathpar.NAUKMA/matrDStrassDrop/matrDMultStrass

    /*
    mpirun -hostfile hostfile -np 2 java -cp \
    $HOME/workspace/new/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar \
    com.mathpar.NAUKMA/matrDStrassDrop/matrDMultStrass
    */

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        final int n = 128;
        LeafSize = n <= 128 ? n : 32;
        final int maxRandomValue = 100;
        final int dropType = 10;
        Ring ring = new Ring("R[]");
//        MatrixD a = getRandomMatrix(n, 1, maxRandomValue, ring);
//        MatrixD b = getRandomMatrix(n, 3, maxRandomValue, ring);
        MatrixD a = ParserMatrix.read_matrixD("matrixA.txt", n);
        MatrixD b = ParserMatrix.read_matrixD("matrixB.txt", n);
        DispThread disp = new DispThread(1, args, ring);

        if (rank == 0) {
            System.out.println("N=" + n);
            System.out.println("LeafSize=" + LeafSize);
            System.out.println("A=");
            printMatr(a);
            System.out.println("B=");
            printMatr(b);
        }

        disp.execute(dropType, args, new MatrixD[]{a, b}, ring);

        if (rank == 0) {
            MatrixD result = (MatrixD) disp.getResult()[0];
            System.out.println("Result:");
            printMatr(result);
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();
        MPI.Finalize();
    }

    private static MatrixD getRandomMatrix(int n, int randomSeed, int maxValue, Ring r) {
        Element one = Ring.oneOfType(r.algebra[0]);
        Element[][] matr = new Element[n][n];
        Random rand = new Random(randomSeed);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matr[i][j] = one.valOf(rand.nextBoolean() ? rand.nextInt(maxValue) : -rand.nextInt(maxValue), r);
            }
        }

        return new MatrixD(matr);
    }

    private static void printMatr(MatrixD matr) {
        if (matr.M.length <= 16)
            System.out.println(matr);
        else
            System.out.println(matr.toString().substring(0, 100) + "...");
        System.out.println();
    }
}