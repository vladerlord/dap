package com.mathpar.parallel.dap.matrixD.matrDDrop.Tests;

import com.mathpar.parallel.dap.matrixD.matrDDrop.MatrDMult;
import com.mathpar.matrix.MatrixD;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.test.DAPTest;
import mpi.MPI;
import mpi.MPIException;
import org.javatuples.Pair;

import java.io.IOException;
import java.util.Random;

public class MatrDMultTest extends DAPTest
{

    private static int Max_Random_Value = 9;
    //TODO?
    protected MatrDMultTest(String reportFile, int taskType)
    {
        super(reportFile, taskType);
    }
    //TODO?
    @Override
    protected MatrixS[] initData(int size, int density, int maxBits, Ring ring)
    {
        return new MatrixS[0];
    }

    //TODO?
    @Override
    protected Pair<Boolean, Element> checkResult(DispThread dispThread, String[] args, Element[] initData, Element[] resultData, Ring ring)
    {
        return null;
    }

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException
    {
        MPI.Init(args);

        Ring ring = new Ring("R[]");
        int rank = MPI.COMM_WORLD.getRank();
        if(rank == 0)
        {
            System.out.println("===================================");
            System.out.println("Start timer!");
            System.out.println("===================================");
            System.out.println("Matrix size = " + MatrDMult.Matrix_Size);
            System.out.println("===================================");
            System.out.println("Leaf size = " + MatrDMult.Leaf_Size);
            System.out.println("===================================");
        }

        MatrixD matrix_1 = new MatrixD(GenerateMatrix(MatrDMult.Matrix_Size, 1), ring);
        MatrixD matrix_2 = new MatrixD(GenerateMatrix(MatrDMult.Matrix_Size, 2), ring);

        /*if (rank == 0) {
            System.out.println("Matrix 1 = ");
            System.out.println(matrix_1);
            System.out.println();

            System.out.println("Matrix 2 = ");
            System.out.println(matrix_2);
            System.out.println();
        }*/
        
        DispThread d_Thread = new DispThread(1, args, ring);


        Element[] input = new MatrixD[]{ matrix_1, matrix_2 };
        d_Thread.execute(MatrDMult.Drop_Number, args, input, ring);
        d_Thread.counter.DoneThread();
        d_Thread.counter.thread.join();
        MPI.Finalize();

        if (rank == 0)
        {
            MatrixD result = (MatrixD) d_Thread.getResult()[0];

            //System.out.println();
            //System.out.println("Result Matrix =");
            //System.out.println(result);
            //System.out.println();
        }
    }

    private static int[][] GenerateMatrix(int m_size, int seed)
    {
        int[][] new_matrix = new int[m_size][m_size];
        Random rand = new Random(seed);
        for (int i = 0; i < m_size; i++)
        {
            for(int j = 0; j < m_size; j++)
            {
                new_matrix[i][j] = rand.nextInt(Max_Random_Value);
            }
        }

        return new_matrix;
    }
}
