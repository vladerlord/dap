package com.mathpar.parallel.dap.matrixD.matrDStrassDrop.tests;

import com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.matrDMultStrassWin;
import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Random;

public class SequentialAlgorithmsComparingTest {
    // mpirun -hostfile hostfile -np 1 java -cp $HOME/dap/target/classes:$HOME/.m2/repository/org/apache/logging/log4j/log4j-api/2.12.1/log4j-api-2.12.1.jar:$HOME/.m2/repository/org/apache/logging/log4j/log4j-core/2.12.1/log4j-core-2.12.1.jar com.mathpar.NAUKMA/matrDStrassDrop/tests/SequentialAlgorithmsComparingTest
    private final static String resultsFileName = "testResults.txt";

    public static void main(String... args) throws MPIException, IOException {
        MPI.Init(args);
        final int maxRandomValue = 100;
        Ring ring = new Ring("R[]");
        Files.write(Paths.get(resultsFileName), "".getBytes(), StandardOpenOption.TRUNCATE_EXISTING);
        writeToFile("N\tMult\tStras\tStrasWin\n");

        for (int n = 16; n < 1024; n *= 2) {
            System.out.println();
            System.out.println("N=" + n);
            MatrixD aMD = getRandomMatrix(n, 1, maxRandomValue, ring);
            MatrixD bMD = getRandomMatrix(n, 2, maxRandomValue, ring);
            long curTime;
            long executeTime;
            MatrixD resSyncMult;
            writeToFile(n + "\t");

            curTime = System.currentTimeMillis();
            resSyncMult = aMD.multCU(bMD, ring);
            executeTime = System.currentTimeMillis() - curTime;
            writeToFile(executeTime + "\t");
            System.out.println("Mult: " + executeTime);
            printResult(resSyncMult, n);

            curTime = System.currentTimeMillis();
            resSyncMult = SequentialAlgorithmsTest.multStrassen(aMD, bMD, ring);
            executeTime = System.currentTimeMillis() - curTime;
            writeToFile(executeTime + "\t");
            System.out.println("Strassen: " + executeTime);
            printResult(resSyncMult, n);

            curTime = System.currentTimeMillis();
            resSyncMult = matrDMultStrassWin.multiply_func(aMD, bMD, ring);
            executeTime = System.currentTimeMillis() - curTime;
            writeToFile(executeTime + "\n");
            System.out.println("StrassenWin: " + executeTime);
            printResult(resSyncMult, n);
        }

        MPI.Finalize();
    }

    private static void writeToFile(String text) throws IOException {
        Files.write(Paths.get(resultsFileName), text.getBytes(), StandardOpenOption.APPEND);
    }

    private static void printResult(MatrixD res, int n) {
        if (n <= 8)
            System.out.println(res);
        else
            System.out.println(res.toString().substring(0, 100) + "...");
        System.out.println();
    }

    private static MatrixD getRandomMatrix(int n, int randomSeed, int maxValue, Ring r) {
        Element one = Ring.oneOfType(r.algebra[0]);
        Element[][] matr = new Element[n][n];
        Random rand = new Random(randomSeed);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matr[i][j] = one.valOf(rand.nextBoolean() ? rand.nextInt(maxValue) : -rand.nextInt(maxValue), r);
            }
        }

        return new MatrixD(matr);
    }
}
