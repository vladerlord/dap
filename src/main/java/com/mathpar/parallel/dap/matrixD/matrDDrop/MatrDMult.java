package com.mathpar.parallel.dap.matrixD.matrDDrop;

import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DropTask;

import java.util.ArrayList;

//mpirun --hostfile ./hostfile -np 1 java -cp ./target/classes:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/log4j-api-2.12.1.jar:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/log4j-core-2.12.1.jar:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/javatuples-1.2.jar com/mathpar/NAUKMA/matrDDrop/Tests/MatrDMultTest

//mpirun --hostfile ./hostfile -np 2 java -cp ./target/classes:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/log4j-api-2.12.1.jar:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/log4j-core-2.12.1.jar:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/javatuples-1.2.jar com/mathpar/NAUKMA/matrDDrop/Tests/MatrDMultTest

//mpirun --hostfile ./hostfile -np 4 java -cp ./target/classes:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/log4j-api-2.12.1.jar:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/log4j-core-2.12.1.jar:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/javatuples-1.2.jar com/mathpar/NAUKMA/matrDDrop/Tests/MatrDMultTest

//mpirun --hostfile ./hostfile -np 6 java -cp ./target/classes:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/log4j-api-2.12.1.jar:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/log4j-core-2.12.1.jar:/Users/alexveligurskiy/Desktop/University/__MP_2/Malashonok/MathPar/target/lib/javatuples-1.2.jar com/mathpar/NAUKMA/matrDDrop/Tests/MatrDMultTest

public class MatrDMult extends DropTask
{
    public static int Drop_Number = 10;
    public static int Leaf_Size = 512;
    public static int Matrix_Size = 1024;

    private static int Input_Matrix_Size = 8;

    private static int[][] Matrix_Arcs = new int[][]
    {
        {
            1, 0, 0,   1, 4, 1,   2, 1, 0,   2, 6, 1,   3, 0, 0,   3, 5, 1,   4, 1, 0,   4, 7, 1,
            5, 2, 0,   5, 4, 1,   6, 3, 0,   6, 6, 1,   7, 2, 0,   7, 5, 1,   8, 3, 0,   8, 7, 1
        },
        {2, 0, 2},{9, 0, 0},{4, 0, 2},{9, 0, 1},
        {6, 0, 2},{9, 0, 2},{8, 0, 2},{9, 0, 3},
        {}
    };
    private int leaf_size = Leaf_Size;
    public MatrDMult()
    {
        System.out.println("DenseMatrixMultiplication");
        inData = new Element[2];
        outData = new Element[1];

        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 16;

        number = cnum++;
        type = Drop_Number;

        resultForOutFunctionLength = 4;
        inputDataLength = 2;

        arcs = Matrix_Arcs;
    }


    @Override
    public Element[] inputFunction(Element[] input, int inputKey, Ring ring)
    {
        MatrixD[] resultM = new MatrixD[Input_Matrix_Size];
        MatrixD[] m0 = ((MatrixD) input[0]).splitTo4();
        MatrixD[] m1 = ((MatrixD) input[1]).splitTo4();

        System.arraycopy(m0, 0, resultM, 0, m0.length);
        System.arraycopy(m1, 0, resultM, m0.length, m1.length);
        //Tools.concatTwoArrays(m0, m1, resultM);
        return resultM;
    }

    @Override
    public Element[] outputFunction(Element[] input, Ring ring)
    {
        MatrixD[] resultM = new MatrixD[input.length];
        for (int a = 0; a < input.length; a++)
        {
            resultM[a] = (MatrixD) input[a];
        }

        return new MatrixD[] {MatrixD.joinMatr(resultM)};
    }

    @Override
    public boolean isItLeaf() {
        return ((MatrixD)inData[0]).M.length <= Leaf_Size;
    }

    @Override
    public ArrayList<DropTask> doAmin()
    {
        ArrayList<DropTask> amin = new ArrayList<DropTask>();

        amin.add(new MatrDMult());
        amin.add(new MatrDMultAdd());
        amin.add(new MatrDMult());
        amin.add(new MatrDMultAdd());
        amin.add(new MatrDMult());
        amin.add(new MatrDMultAdd());
        amin.add(new MatrDMult());
        amin.add(new MatrDMultAdd());

        return amin;
    }

    @Override
    public void sequentialCalc(Ring ring)
    {
        outData[0] = inData[0].multiply(inData[1], ring);
    }


    @Override
    public void setLeafSize(int dataSize)
    {
        leaf_size = dataSize;
    }
}
