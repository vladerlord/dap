package com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DropTask;

import java.util.ArrayList;

public class matrDMultStrassWinSub extends matrDMultStrassWin {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(matrDMultStrassWinSub.class);

    private static int[][] _arcs = {
            { 1, 1, 1,   1, 2, 0,  2, 0, 0,    2, 2, 1 },
            { 2, 0, 2 },
            {} // output function
    };

    public matrDMultStrassWinSub() {
        inData = new Element[2];
        outData = new Element[2];
        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 4;
        type = 14;
        resultForOutFunctionLength = 3;
        inputDataLength = 2;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public void sequentialCalc(Ring ring) {
//        LOGGER.info("sequentialCalc MAdd, inData[2] = " + inData[2]);
        MatrixD bt = (MatrixD) inData[0];
        MatrixD y = (MatrixD) inData[1];
        outData[1] = bt.transpose(ring);
        outData[0] = y.subtract(outData[1] .multiply(bt, ring), ring);
    }

    @Override
    public ArrayList<DropTask> doAmin() {
        ArrayList<DropTask> amin = new ArrayList<DropTask>();
        amin.add(new matrDMultStrassWin());
        return amin;
    }

    @Override
    public MatrixD[] inputFunction(Element[] input, int inputKey, Ring r) {

        MatrixD gamma = (MatrixD) input[0];
        MatrixD bT = (MatrixD) input[1];
        MatrixD b = bT.transpose(r);

        return new MatrixD[] { gamma, bT, b };
    }

    @Override
    public MatrixD[] outputFunction(Element[] input, Ring r) {
        MatrixD y = (MatrixD) input[0];
        MatrixD b = (MatrixD) input[1];
        MatrixD x = (MatrixD) input[2];

        return new MatrixD[] { y.subtract(x, r), b };
    }

}
