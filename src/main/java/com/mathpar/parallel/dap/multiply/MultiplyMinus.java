/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mathpar.parallel.dap.multiply;
//package com.mathpar.students.ukma17i41.bosa.parallel.engine;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;

/**
 *
 * @author alla
 */
public class MultiplyMinus extends Multiply {
  private final static MpiLogger LOGGER = MpiLogger.getLogger(MultiplyMinus.class);
    private static int[][] _arcs = new int[][] {{1, 0, 0, 1, 4, 1, 2, 1, 0, 2, 6, 1, 3, 0, 0, 3, 5, 1, 4, 1, 0, 4, 7, 1,
            5, 2, 0, 5, 4, 1, 6, 3, 0, 6, 6, 1, 7, 2, 0, 7, 5, 1, 8, 3, 0, 8, 7, 1},
            {2, 0, 2}, {9, 0, 0}, {4, 0, 2}, {9, 0, 1}, {6, 0, 2}, {9, 0, 2}, {8, 0, 2}, {9, 0, 3}, {}};
    public MultiplyMinus() {
        inData = new Element[2];
        outData = new Element[1];
        numberOfMainComponents = 2;
        arcs = _arcs;
        type = 2;
        resultForOutFunctionLength = 4;
        numberOfMainComponentsAtOutput = 16;
        inputDataLength = 2;
        number = cnum++;
    }

    @Override
    public void sequentialCalc(Ring ring) {
        outData[0] = ((MatrixS)inData[0]).multiply((MatrixS)inData[1], ring).negate(ring);
        //LOGGER.info("in seq calc multminus inData[0] = "+ inData[0]);
       // LOGGER.info("inData[1] = "+ inData[1]);
    }

    @Override
    public MatrixS[] outputFunction(Element[] input, Ring ring) {
        MatrixS[] resmat = new MatrixS[input.length];
        for (int i = 0; i < input.length; i++) {
            resmat[i] = (MatrixS) input[i];
            //LOGGER.info("resmat[i] = "+resmat[i]);
        }
        MatrixS[] res = new MatrixS[] {MatrixS.join(resmat).negate(ring)};
        return res;

    }

}
