package com.mathpar.parallel.dap.multiply.multiplyVar;

import com.mathpar.number.Element;

public interface DropTaskConfig<T extends Element> {


    byte[] getData();

}
