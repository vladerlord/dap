/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templatesamin.outputData
 * and open the template in the editor.
 */
package com.mathpar.parallel.dap.core;

import com.mathpar.log.MpiLogger;
import com.mathpar.number.Array;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;

import java.util.*;

import com.mathpar.parallel.dap.test.MemoryManager;
import mpi.MPI;
import mpi.MPIException;

/**
 * @author alla
 * @param
 */
public class CalcThread implements Runnable {

    private final static MpiLogger LOGGER = MpiLogger.getLogger(CalcThread.class);

    public Thread thread;
    Ring ring;

    private ArrayList<Amin> pine;
    ArrayList<DropTask>[] vokzal;
    //список готових результатов для отправки родительским процессорам(отправляет диспетчер)
    volatile ArrayList<DropTask> aerodromeResults;
    ArrayList<DropTask> ownTrack;
    volatile ArrayList<DropTask> terminalAddComponents;
    //Stack<DropTask> LeavesStack;

    Element[] result;
    DropTask currentDrop;
    static int myRank;

    volatile boolean finish;
    volatile boolean flToExit;
    volatile boolean IamFree;
    boolean changeTrack;
    long currentMemory;

    static long counterCycle = 0;

    long calcWorkTime = 0;
    long calcWaitTime = 0;

    public CalcThread(ArrayList<Amin> p, ArrayList<DropTask> tAddComp,
                      ArrayList<DropTask> ownTr, Ring ring) throws MPIException {
        thread = new Thread(this, "CalcThread");
        thread.setPriority(1);
        flToExit = false;
        //isEmptyVokzal = true;
        finish = false;
        pine = p;
        terminalAddComponents = tAddComp;
        ownTrack = ownTr;
        this.ring = ring;
        aerodromeResults = new ArrayList<>();
        vokzal = new ArrayList[21];
        myRank = MPI.COMM_WORLD.getRank();
        IamFree = false;
        changeTrack = false;
        //LeavesStack = new Stack<>();

        for (int i = 0; i < vokzal.length; i++) {
            vokzal[i] = new ArrayList<DropTask>();
        }

        thread.start();
    }

    public void DoneThread() {
        flToExit = true;
    }

    public void clear() {
        pine.clear();
        Arrays.stream(vokzal).forEach(ArrayList::clear);
        aerodromeResults.clear();
        ownTrack.clear();
        terminalAddComponents.clear();
        // LeavesStack.clear();
    }


    public void putDropInTrack(DropTask drop) {
        synchronized (ownTrack) {
            ownTrack.add(drop);
            if (drop.recNum < DispThread.trackLevel)
                DispThread.trackLevel = drop.recNum;
        }

    }
    public void putDropInVokzal(DropTask drop) {
        LOGGER.trace("put drop in vokzal rec " + drop.recNum);
        //LOGGER.info("put drop in vokzal num = " + drop.number + ", id = " + drop.dropId + ", amin = " + drop.aminId);
        vokzal[drop.recNum].add(drop);
        if (drop.numberOfDaughterProc == -2) {
            drop.numberOfDaughterProc = -1;
        }

        if (drop.recNum > DispThread.myLevelH || DispThread.myLevelH == 20) {
            DispThread.myLevelH = drop.recNum;
        }

        if (DispThread.myLevel == 20 || DispThread.myLevel > DispThread.myLevelH) {
            DispThread.myLevel = DispThread.myLevelH;
        }
    }

    public void writeResultsToAmin(DropTask drop) {

        int aminId = drop.aminId;
        int dropId = drop.dropId;
        //LOGGER.info("in writeResultsToAmin dropid = " + dropId + ", aminid = " + aminId);
        Amin amin = pine.get(aminId);
        DropTask aminDrop = DropTask.getDropObject(amin.type, amin.config);

        for (int i = 0; i < aminDrop.arcs[dropId + 1].length; i += 3) {

            int number = aminDrop.arcs[dropId + 1][i];
            int from = aminDrop.arcs[dropId + 1][i + 1];
            int to = aminDrop.arcs[dropId + 1][i + 2];
           // LOGGER.info(" number = " + number + ", from =  " + from + ", to = " + to);

            if (aminDrop.arcs[number].length != 0) {

                DropTask dependantDrop = amin.branch.get(number - 1);
                synchronized (dependantDrop) {

                    dependantDrop.inData[to] = drop.outData[from];
                   // LOGGER.trace("drop.outData[from] = "+drop.outData[from]);
                  /*  if (dependantDrop.hasFullInputData() && dependantDrop.isItLeaf()) {
                        //LOGGER.info("writeResultsToAmin go to GLLD = " + dependantDrop.number);
                        LeavesStack.push(dependantDrop);
                    }*/

                   // LOGGER.trace("hasFullInputData()" + dependantDrop.hasFullInputData() + "numberOfDaughterProc = " + dependantDrop.numberOfDaughterProc);
                    //LOGGER.trace("hasMainInputData() "+ dependantDrop.hasMainInputData());
                    if (((dependantDrop.hasMainInputData()&& !dependantDrop.isItLeaf()) ||
                            dependantDrop.hasFullInputData()) && dependantDrop.numberOfDaughterProc == -2) {
                       // LOGGER.trace("writeResultsToAmin go to putinvokzal" +  dependantDrop.number);
                        putDropInVokzal(dependantDrop);

                    } else if (dependantDrop.hasAdditionalInputData()
                            && dependantDrop.numberOfDaughterProc > -1 && !dependantDrop.fullDrop) {

//                        LOGGER.warn("before  addNotMainComponents");
                        addNotMainComponents(dependantDrop);

                    }
                }
            } else {

                amin.resultForOutFunction[to] = drop.outData[from];

                if (amin.hasFullOutput()) {
                    //LOGGER.info("go to this 2");
                    putResultsToAminOutput(amin);
                }
            }
        }


    }

    private void addNotMainComponents(DropTask dependantDrop) {
        if (dependantDrop.numberOfDaughterProc != myRank) {
            synchronized (terminalAddComponents) {
                terminalAddComponents.add(dependantDrop);
            }
        } else {
            int length = dependantDrop.inputDataLength - dependantDrop.numberOfMainComponents;
            Element[] additionalComponents = new Element[length];
            System.arraycopy(dependantDrop.inData, dependantDrop.numberOfMainComponents, additionalComponents, 0, length);
            DropTask drop = DropTask.doNewDrop(dependantDrop.type, dependantDrop.config, dependantDrop.aminId, dependantDrop.dropId, dependantDrop.procId, dependantDrop.recNum, additionalComponents);
            drop.type = -1;
            drop.setNumbOfMyAmine(dependantDrop.getNumbOfMyAmine());
//            LOGGER.warn("number of Daughter proc = "+dependantDrop.numberOfDaughterProc + "  myAmin = "+dependantDrop.getNumbOfMyAmine() + " number = ");
            //LOGGER.warn("Add not main " + dependantDrop);
            putDropInTrack(drop);
        }
    }

    private void writeResultsAfterInpFunc(DropTask drop, Amin curAmin, Element[] resInputFunc, int key) {
        int mainComp = drop.numberOfMainComponentsAtOutput * 3;
        int end = drop.arcs[0].length;

        if (key == 1) {
            mainComp = 0;
        } else if (key == 2) {
            end = mainComp;
            mainComp = 0;
        }

        for (int i = mainComp; i < end; i += 3) {

            int numOfDependantDrop = drop.arcs[0][i];
            int from = drop.arcs[0][i + 1];
            int to = drop.arcs[0][i + 2];

            if (drop.arcs[numOfDependantDrop].length != 0) {
                DropTask dependantDrop = curAmin.branch.get(numOfDependantDrop - 1);

                dependantDrop.inData[to] = resInputFunc[from];

                //LOGGER.info("dependantDrop = " + dependantDrop);
               /* if (dependantDrop.hasFullInputData() && dependantDrop.isItLeaf()) {
                    //LOGGER.info("in writeResultsAfterInpFunc write to GLLD = " + dependantDrop.number);
                    LeavesStack.push(dependantDrop);
                }*/
                if ((dependantDrop.hasMainInputData()&& !dependantDrop.isItLeaf())|| dependantDrop.hasFullInputData()) {
                    putDropInVokzal(dependantDrop);
                }

            } else if (key != 2) {

                Amin amin = pine.get(drop.getNumbOfMyAmine());
                amin.resultForOutFunction[to] = resInputFunc[from];

                if (amin.hasFullOutput()) {
                    // LOGGER.info("drop.getNumbOfMyAmine() "+ drop.getNumbOfMyAmine());
                    // LOGGER.info("go to this1 "+ resInputFunc[from]);
                    putResultsToAminOutput(amin);
                }

            }
        }
    }

    private void addToAerodromeResults(DropTask dropRes) {
        synchronized (aerodromeResults) {
            // LOGGER.warn("put amin num = " + amin.aminIdInPine);
            aerodromeResults.add(dropRes);
        }
    }

    private void putResultsToAminOutput(Amin amin) {
      //  LOGGER.trace("putResultsToAminOutput");
        amin.outputData = DropTask.getDropObject(amin.type, amin.config).outputFunction(amin.resultForOutFunction, ring);

        if (amin.parentAmin == -1 && myRank == 0 && Array.isEmptyArray(vokzal)) {
            LOGGER.trace("go finish");
            finishWholeTask(amin.outputData);
        } else if (amin.parentProc != myRank) {
            DropTask resultAmine = DropTask.doNewDrop(amin.type, amin.config, amin.aminIdInPine, amin.parentDrop, amin.parentProc, amin.recNumb, amin.inputData);
            resultAmine.outData = amin.outputData;
            resultAmine.setNumbOfMyAmine(amin.parentAmin);
            addToAerodromeResults(resultAmine);
        } else {
            DropTask dr = pine.get(amin.parentAmin).branch.get(amin.parentDrop);
            dr.outData = amin.outputData;
            putDropInTrack(dr);

            int aminIndex = pine.indexOf(amin);
            if (aminIndex != -1)
                pine.set(aminIndex, null);

            currentMemory = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
            if (currentMemory > DispThread.usedMemory)
                DispThread.usedMemory = currentMemory;
        }

    }

    private void finishWholeTask(Element[] outputData) {
        result = outputData;
        finish = true;
    }


    public boolean setIfFullDrop(DropTask drop) {
        drop.fullDrop = drop.hasFullInputData();
        return drop.fullDrop;
    }

    synchronized public DropTask getTask(int thread) {
//        LOGGER.warn("GET Task = "+thread);
        boolean empty = false;
     //   LOGGER.trace("mylevel " + DispThread.myLevel + " myLevelH = " + DispThread.myLevelH);

       // if(vokzal[DispThread.myLevelH].size() == 0) changeMyLevelH();
       // if(vokzal[DispThread.myLevel].size() == 0)changeMyLevel();
    //    LOGGER.trace("after mylevel " + DispThread.myLevel + " myLevelH = " + DispThread.myLevelH);
        if (DispThread.isEmptyVokzal()) {
      //     LOGGER.trace("Vokzal is empty");
            empty = true;
        }

        DropTask drop = null;
        ArrayList<DropTask> list;
        boolean getFromTrack = false;

        if (thread == 0) {
            if (ownTrack.size() != 0) {
                // for (int i = 0; i < ownTrack.size(); i++) {
                drop = ownTrack.get(0);
                deleteFromTrack(drop);
                // getFromTrack = true;
                //    break;
                // }
            } else if (/*!getFromTrack &&*/ !empty) {

             //   LOGGER.trace("after change DispThread.myLevelH = " + DispThread.myLevelH+ "empty vokzal " + Tools.isEmptyArray(vokzal));
                list = vokzal[DispThread.myLevelH];
                drop = list.get(0);
                list.remove(0);
                if (vokzal[DispThread.myLevelH].size() == 0) {
                    changeMyLevelH();
                }
                if (drop.numberOfDaughterProc == -1) {
                    drop.numberOfDaughterProc = myRank;

                    // if(myRank == 0 && drop.type == 1)
                    //   LOGGER.info("created amin by drop " + drop);
//                    LOGGER.warn("START");
                    setIfFullDrop(drop);
//                    LOGGER.warn("FINISH");
                }
            }
        } else if (thread == 1 && !empty) {
            list = vokzal[DispThread.myLevel];
            LOGGER.trace(String.format("vokzal level = " + DispThread.myLevel + DispThread.myLevelH
                    +" empty = " +Array.isEmptyArray(vokzal)) + "list size = "+ list.size());
            if(list.size() == 0)  { changeMyLevel();  list = vokzal[DispThread.myLevel];}
            if(DispThread.isEmptyVokzal()) return null;
            drop = list.get(0);
            list.remove(0);
            if (vokzal[DispThread.myLevel].size() == 0) {
                changeMyLevel();
            }
        }

        return drop;
    }


    private void changeMyLevelH() {
        while (DispThread.myLevelH != 0 && DispThread.myLevelH >= DispThread.myLevel && vokzal[DispThread.myLevelH].size() == 0) {
            DispThread.myLevelH--;
        }
        if (vokzal[DispThread.myLevelH].size() == 0) {
            DispThread.myLevelH = 20;
            DispThread.myLevel = 20;
        }
       // if(changeTrack) changeTrackLevel();


    }

    private void changeMyLevel() {
        DispThread.myLevel++;
        while (DispThread.myLevelH >= DispThread.myLevel && vokzal[DispThread.myLevel].size() == 0) {
            DispThread.myLevel++;
        }
        if (DispThread.myLevel > DispThread.myLevelH) {
            DispThread.myLevel = 20;
            DispThread.myLevelH = 20;
        }
        //if(changeTrack) changeTrackLevel();

    }

    private void changeTrackLevel() {
        //LOGGER.trace("ownTrack.size" + ownTrack.size());
        synchronized (ownTrack) {
            DispThread.trackLevel = ownTrack.size() == 0 ? 20 : ownTrack.stream().min(Comparator.comparing(DropTask::getRecNum)).get().recNum;
            changeTrack = false;
        }
    }

    private void deleteFromTrack(DropTask drop) {
        ownTrack.remove(drop);
        if(drop.recNum == DispThread.trackLevel)
            changeTrackLevel();
    }


   /* private void processingTheWholeLeavesStack() {
        //LOGGER.info("GLLD bef proces = " + LeavesStack.size());
        while (!LeavesStack.isEmpty()) {
            DropTask leafDrop = LeavesStack.pop();
            leafDrop.sequentialCalc(ring);
            writeResultsToAmin(leafDrop);
        }
        //LOGGER.info("GLLD after proces = " + LeavesStack.size());
    }*/

    public void inputDataToAmin() throws MPIException {
        //LOGGER.info("In inputDataToAmin");
        int component3Index = 0;
        Amin curAmin = null;
        Element[] resInputFunc;
//        LOGGER.warn("Pine size = " +pine.size());

        if (currentDrop.type != -1) {
            curAmin = new Amin(currentDrop.type, currentDrop.config, currentDrop.procId, currentDrop.aminId, currentDrop.dropId, pine.size(), myRank, currentDrop.recNum + 1);
            pine.add(curAmin);
            currentDrop.setNumbOfMyAmine(curAmin.aminIdInPine);
            System.arraycopy(currentDrop.inData, 0, curAmin.inputData, 0, curAmin.inputData.length);

            // LOGGER.info("currentDrop num = " + currentDrop.number);
            if (currentDrop.inData.length != currentDrop.numberOfMainComponents) {
                //если пришла задача со всеми входними даними, записиваем неглавние компоненти в список для виходной функции
                if (currentDrop.hasAdditionalInputData()) {
                    component3Index = 1;
                } else {
                    component3Index = 2;
                }
            } else {
                //для дропов которие не имеют дополнительних индекс= 1
                component3Index = 1;
            }

            resInputFunc = currentDrop.inputFunction(curAmin.inputData, component3Index, ring);
        } else {
            component3Index = 3;
//            LOGGER.warn("fetched drop "+currentDrop);
            curAmin = pine.get(currentDrop.getNumbOfMyAmine());
            resInputFunc = DropTask.getDropObject(curAmin.type, curAmin.config).inputFunction(currentDrop.inData, component3Index, ring);
        }
        writeResultsAfterInpFunc(currentDrop, curAmin, resInputFunc, component3Index);
    }


    @Override
    public void run() {
        long calcWaitTimeStart, calcWaitTimeEnd, calcWorkTimeStart, calcWorkTimeEnd;
        calcWaitTimeStart = calcWaitTimeEnd = calcWorkTimeStart = calcWorkTimeEnd = System.currentTimeMillis();

        while (!flToExit) {
           // ++counterCycle;
          // if(myRank==0) LOGGER.trace("in calc cycle owntrack size = "+ownTrack.size()+ " vokzalempty = " +  DispThread.isEmptyVokzal());
            if (/*LeavesStack.isEmpty() && */ownTrack.size() == 0 && /*Tools.isEmptyArray(vokzal)*/DispThread.isEmptyVokzal()) {
                if (!IamFree) {
                    IamFree = true;
                }
                continue;
            } else {
                IamFree = false;
                calcWaitTimeEnd = System.currentTimeMillis();
                calcWaitTime+=calcWaitTimeEnd-calcWaitTimeStart;
            }
            try {
                calcWorkTimeStart = System.currentTimeMillis();
                ProcFunc();
                calcWorkTimeEnd = System.currentTimeMillis();
                calcWorkTime += calcWorkTimeEnd - calcWorkTimeStart;
                calcWaitTimeStart = System.currentTimeMillis();
            } catch (MPIException e) {
                e.printStackTrace();
            }
        }
    }
    private void ProcFunc() throws MPIException {
       // LOGGER.trace("go to get task");
        currentDrop = getTask(0);
        if (currentDrop != null) {
          //  LOGGER.trace("get drop number = " + currentDrop.number + " type = " + currentDrop.type + " recv = "+currentDrop.recNum);
            if (!Array.isEmpty(currentDrop.outData)) {
             //   LOGGER.trace("Drop result");
                writeResultsToAmin(currentDrop);
                if(myRank==0) {
               //     LOGGER.trace("empty vokzal = " + Tools.isEmptyArray(vokzal) +
            //                "mylevel = "+DispThread.myLevel + " levelh = "+DispThread.myLevelH
           //         + "size on lev"+vokzal[DispThread.myLevel].size() +" size on h"+ vokzal[DispThread.myLevelH].size() );
                   /* for (Amin amin:pine) {
                        if(amin!=null &&!amin.hasFullOutput()){
                           LOGGER.trace("aminid = "+amin.aminIdInPine + " type = " + amin.type + " size = "+amin.branch.size());
                        for (DropTask drop:amin.branch) {LOGGER.trace("id = " + drop.dropId + " outdata empty " +Tools.isEmpty(drop.outData)
                                + " myamine = " +drop.getNumbOfMyAmine() + " daughtproc = " + drop.numberOfDaughterProc );}
                        }
                    }
                    */
                }
            } else {
                if (currentDrop.isItLeaf()) {
               //     LOGGER.trace("Drop is leaf " + currentDrop.aminId + " id = "+ currentDrop.dropId);
                    currentDrop.sequentialCalc(ring);

                   /* for (int i = 0; i <currentDrop.inputDataLength ; i++) {
                        LOGGER.trace("after currentDrop.INdata = " + currentDrop.inData[i]);

                    }*/

                   /* for (int i = 0; i <currentDrop.outData.length ; i++) {

                        LOGGER.trace("after currentDrop.outdata = " + currentDrop.outData[i]);
                    }*/

                    if (currentDrop.aminId == -1 && myRank == 0) {
                       // LOGGER.trace("go to finish whole task");
                        finishWholeTask(currentDrop.outData);
                    } else if (currentDrop.procId == myRank) {

                        writeResultsToAmin(currentDrop);
                     //   LOGGER.trace("after writeResultsToAmin");

                    } else {
                        //LOGGER.trace(" bef add aerodrome results");
                        addToAerodromeResults(currentDrop);
                    }
                  //  LOGGER.trace("after drop is leaf vokzal empty = " + Tools.isEmptyArray(vokzal));
                } else {
                //   LOGGER.trace("drop is not a leaf");
                    inputDataToAmin();
                }
            }
        }
     //   LOGGER.trace("here");
        return;
    }
}
