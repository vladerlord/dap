/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package com.mathpar.students.ukma17i41.bosa.parallel.engine;
package com.mathpar.parallel.dap.core;

import com.mathpar.parallel.dap.matrixD.matrDDrop.MatrDMult;
import com.mathpar.parallel.dap.matrixD.matrDDrop.MatrDMultAdd;
import com.mathpar.parallel.dap.matrixD.matrDStrassDrop.matrDMultStrass;
import com.mathpar.parallel.dap.matrixD.matrDStrassWinDrop.*;
import com.mathpar.log.MpiLogger;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.QR.QPDecomposition;
import com.mathpar.parallel.dap.QR.QRDecomposition;
import com.mathpar.parallel.dap.QR.Utils;
import com.mathpar.parallel.dap.cholesky.Cholesky;
import com.mathpar.parallel.dap.inverseTriangle.Inversion;
import com.mathpar.parallel.dap.multiply.Multiply;
import com.mathpar.parallel.dap.multiply.MultiplyAdd;
import com.mathpar.parallel.dap.multiply.MultiplyExtended;
import com.mathpar.parallel.dap.multiply.MultiplyMinus;
import com.mathpar.parallel.dap.multiply.multiplyVar.MultiplyVar;
import com.mathpar.parallel.dap.multiply.multiplyVar.MultiplyVarConfig;

import java.io.Serializable;
import java.util.ArrayList;



public abstract class DropTask implements Serializable {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(DropTask.class);
    /**inData - an array of input data to which the initial task data is transmitted
     *
     * outData - the output array of the drop is the result.
     *
     * numberOfMainComponents - the number of main components that the drop must contain.
     *
     * numberOfMainComponentsAtOutput - the number of links in arcs in the array of the input function for the main components.
     * The input function array can also contain links for additional components
     *
     * arcs - an array of dependencies in a graph (arcs) is a two-dimensional array whose positions are drop numbers in the graph.
     * This array shows the connections in the graph between the drops. Each array contains the drop number, which depends on the current (on its output data),
     * the position in the output list of the current drop and the position in the input list of the drop, which depends on it, ie where and where to write data.
     * The zero element of the array is responsible for the input function of the drop, and the latter for the output, and it is always empty.
     *
     * type - drop type. Each drop has a corresponding type of numeric value, which indicates the type of task it performs.
     * Each drop of the appropriate type must inherit the abstract Drop-Task class and implement its methods according to its specification.
     * With the appearance of a new type of drop, it is necessary to take the following, not yet used numerical value for it.
     *
     * resultForOutFunctionLength - the length of the input array for the output function of the drop.
     *
     * inputDataLength - the length of the input array of the drop (the amount of all input data).
     *
     * numberOfDaughterProc - the number of the child processor to which this drop was sent for calculation.
     * It is necessary that in case we sent a drop task only with the main components,
     * it was known to whom to send other data for calculation. By default, this value is -2.
     * The field can acquire the following values:
     * - 2 - the drop has not yet been added to the list of available tasks and has not been sent to the child processor
     * - 1 - drop added to the list of available, but not yet sent;
     * - 0..n - the number of the processor to which this drop was sent.
     *
     * aminId - the number of the amine in which this drop is located.
     * This is necessary in order to quickly find out the number of the amine and the number drop from which the task is taken.
     * First, this value is -1, after the creation of the amine, its droplets write the number (position) on the pine
     *
     * myAmin - the number of the amine on the pine, which was deployed from this drop
     *
     * dropId - the number of this drop in the amine
     *
     * procId - the number of the processor in which this drop was created and is on its pine.
     *
     * recNum - recursion level - at what level the solution of the problem is (calculated according to the amount of input data).
     *
     * number - unique number of drop
     *
     * fullDrop - boolean variable, which shows whether the drop is complete, ie whether it has all input components (main and additional).
     *
     */
    protected Element[] inData;
    protected Element[] outData;
    protected int numberOfMainComponents;
    protected int numberOfMainComponentsAtOutput;
    protected int[][] arcs;
    protected int type;
    protected int resultForOutFunctionLength;
    protected int inputDataLength;
    protected int numberOfDaughterProc = -2;
    protected int aminId = -1;
    private int myAmin = -1;
    protected int dropId = -1;
    protected int procId = -1;
    protected int recNum = 0;
    protected int number;
    boolean fullDrop = false;
    protected static int cnum = 0;
    protected byte[] config;


    public abstract ArrayList<DropTask> doAmin();
    public abstract void sequentialCalc(Ring ring);
    public abstract Element[] inputFunction(Element[] input, int inputKey, Ring ring);
    public abstract Element[] outputFunction(Element[] input, Ring ring);
    public abstract boolean isItLeaf();

    void setNumbOfMyAmine(int numOfAmin)
    {
        myAmin = numOfAmin;
    }

    int getNumbOfMyAmine()
    {
        return myAmin;
    }

    int getRecNum() {return recNum;}

    public boolean hasFullInputData(){
        return checkExistance(0, inputDataLength, true);
    }

    public boolean hasMainInputData(){
        return checkExistance(0, numberOfMainComponents, true);
    }

    public boolean hasAdditionalInputData(){
        return checkExistance(numberOfMainComponents, inputDataLength, canHaveAdditionalData());
    }

    public boolean canHaveAdditionalData(){
        return inputDataLength != numberOfMainComponents;
    }

    private boolean checkExistance(int start, int end, boolean positiveResult){
        for (int j = start; j < end; j++) {
           // LOGGER.trace("inData[j] = " + inData[j]);
            if (inData[j] == null) {
                return false;
            }
        }

        return positiveResult;
    }

    public abstract void setLeafSize(int dataSize);

     @Override
        public String toString() {
           String str = "";
//           for (Element i: inData) {
//                 str +=i + " ";
//           }
//            str+= " OutData = ";
//           for (Element i: outData) {
//                 str +=i + " ";
//           }
            str+= Utils.matrixSArrayToString(inData);
            str+= Utils.matrixSArrayToString(outData);
           str+= " type = "+type;
           str+= " numberOfDaughterProc = "+numberOfDaughterProc;
           str+= " myAmin = "+myAmin;
           str+= " dropId = "+dropId;
           str+= " aminId = "+aminId;
           str+= " procId = "+procId;
           str+= " number = "+number;
           return str;
        }


    @Override
    public boolean equals(Object obj) {
        if (obj == this) { return true; }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        DropTask drop = (DropTask) obj;

        return dropId == drop.dropId && aminId == drop.aminId ;// && procId == drop.procId;
    }

    public static DropTask doNewDrop(
            int type,
            byte[] config,
            int aminId,
            int dropId,
            int procId,
            int recNum,
            Element[] data
    ) {
        DropTask newDrop = getDropObject(type, config);
        newDrop.aminId = aminId;
        newDrop.dropId = dropId;
        newDrop.procId = procId;
        newDrop.recNum = recNum;
        System.arraycopy(data, 0, newDrop.inData, 0, data.length);
        return newDrop;
    }

    /**
     Стандартное умножение матриц в классе MatrіxS
     Multiply type = 0 ++  (AB)

     Стандартное умножение матриц в классе MatrixS вместе с сложением третьей матрицы
     MultiplyAdd type = 1  (AB+C)

     Стандартное умножение матриц в классе MatrixS со сменой знака всех элементов
     MultiplyMinus type = 2  (-AB)

     Расширенное стандартное умножение матриц в классе MatrixS
     * (от третьей матрицы отнимает результат умножения)
     MultiplyExtended type = 3  (B-AA^T)

     Обращение треугольной матрицы в классе MatrixS
     Inversion type = 4

     Разложение Холецкого в классе MatrixS
     Cholesky type = 5

     Стандартное умножение матриц в классе MatrixS с конфигурациями
     MultiplyVar type = 6 (Ilya  QR)

     Разложение матриц в классе MatrixS QP
     QPDecomposition type = 7

     Разложение матриц в классе MatrixS QR
     QRDecomposition type = 8

     Умножение матриц за Штрассеном в классе MatrixD
     matrDMultStrass type = 9

     Стандартное умножение матриц в классе MatrixD
     MatrDMult type = 10

     Стандартное умножение матриц в классе MatrixD вместе с сложением третьей матрицы
     MatrDMultAdd type = 11 (AB+C)

     Умножение матриц за Виноград-Штрассеном в классе MatrixD
     matrDMultStrassWin type = 12 (!!!)

     Умножение матриц за Виноград-Штрассеном в классе MatrixD со сменой знака всех элементов
     matrDMultStrassWinNegate type = 13   (-AB)

     Умножение матриц за Виноград-Штрассеном в классе MatrixD типа С-A*B
     matrDMultStrassWinSub type = 14 (B-A*A^T)

     Обращение треугольной матрицы в классе MatrixS за Виноград-Штрассеном
     matrDTriangInvStrassWin type = 15

     Разложение Холецкого в классе MatrixD с использованием умножения за Виноград-Штрассеном
     matrDCholFactStrassWin type = 16

     */
    //public final static int multiplyAdd = 1;
    public static DropTask getDropObject(int type, byte[] config) {
        DropTask drop = null;
        switch (type) {
            case 0:
                drop = new Multiply();
                break;
            case 1:
                drop = new MultiplyAdd();
                break;
            case 2:
                drop = new MultiplyMinus();
                break;
            case 3:
                drop = new MultiplyExtended();
                break;
            case 4:
                drop = new Inversion();
                break;
            case 5:
                drop = new Cholesky();
                break;
            case 6:
                drop = new MultiplyVar(new MultiplyVarConfig(config));
                break;
            case 7:
                drop = new QPDecomposition();
                break;
            case 8:
                drop = new QRDecomposition();
                break;
            case 9:
                drop = new matrDMultStrass();
                break;
            case 10:
                drop = new MatrDMult();
                break;
            case 11:
                drop = new MatrDMultAdd();
                break;
            case 12:
                drop = new matrDMultStrassWin();
                break;
            case 13:
                drop = new matrDMultStrassWinNegate();
                break;
            case 14:
                drop = new matrDMultStrassWinSub();
                break;
            case 15:
                drop = new matrDTriangInvStrassWin();
                break;
            case 16:
                drop = new matrDCholFactStrassWin();
                break;
        }

        return drop;

    }
}
