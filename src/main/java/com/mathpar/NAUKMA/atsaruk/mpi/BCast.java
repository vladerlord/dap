package com.mathpar.NAUKMA.atsaruk.mpi;

import java.util.Random;
import mpi.MPI;
import mpi.MPIException;

public class BCast {

  public static void main(String[] args) {
    try {
      MPI.Init(args);
      int myrank = MPI.COMM_WORLD.getRank();
      int n = Integer.parseInt(args[0]);
      double[] a;
      if (myrank == 0) {
        a = new double[n - 4];
      } else {
        a = new double[n];
      }

      if (myrank == 0) {
        for (int i = 0; i < n - 4; i++) {
          a[i] = new Random().nextDouble();
          System.out.print("Snd R[" + MPI.COMM_WORLD.getRank() + "] a[" + i + "]= " + a[i] + System
              .lineSeparator());
        }
      }
      MPI.COMM_WORLD.barrier();
      MPI.COMM_WORLD.bcast(a, a.length, MPI.DOUBLE, 0);
//    if (myrank != 0) {
      for (int i = 0; i < n; i++) {
        System.out.print("Rec R[" + MPI.COMM_WORLD.getRank() + "] a[" + i + "]= " + a[i] + System
            .lineSeparator());
      }
//    }
      MPI.Finalize();
    } catch (MPIException e) {
      System.out.println("Error: " + e.getMessage());
      System.out.println("Stack: ");
      e.printStackTrace(System.out);
    }
  }
}
