package com.mathpar.NAUKMA.atsaruk.mpi.matrix.data;

public class Matrix<T extends Number> {

  private T[][] data;

  public Matrix(T[][] data) {
    this.data = data;
  }

  public T[][] getData() {
    return data;
  }
}
