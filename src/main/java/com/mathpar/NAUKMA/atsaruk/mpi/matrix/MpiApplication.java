package com.mathpar.NAUKMA.atsaruk.mpi.matrix;

import static java.lang.String.format;

import com.mathpar.NAUKMA.atsaruk.mpi.matrix.data.Matrix;
import mpi.MPI;
import mpi.MPIException;

public class MpiApplication {

  public static void main(String[] args) throws MPIException {
    MPI.Init(args);

    if (MPI.COMM_WORLD.getRank() == 0) {
      Matrix<Integer> matrixA = new Matrix<>(
          new Integer[][]{{1, 2, 3, 4}, {2, 3, 4, 5}, {3, 4, 5, 6}, {4, 5, 6, 7}});
      Matrix<Integer> matrixB = new Matrix<>(
          new Integer[][]{{1, 2, 3, 4}, {2, 3, 4, 5}, {3, 4, 5, 6}, {4, 5, 6, 7}});



    }

    System.out.println(format("R[%d]: Hello", MPI.COMM_WORLD.getRank()));
    MPI.Finalize();
    System.out.println("After Hello");
  }
}
