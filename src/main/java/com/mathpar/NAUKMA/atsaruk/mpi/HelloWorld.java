package com.mathpar.NAUKMA.atsaruk.mpi;

import com.mathpar.log.MpiLogger;
import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;

// EXAMPLE FOR DEBUGGER !!!! Terminal opened in main project faulder !!!
//./runclass.sh -np 9 com.mathpar.students.ukma.atsaruk.mpi.HelloWorld

public class HelloWorld {

  private final static MpiLogger LOGGER = MpiLogger.getLogger(HelloWorld.class);

  public static void main(String[] args) throws MPIException {
    MPI.Init(args);

    final Intracomm CW = MPI.COMM_WORLD;
//    5 levels of debugger messages: error, warn,info, debug, trace
    LOGGER.error("ERROR Level example: Name: " + CW.getName());
    LOGGER.warn("WARN Level log example");
    LOGGER.info("INFO Level log example");
    LOGGER.debug("DEBUG Level log Hello From logger");
    LOGGER.trace("TRACE Level log Hello From logger");

    MPI.Finalize();
  }
}
