package com.mathpar.NAUKMA.masters_1.Morenets;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.test.InversionTestManual;
import org.javatuples.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

public class InversionTestSequential {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(InversionTestManual.class);

    public static void main(String[] args) {
        Ring ring = new Ring("R64[]");
        ring.setAccuracy(300);
        ring.setMachineEpsilonR64(290);

        int testsAmount = 20;

        List<Pair<Function<MatrixS, MatrixS>, String>> implementations =
            new ArrayList<Pair<Function<MatrixS, MatrixS>, String>>() {{
                add(new Pair<>(
                    ((matrix) -> TriangularInversionTools.inverseRecursively(matrix, ring)),
                    "inverseTriangleRecursive"
                ));
//                add(new Pair<>(
//                    ((matrix) -> TriangularInversionTools.inverse(matrix, ring)),
//                    "inverseTriangle"
//                ));
            }};

        for (int size = 8; size <= 1024; size *= 2) {
            for (Pair<Function<MatrixS, MatrixS>, String> impl : implementations) {
                Pair<Double, Double> res =
                    testImplementation(impl.getValue0(), size, testsAmount, ring);

                LOGGER.info("Implementation {}", impl.getValue1());
                LOGGER.info("Size: {}", size);
                LOGGER.info("Avg time: {}", res.getValue0());
                LOGGER.info("Avg error: {} \n", res.getValue1());
            }
        }
    }

    private static Pair<Double, Double> testImplementation(
        Function<MatrixS, MatrixS> funToTest,
        int size,
        int testsAmount,
        Ring ring
    ) {
        long[] time = new long[testsAmount];
        double[] errors = new double[testsAmount];

        for (int testNum = 0; testNum < testsAmount; testNum++) {
            MatrixS matrix = TriangularInversionTools.generateMatrix(size, 100, 3, ring);

            Pair<Long, Element> res = runTest(funToTest, matrix, ring);

            time[testNum] = res.getValue0();
            errors[testNum] = res.getValue1().doubleValue();
        }

        return new Pair<>(
            Arrays.stream(time).average().orElse(Double.NaN),
            Arrays.stream(errors).average().orElse(Double.NaN)
        );
    }

    private static Pair<Long, Element> runTest(
        Function<MatrixS, MatrixS> funToTest,
        MatrixS testMatrix,
        Ring ring
    ) {
        long start = System.currentTimeMillis();
        MatrixS inverted = funToTest.apply(testMatrix);
        long end = System.currentTimeMillis();

        Element error = testMatrix.multiply(inverted, ring)
                                  .subtract(TriangularInversionTools.identity(
                                      testMatrix.size,
                                      ring
                                  ), ring)
                                  .max(ring);

        return new Pair<>(end - start, error);
    }
}
