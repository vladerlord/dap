package com.mathpar.NAUKMA.masters_1.Morenets;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;

import java.util.Random;

public class TriangularInversionTools {
    /**
     * Get a triangular matrix for inversion
     *
     * @return a triangular invertible matrix
     */
    public static MatrixS generateMatrix(
        int size,
        int density,
        int maxBitsForElements,
        Ring ring
    ) {
        int[] randomType = new int[]{maxBitsForElements};
        Random rng = new Random();
        MatrixS mat = new MatrixS(size, size, density, randomType, rng, ring.numberONE(), ring);

        for (int row = 0; row < size; row++) {
            for (int col = row + 1; col < size; col++) {
                mat.putElement(ring.numberZERO, row, col);
            }
        }

        // protect from diagonal zeroes because such matrix has determinant = 0 and is NOT invertible
        for (int diag = 0; diag < size; diag++) {
            Element elem;
            while ((elem = mat.getElement(diag, diag, ring)).isZero(ring)) {
                mat.putElement(elem.random(randomType, rng, ring), diag, diag);
            }
        }

        return mat;
    }


    /**
     * Get an inverse of triangular matrix
     *
     * @param ring to inverse the matrix over
     * @return the inverted matrix
     */
    public static MatrixS inverse(MatrixS matrix, Ring ring) {
        MatrixS res = matrix.copy();

        for (int submatrixSize = 1; submatrixSize <= matrix.size; submatrixSize *= 2) {
            for (int diag = 0; diag < matrix.size; diag += submatrixSize) {
                MatrixS sub = getSquareSubmatrix(res, diag, diag, submatrixSize);

                MatrixS invertedBottomLeft;
                if (sub.size == 1) {
                    invertedBottomLeft = inverse1x1(matrix, ring);
                } else {
                    MatrixS[] blocks = sub.split();
                    invertedBottomLeft = inverseBottomLeft(blocks[0], blocks[3], blocks[2], ring);
                }

                setSubmatrix(res, invertedBottomLeft, diag + submatrixSize / 2, diag, ring);
            }
        }

        return res;
    }

    /**
     * Get an inverse of a triangular matrix recursively
     *
     * @param ring to invert the matrix over
     * @return the inverted matrix
     */
    public static MatrixS inverseRecursively(MatrixS matrix, Ring ring) {
        if (matrix.size == 1) {
            return inverse1x1(matrix, ring);
        }

        MatrixS[] blocks = matrix.split();
        MatrixS invertedTopLeft = inverseRecursively(blocks[0], ring);
        MatrixS invertedBottomRight = inverseRecursively(blocks[3], ring);
        MatrixS invertedBottomLeft =
            inverseBottomLeft(invertedTopLeft, invertedBottomRight, blocks[2], ring);

        return join(
            invertedTopLeft,
            blocks[1],
            invertedBottomLeft,
            invertedBottomRight
        );
    }

    /**
     * Invert 1x1 matrix over a ring
     *
     * @return inverse of the matrix
     */
    private static MatrixS inverse1x1(MatrixS matrix, Ring ring) {
        if (matrix.size != 1 || matrix.colNumb != 1) {
            throw new IllegalArgumentException("Matrix has to be 1x1");
        }

        return new MatrixS(matrix.getElement(0, 0, ring).inverse(ring));
    }

    /**
     * Invert the bottom left part of the matrix given already inverted top left and bottom right parts
     * and an initial bottom left part
     * <p>
     * a b
     * c d
     * <p>
     * c is the bottom left
     *
     * @return inverse of the bottom left part
     */
    private static MatrixS inverseBottomLeft(
        MatrixS invertedTopLeft,
        MatrixS invertedBottomRight,
        MatrixS bottomLeft,
        Ring ring
    ) {
        return invertedBottomRight.multiply(bottomLeft, ring)
                                  .multiply(invertedTopLeft, ring)
                                  .negate(ring);
    }

    /**
     * Создание матрицы из четырех квадратных блоков одинакового размера:
     * a b
     * c d
     *
     * @return матрица, полученная из данных четырех блоков
     */
    private static MatrixS join(MatrixS a, MatrixS b, MatrixS c, MatrixS d) {
        return MatrixS.join(new MatrixS[]{a, b, c, d});
    }

    /**
     * Set part of a matrix to the elements of another matrix
     */
    private static void setSubmatrix(
        MatrixS matrix,
        MatrixS submat,
        int startRow,
        int startCol,
        Ring ring
    ) {
        for (int row = 0; row < submat.size && row + startRow < matrix.size; row++) {
            for (int col = 0; col < submat.colNumb && col + startCol < matrix.colNumb; col++) {
                matrix.putElement(
                    submat.getElement(row, col, ring),
                    row + startRow,
                    col + startCol
                );
            }
        }
    }

    /**
     * Get a square submatrix of a matrix
     *
     * @return the submatrix
     */
    private static MatrixS getSquareSubmatrix(
        MatrixS matrix,
        int startRow,
        int startCol,
        int size
    ) {
        return matrix.getSubMatrix(
            startRow,
            startRow + size - 1,
            startCol,
            startCol + size - 1
        );
    }

    /**
     * Сгенерировать единичную матрицу
     */
    public static MatrixS identity(int size, Ring ring) {
        MatrixS identity =
            new MatrixS(size, size, 0, new int[]{0}, new Random(), ring.numberONE(), ring);

        for (int diag = 0; diag < size; diag++) {
            identity.putElement(ring.numberONE, diag, diag);
        }

        return identity;
    }
}
