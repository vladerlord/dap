package com.mathpar.NAUKMA.masters_1.Morenets;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.test.DAPTest;
import mpi.MPIException;
import org.javatuples.Pair;

/**
 * @author Ihor Morenets
 */
public class InversionTest extends DAPTest {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(InversionTest.class);

    protected InversionTest(Ring defaultRing) {
        super("inversion", 3);

        this.ring = defaultRing;
    }

    public static void main(String[] args) throws InterruptedException, MPIException {
        Ring defaultRing = new Ring("R[]");
        // defaultRing.setAccuracy(200);
        new InversionTest(defaultRing).runTests(args);
    }

    @Override
    protected MatrixS[] initData(int size, int density, int maxBitsForElements, Ring ring) {
        return new MatrixS[]{matrix(size, density, maxBitsForElements, ring)};
    }

    @Override
    protected Pair<Boolean, Element> checkResult(
        DispThread dispThread,
        String[] args,
        Element[] initData,
        Element[] resultData,
        Ring ring
    ) {
        MatrixS initial = (MatrixS) initData[0];
        MatrixS inverted = (MatrixS) resultData[0];

        Element[] results = runTask(dispThread, 0, args, new Element[]{initial, inverted}, ring);
        MatrixS check = (MatrixS) results[0];
        MatrixS errors = check.multiply(inverted, ring)
                              .subtract(
                                  com.mathpar.NAUKMA.masters_1.Morenets.TriangularInversionTools.identity(initial.size, ring),
                                  ring
                              );

        boolean isFlawless = errors.isZero(ring);
        Element maxError = errors.max(ring);

        return new Pair<>(isFlawless, maxError);
    }

    @Override
    protected MatrixS matrix(int size, int density, int maxBitsForElements, Ring ring) {
        return TriangularInversionTools.generateMatrix(size, density, maxBitsForElements, ring);
    }
}
