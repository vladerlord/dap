package com.mathpar.NAUKMA.masters_1.Androshchuk;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Ring;
import com.mathpar.parallel.utils.MPITransport;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;

public class VynogradStrassenMultiplicationTool {
  //  mpirun -np 7 --oversubscribe java -cp /home/vitaly/IdeaProjects/dap/target/classes com.mathpar.NAUKMA.masters_1.Androshchuk.VynogradStrassenMultiplicationTest

    public static MatrixS multiply(MatrixS mathparA, MatrixS mathparB, Ring ring) throws MPIException, IOException, ClassNotFoundException {
        //визначення номеру процесора
        int myrank = MPI.COMM_WORLD.getRank();

        MatrixS result = null;
        switch (myrank) {
            case 0:
                result = processRoot(mathparA, mathparB, ring);
                break;
            case 1:
                process1(ring, myrank);
                break;
            case 2:
                process2(ring, myrank);
                break;
            case 3:
                process3(ring, myrank);
                break;
            case 4:
                process4(ring, myrank);
                break;
            case 5:
                process5(ring, myrank);
                break;
            case 6:
                process6(ring, myrank);
                break;
        }
        return result;
    }

    private static void process6(Ring ring, int myrank) throws IOException, MPIException, ClassNotFoundException {
        muliplyReceived(ring, myrank);
        return;
    }

    private static void muliplyReceived(Ring ring, int myrank) throws IOException, MPIException, ClassNotFoundException {
        Object[] n = new Object[2];
        MPITransport.recvObjectArray(n, 0, 2, 0, myrank);
        MatrixS a = (MatrixS) n[0];
        MatrixS b = (MatrixS) n[1];
        MatrixS res = a.copy().multiply((b.copy()), ring);
        MPITransport.sendObject(res, 0, myrank);
    }

    private static void process5(Ring ring, int myrank) throws IOException, MPIException, ClassNotFoundException {
        muliplyReceived(ring, myrank);
    }

    private static void process4(Ring ring, int myrank) throws IOException, MPIException, ClassNotFoundException {
        muliplyReceived(ring, myrank);
    }

    private static void process3(Ring ring, int myrank) throws IOException, MPIException, ClassNotFoundException {
        muliplyReceived(ring, myrank);
    }

    private static void process2(Ring ring, int myrank) throws IOException, MPIException, ClassNotFoundException {
        muliplyReceived(ring, myrank);
    }

    private static void process1(Ring ring, int myrank) throws IOException, MPIException, ClassNotFoundException {
        muliplyReceived(ring, myrank);
    }

    private static MatrixS processRoot(MatrixS mathparA, MatrixS mathparB, Ring ring) throws IOException, MPIException, ClassNotFoundException {
        // матриця A на 4 частини
        MatrixS[] splittedA = mathparA.split();
        // матриця B на 4 частини
        MatrixS[] splittedB = mathparB.split();

        //Проміжні змінні для зручності
        MatrixS a11 = splittedA[0];
        MatrixS a12 = splittedA[1];
        MatrixS a21 = splittedA[2];
        MatrixS a22 = splittedA[3];

        //Проміжні змінні для зручності
        MatrixS b11 = splittedB[0];
        MatrixS b12 = splittedB[1];
        MatrixS b21 = splittedB[2];
        MatrixS b22 = splittedB[3];

        //Проміжні змінні для зручності
        MatrixS s1 = a21.copy().add(a22.copy(), ring);
        MatrixS s2 = s1.copy().subtract(a11.copy(), ring);
        MatrixS s3 = a11.copy().subtract(a21.copy(), ring);
        MatrixS s4 = a12.copy().subtract(s2.copy(), ring);
        MatrixS s5 = b12.copy().subtract(b11.copy(), ring);
        MatrixS s6 = b22.copy().subtract(s5.copy(), ring);
        MatrixS s7 = b22.copy().subtract(b12.copy(), ring);
        MatrixS s8 = s6.copy().subtract(b21.copy(), ring);


        //Відправка частин матриць для обрахунку на інших процесорах
        MPITransport.sendObjectArray(new Object[]{
                a11, b11}, 0, 2, 1, 1);


        MPITransport.sendObjectArray(new Object[]{
                a12, b21}, 0, 2, 2, 2);


        MPITransport.sendObjectArray(new Object[]{
                s3, s7}, 0, 2, 3, 3);


        MPITransport.sendObjectArray(new Object[]{
                s1, s5}, 0, 2, 4, 4);

        MPITransport.sendObjectArray(new Object[]{
                s4, b22}, 0, 2, 5, 5);


        MPITransport.sendObjectArray(new Object[]{
                a22, s8}, 0, 2, 6, 6);

        //Закінчення відправки частин матриць для обрахунку на інших процесорах


        //Перший рахуємо на корневому
        MatrixS p1 = s2.copy().multiply(s6.copy(), ring);

        //Отримання частин матриць, які перемножили
        MatrixS p2 = (MatrixS) MPITransport.recvObject(1, 1);
        MatrixS p3 = (MatrixS) MPITransport.recvObject(2, 2);
        MatrixS p4 = (MatrixS) MPITransport.recvObject(3, 3);
        MatrixS p5 = (MatrixS) MPITransport.recvObject(4, 4);
        MatrixS p6 = (MatrixS) MPITransport.recvObject(5, 5);
        MatrixS p7 = (MatrixS) MPITransport.recvObject(6, 6);

        //Промоіжні результати
        MatrixS t1 = p1.copy().add(p2.copy(), ring);
        MatrixS t2 = t1.copy().add(p4.copy(), ring);


        //Обрахунок частин кінцевої матриці
        MatrixS c11 = p2.copy().add(p3.copy(), ring);
        MatrixS c12 = t1.copy().add(p5.copy(), ring).add(p6.copy(), ring);
        MatrixS c21 = t2.copy().subtract(p7.copy(), ring);
        MatrixS c22 = t2.copy().add(p5.copy(), ring);


        MatrixS[] c = new MatrixS[4];
        c[0] = c11;
        c[1] = c12;
        c[2] = c21;
        c[3] = c22;
        MatrixS result = MatrixS.join(c);

        return result;
    }
    public static MatrixS multiplySequential(MatrixS mathparA, MatrixS mathparB, Ring ring) throws MPIException, IOException, ClassNotFoundException {
        MatrixS[] splitA = mathparA.split();
        MatrixS[] splitB = mathparB.split();

        MatrixS a11 = splitA[0];
        MatrixS a12 = splitA[1];
        MatrixS a21 = splitA[2];
        MatrixS a22 = splitA[3];

        MatrixS b11 = splitB[0];
        MatrixS b12 = splitB[1];
        MatrixS b21 = splitB[2];
        MatrixS b22 = splitB[3];

        MatrixS s1 = a21.copy().add(a22.copy(), ring);
        MatrixS s2 = s1.copy().subtract(a11.copy(), ring);
        MatrixS s3 = a11.copy().subtract(a21.copy(), ring);
        MatrixS s4 = a12.copy().subtract(s2.copy(), ring);
        MatrixS s5 = b12.copy().subtract(b11.copy(), ring);
        MatrixS s6 = b22.copy().subtract(s5.copy(), ring);
        MatrixS s7 = b22.copy().subtract(b12.copy(), ring);
        MatrixS s8 = s6.copy().subtract(b21.copy(), ring);


        MatrixS p1 = s2.copy().multiply(s6.copy(), ring);
        MatrixS p2 = a11.copy().multiply(b11.copy(), ring);
        MatrixS p3 = a12.copy().multiply(b21.copy(), ring);
        MatrixS p4 = s3.copy().multiply(s7.copy(), ring);
        MatrixS p5 = s1.copy().multiply(s5.copy(), ring);
        MatrixS p6 = s4.copy().multiply(b22.copy(), ring);
        MatrixS p7 = a22.copy().multiply(s8.copy(), ring);


        MatrixS t1 = p1.copy().add(p2.copy(), ring);
        MatrixS t2 = t1.copy().add(p4.copy(), ring);


        MatrixS c11 = p2.copy().add(p3.copy(), ring);
        MatrixS c12 = t1.copy().add(p5.copy(), ring).add(p6.copy(), ring);
        MatrixS c21 = t2.copy().subtract(p7.copy(), ring);
        MatrixS c22 = t2.copy().add(p5.copy(), ring);


        MatrixS[] c = new MatrixS[4];
        c[0] = c11;
        c[1] = c12;
        c[2] = c21;
        c[3] = c22;
        MatrixS result = MatrixS.join(c);
        return result;
    }


}
