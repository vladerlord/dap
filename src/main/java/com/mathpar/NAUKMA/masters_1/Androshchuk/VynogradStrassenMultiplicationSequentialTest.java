package com.mathpar.NAUKMA.masters_1.Androshchuk;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Ring;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

public class VynogradStrassenMultiplicationSequentialTest {


    public static void main(String[] args) throws MPIException, IOException, ClassNotFoundException {
        Ring ring = new Ring("R64[]");

        int matrixSize = 128;
        long[][] matrixA = new long[matrixSize][matrixSize];
        long[][] matrixB = new long[matrixSize][matrixSize];

        initializeMatrix(matrixA);
        initializeMatrix(matrixB);
        MatrixS mA = new MatrixS(matrixA, ring);
        MatrixS mB = new MatrixS(matrixB, ring);
        long timeBeforeSimple = System.currentTimeMillis();
        MatrixS resultDefault = mA.copy().multiply(mB, ring);
        long timeAfterSimple = System.currentTimeMillis();
        long timeBefore = System.currentTimeMillis();
        MatrixS result = VynogradStrassenMultiplicationTool.multiplySequential(mA, mB, ring);
        long timeAfter = System.currentTimeMillis();
        System.out.println("Time of execution standard " + (timeAfterSimple - timeBeforeSimple));
        System.out.println("Time of execution " + (timeAfter - timeBefore));
        System.out.println(resultDefault.toString());
        System.out.println(result.toString());
        System.out.println(resultDefault.subtract(result, ring));
    }

    private static void initializeMatrix(long[][] matrixA) {
        Random r = new Random();
        for (int i = 0; i < matrixA.length; i++) {
            for (int j = 0; j < matrixA[0].length; j++) {
                matrixA[i][j] = r.nextInt(10000);
            }
        }
    }

    private static void initializeMatrix2(long[][] matrixA) {
        for (int i = 0; i < matrixA.length; i++) {
            for (int j = 0; j < matrixA[0].length; j++) {
                matrixA[i][j] = i;
            }
        }
    }
}
