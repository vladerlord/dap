package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;
import mpi.MPIException;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 4 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/AllReduce 10
Rank = 3
> 16
> 81
> 0
Rank = 1Rank = 0>
> 16
> 81
> 0
1

> 16
> 1
> > 12961296

> 2401
> > 25681
>
2401> 625
Rank = > 10000
>
> 14641
0
2> 256

> 1
> 16
> 625
> 81
> 1296> 0

> 2401> 1

> 256
> 1296
> 10000
> > 625
> > 10000
146412401
>
14641
> 256
> 625
> 10000
> 14641
*/

public class AllReduce {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int commRank = MPI.COMM_WORLD.getRank();
        int iterationsAmount = Integer.parseInt(args[0]);

        int[] sendBuffer = new int[iterationsAmount];
        int[] receiveBuffer = new int[iterationsAmount];

        for (int i = 0; i < iterationsAmount; i++) {
            sendBuffer[i] = i ^ 2;
        }

        MPI.COMM_WORLD.allReduce(sendBuffer, receiveBuffer, iterationsAmount, MPI.INT, MPI.PROD);

        for (int i = 0; i < iterationsAmount; i++) {
            if (commRank != i) {
                continue;
            }

            System.out.printf("Rank = %d%n", i);

            for (int j : receiveBuffer) {
                System.out.printf("> %d%n", j);
            }
        }

        MPI.Finalize();
    }
}