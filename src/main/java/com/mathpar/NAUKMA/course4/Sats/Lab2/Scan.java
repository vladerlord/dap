package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;
import mpi.MPIException;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 4 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/Scan 10
Rank = 0
> 2
> 3
> 0
> 1
> 6
> 7
> 4
Rank = 1
> 4
> 6
> 0
> 2
Rank = 2
> 6
> 5
> 10
> 12
> 14
> 9
> 8
> 10
> 20
> 22
> 11
> 0
> 3
> 18
> 21
> 12
> 15
Rank = 3
> 8
> 12
> 30
> 33
> 0
> 4
> 24
> 28
> 16
> 20
> 40
> 44
*/

public class Scan {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int commRank = MPI.COMM_WORLD.getRank();
        int iterationsAmount = Integer.parseInt(args[0]);

        int[] sendBuffer = new int[iterationsAmount];
        int[] receiveBuffer = new int[iterationsAmount];

        for (int i = 0; i < iterationsAmount; i++) {
            sendBuffer[i] = i ^ 2;
        }

        MPI.COMM_WORLD.scan(sendBuffer, receiveBuffer, iterationsAmount, MPI.INT, MPI.SUM);

        for (int j = 0; j < iterationsAmount; j++) {
            if (commRank != j) {
                continue;
            }

            System.out.print("Rank = " + j + "\n");

            for (int k : receiveBuffer) {
                System.out.print("> " + k + "\n");
            }
        }

        MPI.Finalize();
    }
}