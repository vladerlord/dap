package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;
import mpi.MPIException;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 2 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/Scatter 2
Rank = 1; 3
Rank = 1; 0
Rank = 0; 2
Rank = 0; 0
*/

public class Scatter {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int commRank = MPI.COMM_WORLD.getRank();
        int iterationsAmount = Integer.parseInt(args[0]);

        int[] sendBuffer = new int[iterationsAmount];
        int[] receiveBuffer = new int[iterationsAmount];

        for (int i = 0; i < iterationsAmount; i++) {
            sendBuffer[i] = i ^ 2;
        }

        MPI.COMM_WORLD.barrier();
        MPI.COMM_WORLD.scatter(sendBuffer, 1, MPI.INT, receiveBuffer, 1, MPI.INT, 0);

        for (int j : receiveBuffer) {
            System.out.printf("Rank = %d; %d%n", commRank, j);
        }

        MPI.Finalize();
    }
}