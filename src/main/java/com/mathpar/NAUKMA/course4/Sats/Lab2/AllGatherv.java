package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;
import mpi.MPIException;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 2 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/AllGatherv 2
Rank: 0
 0
 1
 0
 1

Rank: 1
 0
 1
 0
 1
*/

public class AllGatherv {
    public static void main(String[] args) throws MPIException, InterruptedException {
        MPI.Init(args);

        int commRank = MPI.COMM_WORLD.getRank();
        int commSize = MPI.COMM_WORLD.getSize();
        int iterationsAmount = Integer.parseInt(args[0]);

        int[] sendBuffer = new int[iterationsAmount];
        int[] receiveBuffer = new int[iterationsAmount * commSize];

        for (int i = 0; i < iterationsAmount; i++) {
            sendBuffer[i] = i;
        }

        MPI.COMM_WORLD.allGatherv(sendBuffer, iterationsAmount, MPI.INT, receiveBuffer,
                new int[]{iterationsAmount, iterationsAmount}, new int[]{2, 0}, MPI.INT);
        Thread.sleep(60L * commRank);
        System.out.printf("Rank: %d%n", commRank);

        for (int j : receiveBuffer) {
            System.out.println(" " + j);
        }

        System.out.println();
        MPI.Finalize();
    }
}