package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;
import mpi.MPIException;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 4 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/Reduce 10
> 8
> 12
> 0
> 4
> 24
> 28
> 16
> 20
> 40
> 0
*/

public class Reduce {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int commRank = MPI.COMM_WORLD.getRank();
        int iterationsAmount = Integer.parseInt(args[0]);

        int[] sendBuffer = new int[iterationsAmount];
        int[] receiveBuffer = new int[iterationsAmount];

        for (int i = 0; i < iterationsAmount; i++) {
            sendBuffer[i] = i^2;
        }

        MPI.COMM_WORLD.reduce(sendBuffer, receiveBuffer, iterationsAmount - 1, MPI.INT, MPI.SUM, 0);

        if (commRank != 0) {
            return;
        }

        for (int j : receiveBuffer) {
            System.out.printf("> %d%n", j);
        }

        MPI.Finalize();
    }
}