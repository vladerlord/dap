package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;
import mpi.MPIException;

import java.util.Arrays;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 2 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/AllToAll 2
Rank = 0: send buffer = [0, 0]
Rank = 0: receive buffer = [0, 1]
Rank = 1: send buffer = [1, 1]
Rank = 1: receive buffer = [0, 1]
*/

public class AllToAll {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int commRank = MPI.COMM_WORLD.getRank();
        int iterationsAmount = Integer.parseInt(args[0]);

        int[] sendBuffer = new int[iterationsAmount];
        int[] receiveBuffer = new int[iterationsAmount];

        for (int i = 0; i < iterationsAmount; i++){
            sendBuffer[i] = commRank;
        }

        MPI.COMM_WORLD.allToAll(sendBuffer, 1, MPI.INT, receiveBuffer, 1, MPI.INT);

        System.out.println("Rank = " + commRank + ": send buffer = " + Arrays.toString(sendBuffer));
        System.out.println("Rank = " + commRank + ": receive buffer = " + Arrays.toString(receiveBuffer));
        MPI.Finalize();
    }
}