package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;
import mpi.MPIException;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 1 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/AllToAllv 2
Rank = 0; 0
Rank = 0; 1
Rank = 0; 0
Rank = 0; 0
*/

public class AllToAllv {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int commRank = MPI.COMM_WORLD.getRank();
        int iterationsAmount = Integer.parseInt(args[0]);

        int[] sendBuffer = new int[iterationsAmount];
        int[] receiveBuffer = new int[iterationsAmount];

        for (int i = 0; i < iterationsAmount; i++) {
            sendBuffer[i] = i;
        }

        MPI.COMM_WORLD.allToAllv(sendBuffer, new int[]{2, 2},
                new int[]{0, 2}, MPI.INT, receiveBuffer, new int[]{2, 2},
                new int[]{0, 2}, MPI.INT);

        for (int j : receiveBuffer) {
            System.out.printf("Rank = %d; %d%n", commRank, j);
        }

        MPI.Finalize();
    }
}