package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;
import mpi.MPIException;

import java.util.Random;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 2 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/Bcast 10
sendBuffer[0] = 0.45437626480293636
sendBuffer[1] = 0.9616077811209286
sendBuffer[2] = 0.7232704171967553
sendBuffer[3] = 0.4523410866062729
sendBuffer[4] = 0.004097923809974646
sendBuffer[5] = 0.775601712556154
sendBuffer[6] = 0.706649049640487
sendBuffer[7] = 0.17905498296405586
sendBuffer[8] = 0.24749021057607512
sendBuffer[9] = 0.9916711847322318

Result:

Result:
sendBuffer[0] = 0.45437626480293636
sendBuffer[1] = 0.9616077811209286
sendBuffer[2] = 0.7232704171967553
sendBuffer[3] = 0.4523410866062729
sendBuffer[4] = 0.004097923809974646
sendBuffer[5] = 0.775601712556154
sendBuffer[6] = 0.706649049640487
sendBuffer[7] = 0.17905498296405586
sendBuffer[8] = 0.24749021057607512
sendBuffer[9] = 0.9916711847322318
*/

public class Bcast {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int commRank = MPI.COMM_WORLD.getRank();
        int iterationsAmount = Integer.parseInt(args[0]);

        double[] sendBuffer = new double[iterationsAmount];

        if (commRank == 0) {
            for (int i = 0; i < iterationsAmount; i++) {
                sendBuffer[i] = new Random().nextDouble();
                System.out.println("sendBuffer[" + i + "] = " + sendBuffer[i]);
            }
        }

        MPI.COMM_WORLD.barrier();
        MPI.COMM_WORLD.bcast(sendBuffer, sendBuffer.length, MPI.DOUBLE, 0);

        System.out.println("\nResult:");

        if (commRank != 0) {
            for (int i = 0; i < iterationsAmount; i++) {
                System.out.println("sendBuffer[" + i + "] = " + sendBuffer[i]);
            }
        }

        MPI.Finalize();
    }
}