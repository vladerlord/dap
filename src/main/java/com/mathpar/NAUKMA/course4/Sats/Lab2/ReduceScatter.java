package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;
import mpi.MPIException;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 2 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/ReduceScatter 10
Rank = 0; 4
Rank = 0; 6
Rank = 0; 0
Rank = 0; 0
Rank = 0; 0
Rank = 1; 0
Rank = 1; 2
Rank = 1; 0
Rank = 1; 0
Rank = 1; 0
Rank = 1; 0
Rank = 1; 0
Rank = 0; 0
Rank = 0; 0
Rank = 0; 0
Rank = 0; 0
Rank = 0; 0
Rank = 1; 0
Rank = 1; 0
Rank = 1; 0
*/

public class ReduceScatter {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int commRank = MPI.COMM_WORLD.getRank();
        int iterationsAmount = Integer.parseInt(args[0]);

        int[] sendBuffer = new int[iterationsAmount];
        int[] receiveBuffer = new int[iterationsAmount];

        for (int i = 0; i < iterationsAmount; i++) {
            sendBuffer[i] = i ^ 2;
        }

        MPI.COMM_WORLD.reduceScatter(sendBuffer, receiveBuffer, new int[]{2, 2}, MPI.INT, MPI.SUM);

        for (int i = 0; i < receiveBuffer.length; i++)
            System.out.print("Rank = " + commRank + "; " + receiveBuffer[i] + "\n");

        MPI.Finalize();
    }
}