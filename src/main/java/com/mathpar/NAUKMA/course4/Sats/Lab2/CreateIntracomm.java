package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;

import java.util.Random;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 2 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/CreateIntracomm 10
sendBuffer[0] = 0.6227702698108605
sendBuffer[1] = 0.016247772579647934
sendBuffer[2] = 0.5162186572817612
sendBuffer[3] = 0.039870535261475215
sendBuffer[4] = 0.6205087799264123
sendBuffer[5] = 0.27618245562029164
sendBuffer[6] = 0.02232541764897855
sendBuffer[7] = 0.45093430451356753
sendBuffer[8] = 0.6212752651142476
sendBuffer[9] = 0.01465770651016629

Result:

Result:
sendBuffer[0] = 0.6227702698108605
sendBuffer[1] = 0.016247772579647934
sendBuffer[2] = 0.5162186572817612
sendBuffer[3] = 0.039870535261475215
sendBuffer[4] = 0.6205087799264123
sendBuffer[5] = 0.27618245562029164
sendBuffer[6] = 0.02232541764897855
sendBuffer[7] = 0.45093430451356753
sendBuffer[8] = 0.6212752651142476
sendBuffer[9] = 0.01465770651016629
*/

public class CreateIntracomm {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        mpi.Group commGroup = MPI.COMM_WORLD.getGroup().incl(new int[]{0, 1});
        Intracomm intracomm = MPI.COMM_WORLD.create(commGroup);

        int commRank = intracomm.getRank();
        int iterationsAmount = Integer.parseInt(args[0]);

        double[] sendBuffer = new double[iterationsAmount];

        if (commRank == 0) {
            for (int i = 0; i < iterationsAmount; i++) {
                sendBuffer[i] = new Random().nextDouble();
                System.out.println("sendBuffer[" + i + "] = " + sendBuffer[i]);
            }
        }

        MPI.COMM_WORLD.barrier();
        MPI.COMM_WORLD.bcast(sendBuffer, sendBuffer.length, MPI.DOUBLE, 0);

        System.out.println("\nResult:");

        if (commRank != 0) {
            for (int i = 0; i < iterationsAmount; i++) {
                System.out.println("sendBuffer[" + i + "] = " + sendBuffer[i]);
            }
        }

        MPI.Finalize();
    }
}