package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;
import mpi.MPIException;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 2 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/Scatterv 10
Rank = 0; 2
Rank = 0; 3
Rank = 0; 0
Rank = 0; 0
Rank = 0; 0
Rank = 0; 0
Rank = 1; 3
Rank = 1; 0
Rank = 1; 0
Rank = 1; 0
Rank = 1; 0
Rank = 1; 0
Rank = 1; 0
Rank = 1; 0
Rank = 0; 0
Rank = 0; 0
Rank = 0; 0
Rank = 0; 0
Rank = 1; 0
Rank = 1; 0
*/

public class Scatterv {
    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int commRank = MPI.COMM_WORLD.getRank();
        int iterationsAmount = Integer.parseInt(args[0]);

        int[] sendBuffer = new int[iterationsAmount];
        int[] receiveBuffer = new int[iterationsAmount];

        for (int i = 0; i < iterationsAmount; i++) {
            sendBuffer[i] = i ^ 2;
        }

        MPI.COMM_WORLD.barrier();
        MPI.COMM_WORLD.scatterv(sendBuffer, new int[]{3, 2, 1, 0},
                new int[]{0, 1, 1, 0}, MPI.INT, receiveBuffer, iterationsAmount, MPI.INT, 0);

        for (int i = 0; i < receiveBuffer.length; i++)
            System.out.print("Rank = " + commRank + "; " + receiveBuffer[i] + "\n");

        MPI.Finalize();
    }
}