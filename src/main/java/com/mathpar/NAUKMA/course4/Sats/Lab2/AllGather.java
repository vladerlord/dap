package com.mathpar.NAUKMA.course4.Sats.Lab2;

import mpi.MPI;

/*
vlad@7010c14e1418:/src$ mpirun --hostfile hostfile -np 2 java -cp /src/target/classes com/mathpar/NAUKMA/course4/Sats/Lab2/AllGather 2
Rank: 0
 0
 0
 0
 1

Rank: 1
 0
 0
 0
 1
*/

public class AllGather {
    public static void main(String[] args) throws Exception {
        MPI.Init(args);
        int iterationsAmount = Integer.parseInt(args[0]);

        int commRank = MPI.COMM_WORLD.getRank();
        int commSize = MPI.COMM_WORLD.getSize();

        int[] sendBuffer = new int[iterationsAmount];
        int[] receiveBuffer = new int[iterationsAmount * commSize];

        for (int i = commRank; i < iterationsAmount; i++) {
            sendBuffer[i] = commRank;
        }

        MPI.COMM_WORLD.allGather(sendBuffer, iterationsAmount, MPI.INT, receiveBuffer, iterationsAmount, MPI.INT);
        Thread.sleep(60L * commRank);
        System.out.printf("Rank: %d%n", commRank);

        for (int j : receiveBuffer) {
            System.out.println(" " + j);
        }

        System.out.println();
        MPI.Finalize();
    }
}
