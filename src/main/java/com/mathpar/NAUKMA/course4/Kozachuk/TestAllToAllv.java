package com.mathpar.NAUKMA.course4.Kozachuk;

import mpi.MPI;
import mpi.MPIException;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 1 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestAllToAllv 2
 */

/*
Output:
myrank = 0; 0
myrank = 0; 1
 */
public class TestAllToAllv {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        int n = Integer.parseInt(args[0]);
        Integer[] a = new Integer[n];
        for (int i = 0; i < n; i++) {
            a[i] = Integer.valueOf(i);
        }
        Integer[] b = new Integer[2];
        MPI.COMM_WORLD.allToAllv(a, new int[]{2, 2},
                new int[]{0, 2}, MPI.INT, b, new int[]{2, 2},
                new int[]{0, 2}, MPI.INT);

        for (int i = 0; i < 2; i++) {
            System.out.print("myrank = " + myrank
                    + "; " + b[i] + "\n");

        }
        // завершення паралельної частини
        MPI.Finalize();
    }
}
