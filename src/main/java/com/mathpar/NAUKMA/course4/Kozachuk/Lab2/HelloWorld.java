package com.mathpar.NAUKMA.course4.Kozachuk.Lab2;

import mpi.MPI;
import mpi.MPIException;

/**
 mpirun --hostfile /home/akozachuk/hostfile -np 2 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/HelloWorld
 */

/*
output:
Hello, processor number is 1
Hello, processor number is 0
 */
public class HelloWorld{

    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        System.out.printf("Hello, processor number is %d\n", processorNumber());

        MPI.Finalize();
    }

    private static int processorNumber() throws MPIException {
        return MPI.COMM_WORLD.getRank();
    }
}
