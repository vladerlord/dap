package com.mathpar.NAUKMA.course4.Kozachuk;

import mpi.MPI;
import mpi.MPIException;

import java.nio.IntBuffer;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 2 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestAllToAll 4
 */

/*
Output:
b[0] = 0b[0] = 0
b[1] = 1
b[2] = 2
b[3] = 3
capacity b = 4

b[1] = 1
b[2] = 2
b[3] = 3
capacity b = 4
myrank = 1 send=0 recv=0
myrank = 1 send=1 recv=0
myrank = 1 send=2 recv=37
myrank = 1 send=3 recv=0myrank = 0 send=0 recv=0

myrank = 0 send=1 recv=1
myrank = 0 send=2 recv=2
myrank = 0 send=3 recv=3

 */
public class TestAllToAll {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        // визначення числа процесорів в групі
        int np = MPI.COMM_WORLD.getSize();
        int n = Integer.parseInt(args[0]);
        // створення масиву з 4 елементів
        IntBuffer b = MPI.newIntBuffer(4);
        b.put(0); b.put(1); b.put(2); b.put(3);
        for (int i = 0; i < 4; i++)
            System.out.println("b[" + i + "] = " + b.get(i));
        System.out.println("capacity b = " + b.capacity());
        IntBuffer q = MPI.newIntBuffer(4);
        MPI.COMM_WORLD.allToAll(b, n, MPI.INT, q, n, MPI.INT);
        for (int i = 0; i < q.capacity(); i++) {
            System.out.println("myrank = " + myrank +
                    " send=" + b.get(i) + " recv=" + q.get(i));
        }
        // завершення паралельної частини
        MPI.Finalize();
    }
}
