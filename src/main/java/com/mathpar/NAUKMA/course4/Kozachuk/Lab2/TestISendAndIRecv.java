package com.mathpar.NAUKMA.course4.Kozachuk.Lab2;

import mpi.MPI;
import mpi.MPIException;

import java.nio.IntBuffer;
import java.util.Random;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 4 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestISendAndIRecv.java 1000
 */

/*
output:
proc num = 0 array sent
proc num = 3 array received
proc num = 2 array received
proc num = 1 array received
 */
public class TestISendAndIRecv {
    public static void main(String[] args) throws MPIException {

        MPI.Init(args);

        int myrank = MPI.COMM_WORLD.getRank();

        int np = MPI.COMM_WORLD.getSize();

        int n = Integer.parseInt(args[0]);

        IntBuffer b = MPI.newIntBuffer(n);
        MPI.COMM_WORLD.barrier();
        if (myrank == 0) {
            for (int i = 0; i < n; i++) {
                b.put(new Random().nextInt(10));
            }
            for (int i = 1; i < np; i++) {
                MPI.COMM_WORLD.iSend(b, b.capacity(), MPI.INT, i, 3000);
            }
            System.out.println("proc num = " + myrank +

                    " array sent");

        } else {
            MPI.COMM_WORLD.recv(b, b.capacity(), MPI.INT, 0, 3000);
            System.out.println("proc num = " + myrank +
                    " array received");

        }

        MPI.Finalize();
    }
}
