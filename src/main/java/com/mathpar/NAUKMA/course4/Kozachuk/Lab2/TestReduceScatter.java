package com.mathpar.NAUKMA.course4.Kozachuk.Lab2;

import mpi.MPI;
import mpi.MPIException;

import java.util.Arrays;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 2 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestReduceScatter 10
 */

/*
 0
 2
 0
 0
 0
 0
 0
 0
 0
 0
 */
public class TestReduceScatter {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        int n = Integer.parseInt(args[0]);
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = i;
        }
        int[] q = new int[n];
        MPI.COMM_WORLD.reduceScatter(a, q, new int[]{3, 5},
                MPI.INT, MPI.SUM);


        System.out.println("myrank = " + myrank + " a = " + Arrays.toString(q));

        // завершення паралельної частини
        MPI.Finalize();
    }
}
