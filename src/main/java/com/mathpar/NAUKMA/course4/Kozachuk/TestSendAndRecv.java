package com.mathpar.NAUKMA.course4.Kozachuk;

import mpi.MPI;
import mpi.MPIException;

import java.nio.IntBuffer;
import java.util.Random;

/**
 mpirun --hostfile /home/akozachuk/hostfile -np 4 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestSendAndRecv 1000
 */

/*
output:
Processor number is 1, array received
Processor number is 2, array received
Processor number is 3, array received
Processor number is 0, array sent
 */
public class TestSendAndRecv {

    public static void main(String[] args) throws MPIException {
        MPI.Init(args);

        int processorNumber = MPI.COMM_WORLD.getRank();
        int processorsAmount = MPI.COMM_WORLD.getSize();

        int arraySize = Integer.parseInt(args[0]);

        IntBuffer buffer = MPI.newIntBuffer(arraySize);
        // Synchronize all processors
        // Block all processors in MPI.Comm group until all of them call this method
        MPI.COMM_WORLD.barrier();
        if (processorNumber == 0) {
            for (int i = 0; i < arraySize; i++) {
                buffer.put(new Random().nextInt(10));
            }
            for (int i = 1; i < processorsAmount; ++i) {
                MPI.COMM_WORLD.send(buffer, buffer.capacity(), MPI.INT, i, 3000);
            }
            System.out.printf("Processor number is %d, array sent\n", processorNumber);
        } else {
            MPI.COMM_WORLD.recv(buffer, buffer.capacity(), MPI.INT, 0, 3000);
            System.out.printf("Processor number is %d, array received\n", processorNumber);
        }

        MPI.Finalize();
    }
}
