package com.mathpar.NAUKMA.course4.Kozachuk.Lab2;

import mpi.MPI;
import mpi.MPIException;

import java.util.Random;

/**
 * Процесор з номером 0 пересилає масив чисел
 * іншим процесорам,
 * використовуючи роздачу за бінарним деревом.
 mpirun --hostfile /home/akozachuk/hostfile -np 4 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestBcast 10
 */


/*
Output:
a[0]= 0.039180301554655794
a[1]= 0.8025860655060459
a[2]= 0.023812317088678592
a[3]= 0.005836537462511671
a[4]= 0.6742570215905618
a[5]= 0.7302780959162425
a[6]= 0.13547824310436696
a[7]= 0.8221978883299635
a[8]= 0.3824287155655568
a[9]= 0.9591996544525804
a[0]= 0.039180301554655794
a[1]= 0.8025860655060459
a[2]= 0.023812317088678592
a[3]= 0.005836537462511671
a[4]= 0.6742570215905618
a[5]= 0.7302780959162425
a[6]= 0.13547824310436696
a[7]= 0.8221978883299635
a[8]= 0.3824287155655568
a[9]= 0.9591996544525804
a[0]= 0.039180301554655794a[0]= 0.039180301554655794

a[1]= 0.8025860655060459a[1]= 0.8025860655060459

a[2]= 0.023812317088678592a[2]= 0.023812317088678592

a[3]= 0.005836537462511671a[3]= 0.005836537462511671

a[4]= 0.6742570215905618a[4]= 0.6742570215905618

a[5]= 0.7302780959162425a[5]= 0.7302780959162425

a[6]= 0.13547824310436696a[6]= 0.13547824310436696

a[7]= 0.8221978883299635
a[8]= 0.3824287155655568a[7]= 0.8221978883299635
a[8]= 0.3824287155655568
a[9]= 0.9591996544525804

a[9]= 0.9591996544525804
 */
public class TestBcast {
    public static void main(String[] args)
            throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        int n = Integer.parseInt(args[0]);
        double[] a = new double[n];
        if (myrank == 0) {
            for (int i = 0; i < n; i++) {

                a[i] = new Random().nextDouble();
                System.out.println("a[" + i + "]= " + a[i]);
            }
        }
        MPI.COMM_WORLD.barrier();
        // передача даних від 0 процесора іншим
        MPI.COMM_WORLD.bcast(a, a.length, MPI.DOUBLE, 0);
        if (myrank != 0) {
            for (int i = 0; i < n; i++) {
                System.out.println("a[" + i + "]= " + a[i]);
            }
        }
        // завершення параленьої частини
        MPI.Finalize();
    }
}
