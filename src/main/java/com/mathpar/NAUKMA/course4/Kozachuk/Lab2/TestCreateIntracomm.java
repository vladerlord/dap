package com.mathpar.NAUKMA.course4.Kozachuk.Lab2;

import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;

import java.util.Random;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 2 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestCreateIntracomm 10
 */

/*
a[0]= 0.3412800550832513
a[1]= 0.056631583727172585
a[2]= 0.6087043142503231
a[3]= 0.5955166583767191
a[4]= 0.293055523405619
a[5]= 0.14088083510458604
a[6]= 0.08679818259944694
a[7]= 0.316668535788598
a[8]= 0.0033285275353784893
a[9]= 0.1150291993926652
a[0]= 0.3412800550832513
a[1]= 0.056631583727172585
a[2]= 0.6087043142503231
a[3]= 0.5955166583767191
a[4]= 0.293055523405619
a[5]= 0.14088083510458604
a[6]= 0.08679818259944694
a[7]= 0.316668535788598
a[8]= 0.0033285275353784893
a[9]= 0.1150291993926652
 */

public class TestCreateIntracomm {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначаємо нову групу працюючих процесорів
        mpi.Group g = MPI.COMM_WORLD.getGroup().incl(
                new int[]{0, 1});
        // створюємо новий комунікатор
        Intracomm COMM_NEW = MPI.COMM_WORLD.create(g);
        int myrank = COMM_NEW.getRank();
        int n = Integer.parseInt(args[0]);
        double[] a = new double[n];
        if (myrank == 0) {
            for (int i = 0; i < n; i++) {
                a[i] = new Random().nextDouble();
                System.out.println("a[" + i + "]= " + a[i]);
            }
        }
        COMM_NEW.barrier();
        // застосовуємо до нового комунікатора функцію bcast
        COMM_NEW.bcast(a, a.length, MPI.DOUBLE, 0);
        if (myrank != 0) {
            for (int i = 0; i < n; i++) {
                System.out.println("a[" + i + "]= " + a[i]);
            }
        }
        // завершення парарельної частини
        MPI.Finalize();
    }
}