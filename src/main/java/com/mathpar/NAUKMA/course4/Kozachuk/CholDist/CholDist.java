package com.mathpar.NAUKMA.course4.Kozachuk.CholDist;

import com.mathpar.func.FuncNumberR;
import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.*;

import java.util.Random;

/*
*
* Coomand to execute:

mpirun --hostfile /home/akozachuk/hostfile -np 1 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/CholDist/CholDist 4

* Example of work:

Generated matrix:

[[18, 22, 54,  42 ]
 [22, 70, 86,  62 ]
 [54, 86, 174, 134]
 [42, 62, 134, 106]]

Decomposed matrix L:

[[4.24,  0,    0,    0   ]
 [5.19,  6.57, 0,    0   ]
 [12.73, 3.05, 1.65, 0   ]
 [9.9,   1.62, 1.85, 1.39]]

Decomposed matrix L^T:

[[4.24, 5.19, 12.73, 9.9 ]
 [0,    6.57, 3.05,  1.62]
 [0,    0,    1.65,  1.85]
 [0,    0,    0,     1.39]]

Decomposed matrix L^(-1):

[[0.24,  0,     0,     0   ]
 [-0.19, 0.15,  0,     0   ]
 [-1.47, -0.28, 0.61,  0   ]
 [0.5,   0.2,   -0.81, 0.72]]
*
* */

public class CholDist {
    private static final Ring ring = new Ring("R[]");
    private final static MpiLogger LOGGER = MpiLogger.getLogger(CholDist.class);


    public static void main(String[] args) {

        System.gc();
        ring.setMachineEpsilonR(499);
        ring.setAccuracy(500);
        // int matrixDim = Integer.parseInt(args[0]);
        ring.setFLOATPOS(510);

     //   NumberR t = (NumberR)new NumberR(1).divide(new NumberR(3), ring);
        //FuncNumberR fnr = new FuncNumberR(ring);
      //  Element div = t.divide(new NumberR(3),ring);
     //   Element mult = div.multiply(new NumberR(3),ring);
      //  Element err = t.subtract(mult, ring);

        //LOGGER.info("1/3 = "+t.toString(ring));
       // LOGGER.info("div = "+div.toString(ring));
      //  LOGGER.info("mult = "+mult.toString(ring));
      //  LOGGER.info("Error = "+err.toString(ring));

        MatrixS[] A = null;
        MatrixS[] matrixBlocks = null;
        MatrixS test = null;
        MatrixS sub = null;
        Element err;
        int n = 100;
       // double[] arr = { 0.0000000442097117692163950 , 0.0000000271317896594069900 , 0.0000000013986813929278696 ,
       //         0.0000004492611447526329000 , 0.0000000107962919493331800 , 0.0000000332316513640322400 ,
      //          0.0000000099341723736046100, 0.0000000177868117789969930 , 0.0000000096260060145170840 , 0.0000000086468085580548850 };

        //double average1 = Arrays.stream(arr).average().getAsDouble();
        //System.out.println(String.format(".ARR average =  %.25f ", average1));
        Element[] arrerr = new Element[n];
        // double[] maxarrerr = new double[10];

             for (int i = 0; i < n; i++) {
//                A = /*testMatrix( 64);*/Tools.initForCholesky(64, ring);
                // System.out.println(i);
        //System.out.println("Generated matrix:\n" + A.toString());


        matrixBlocks = choleskyDecomp(A[0]);

      //  System.out.println("\nDecomposed matrix L:\n" +matrixBlocks[0].toString(ring));
        //         System.out.println("\nA[1]\n" +A[1].toString(ring));
        //System.out.println("\nDecomposed matrix L^T:\n" + matrixBlocks[0].transpose());
        //System.out.println("\nDecomposed matrix L^(-1):\n" + matrixBlocks[1]);
            //test = matrixBlocks[0].multiply(matrixBlocks[0].transpose(), ring);
            sub = A[1].subtract(matrixBlocks[0], ring);
            err = sub.max(ring);
          //  System.out.println( i+". Error =   " + err.toString(ring));
                 //System.out.println(err.value);
                 arrerr[i] =  err;
        // System.out.println("\nL*L^T:\n" + test);
        //System.out.println("Sub =   " + sub);
   }

      // double maxerr = Arrays.stream(arrerr).max().getAsDouble();
        Element maxerr = Array.maxabs(arrerr, ring);

    //for(int i = 0; i<n; i++)System.out.println(String.format(".ARR Error max =  %.25f ", maxerr));

    //      maxarrerr[j] = maxerr;


    //  double maxx = Arrays.stream(maxarrerr).max().getAsDouble();
     // Element average = Array.;

        LOGGER.info(".ARR Error max =  "+maxerr.toString(ring));

      Element sum = ring.numberZERO;
      for(int i = 0; i<n; i++)
      {
          //System.out.println("arrerr[i] =  "+arrerr[i].numbElementType());
         sum =  sum.add(arrerr[i], ring);

      }

      Element average = sum.divide(new NumberR(n), ring);
     LOGGER.info(".ARR average =  "+average.toString(ring));

        System.gc();
}

    public static MatrixS[] choleskyDecomp(MatrixS inputMatrix) {

        if (inputMatrix.size == 1 && inputMatrix.colNumb == 1) {
           // Element [][]m = inputMatrix.toScalarArray(ring);
            //double value = m[0][0].doubleValue();

            Element value = inputMatrix.getElement(0, 0, ring);

           // LOGGER.info("value = " + value.toString(ring));
          //  LOGGER.info("value.isNegative() = " + value.isNegative());
            if(value.isNegative()){
               // Element sqrtValue = value.sqrt(ring);
              //  LOGGER.info("inputMatrix = " + inputMatrix+  "value = " + value.toString(ring));
                return new MatrixS[]{
                        new MatrixS(ring.numberZERO),
                        new MatrixS(ring.numberZERO)

                };
            }
            else{
               // LOGGER.info("in else try ti sqrt");
                FuncNumberR fnr = new FuncNumberR(ring);
                Element sqrtValue =fnr.sqrt((NumberR)value);
          //  LOGGER.info(",new Fraction( ring.numberONE, sqrtValue) " + new Fraction( ring.numberONE, sqrtValue).numbElementType());
           // LOGGER.info("sqrtValue =  "+sqrtValue.toString(ring));
          //      LOGGER.info(" ring.numberONE =  "+ ring.numberONE.numbElementType());

            return new MatrixS[]{
                    new MatrixS(sqrtValue),
                    new MatrixS( ring.numberONE.divide(sqrtValue, ring))

            };}
        } else {

            MatrixS[] inputMatrixBlocks = inputMatrix.split(); //(alpha, beta, beta , gamma)
         //   LOGGER.info("block[0] = " + Arrays.toString(inputMatrixBlocks));
            MatrixS[] decomposedA = choleskyDecomp(inputMatrixBlocks[0]);
          //  LOGGER.info("decomposedA[1] = " + decomposedA[1]);
            MatrixS bT = decomposedA[1].multiply(inputMatrixBlocks[1], ring);


          //  LOGGER.info("blocks[3] = " + inputMatrixBlocks[3].toString(ring));
            MatrixS b = bT.transpose();
         //   LOGGER.info("b = " + b.toString(ring));
          //  LOGGER.info("b.multiply(bT, ring) = " +b.multiply(bT, ring).toString(ring));
            MatrixS beta = inputMatrixBlocks[3].subtract(b.multiply(bT, ring), ring);
            //if(inputMatrix.size==32)

            //    LOGGER.info("bT = " + bT + " b = " + b + " b.multiply(bT, ring) = "+ b.multiply(bT, ring) + "  gamma = " + beta);

            MatrixS[] decomposedC = choleskyDecomp(beta);
           // LOGGER.info("decomposedC = " + decomposedC);
            MatrixS z = decomposedC[1].multiply(b, ring).multiply(decomposedA[1], ring).negate(ring);
           // LOGGER.info("z = " + z);

            MatrixS[] LBlocks = new MatrixS[4];
            MatrixS[] LinvBlocks = new MatrixS[4];

            LBlocks[0] = decomposedA[0];
            LBlocks[1] = createBlockFilledByZeros(decomposedA[0].size, decomposedA[0].colNumb);
            LBlocks[2] = b;
            LBlocks[3] = decomposedC[0];

            LinvBlocks[0] = decomposedA[1];
            LinvBlocks[1] = createBlockFilledByZeros(decomposedA[0].size, decomposedA[0].colNumb);
            LinvBlocks[2] = z;
            LinvBlocks[3] = decomposedC[1];

            MatrixS L = MatrixS.join(LBlocks);

            MatrixS Linv = MatrixS.join(LinvBlocks);
          //  MatrixS res =  L.multiply(L.transpose(),ring);
          //  MatrixS resinv =  L.multiply(Linv,ring);


          //  LOGGER.info("check matrix chol = " +res.subtract(inputMatrix,ring).isZero(ring) + " , L = " + L);
         //   LOGGER.info("res = " +res);
         //   LOGGER.info("res.subtract(inputMatrix,ring = " +res.subtract(inputMatrix,ring));
         //   LOGGER.info("resinv = " + resinv.toString(ring));
         //   LOGGER.info("check matrix ininv = v = " +resinv.isOne(ring) + " , matrixsize = " + res.size + " , LInv =  " + Linv.toString(ring) );
            return new MatrixS[]{L, Linv};
        }
    }

    private static MatrixS createBlockFilledByZeros(int row, int column) {
        Element[][] elMatrix = new Element[row][column];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                elMatrix[i][j] = new NumberR64(0);
            }
        }

        return new MatrixS(elMatrix, ring);
    }



    private static MatrixS testMatrix(int n)
    {
        MatrixS test = createBlockFilledByZeros(n,n);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i >= j)
                   test.putElement(new NumberR64(i), i, j);

               if (i == j) {
                   if (test.getElement(i, j, ring).isZero(ring))
                    test.putElement(ring.numberONE, i, j);
                }
            }
        }

       // LOGGER.info("Test Matrix = " + test);
        return test;
    }

    private static MatrixS init(int dim) {

        Element[][] matrixElements = new Element[dim][dim];
        Random random = new Random();
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                matrixElements[i][j] = new NumberR64(random.nextDouble() * 100);
            }
        }

        Element[][] identityElements = new Element[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                if (i == j)
                    identityElements[i][j] = new NumberR64((dim - 1) * 100);
                else
                    identityElements[i][j] = new NumberR64(0);
            }
        }

/*        Element matrixElements[][] = {
                {new NumberR64(18), new NumberR64(22), new NumberR64(54), new NumberR64(42)},
                {new NumberR64(22), new NumberR64(70), new NumberR64(86), new NumberR64(62)},
                {new NumberR64(54), new NumberR64(86), new NumberR64(174), new NumberR64(134)},
                {new NumberR64(42), new NumberR64(62), new NumberR64(134), new NumberR64(106)}
        };*/

        MatrixS m = new MatrixS(matrixElements, ring);
        MatrixS mT = m.transpose();
        MatrixS identityM = new MatrixS(identityElements, ring);
        return m.add(mT, ring).multiplyByNumber(new NumberR64(0.5), ring).add(identityM, ring);
    }

}
