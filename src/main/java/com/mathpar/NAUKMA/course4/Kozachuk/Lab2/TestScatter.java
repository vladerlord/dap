package com.mathpar.NAUKMA.course4.Kozachuk.Lab2;

import mpi.MPI;
import mpi.MPIException;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 2 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestScatter 2
 */

/*
a = 0
a = 1
rank = 0
myrank = 0 ; 0

myrank = 0 ; 0

myrank = 1 ; 1

myrank = 1 ; 0

 */
public class TestScatter {
    public static void main(String[] args) throws MPIException{
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        // визначенння числа процесорів в групі
        int np = MPI.COMM_WORLD.getSize();
        int n = Integer.parseInt(args[0]);
        // оголошуємо масив об'єктів
        int[] a = new int[2];
        // заповнюємо цей масив на нульовому процесорі
        if (myrank == 0){
            for (int i = 0; i < 2; i++){
                a[i] = i ;
                System.out.println("a = " + a[i] + " ");
            }
            System.out.println("rank = " + myrank);
        }
        // оголошуємо масив, в який будуть записуватись
        // прийняті процесором елементи
        int[] q = new int[2];
        MPI.COMM_WORLD.barrier();
        MPI.COMM_WORLD.scatter(a, 1, MPI.INT, q, 1, MPI.INT, 0);
        // роздруковуємо отримані масиви і номера процесорів
        for (int i = 0; i < q.length; i++)
            System.out.println("myrank = " + myrank
                    + " ; " + q[i] + "\n");

        // завершення паралельної частини
        MPI.Finalize();
    }
}
