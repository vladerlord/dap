package com.mathpar.NAUKMA.course4.Kozachuk.Lab2;

import mpi.MPI;
import mpi.MPIException;

import java.nio.IntBuffer;
import java.util.Arrays;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 2 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestAllToAll 4
 */

/*
Output:
b[0] = 0b[0] = 0
b[1] = 1
b[2] = 2
b[3] = 3
capacity b = 4

b[1] = 1
b[2] = 2
b[3] = 3
capacity b = 4
myrank = 1 send=0 recv=0
myrank = 1 send=1 recv=0
myrank = 1 send=2 recv=37
myrank = 1 send=3 recv=0myrank = 0 send=0 recv=0

myrank = 0 send=1 recv=1
myrank = 0 send=2 recv=2
myrank = 0 send=3 recv=3

 */
public class TestAllToAll {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        // визначення числа процесорів в групі
        int np = MPI.COMM_WORLD.getSize();
        int n = Integer.parseInt(args[0]);
        // створення масиву з 4 елементів
        int a[] = new int[n];
        for (int i = 0; i < n; i++) a[i] = i+myrank;
        int q[] = new int[n];
        System.out.println("myrank = " + myrank + " a = " + Arrays.toString(a));
        MPI.COMM_WORLD.allToAll(a, 1, MPI.INT, q, 2, MPI.INT);
        System.out.println("myrank = " + myrank + " q = " + Arrays.toString(q));
        MPI.Finalize();
    }
}
