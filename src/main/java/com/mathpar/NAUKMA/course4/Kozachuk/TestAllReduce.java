package com.mathpar.NAUKMA.course4.Kozachuk;

import mpi.MPI;
import mpi.MPIException;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 4 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestAllReduce 10
 */

/*
output:
myrank = 1myrank = 3

 0 0

 1 1

 16 16

 81 81
 256
myrank = 0
 256 625

 625
 1296
 2401 1296

 2401 4096

 4096
 6561

 6561 0

 1
 16
 81
 256
 625
 1296
 2401
 4096
 6561
myrank = 2
 0
 1
 16
 81
 256
 625
 1296
 2401
 4096
 6561
 */
public class TestAllReduce {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        // визначення числа процесорів в групі
        int np = MPI.COMM_WORLD.getSize();
        int n = Integer.parseInt(args[0]);
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = i;
        }
        int[] q = new int[n];
        MPI.COMM_WORLD.allReduce(a, q, n, MPI.INT, MPI.PROD);
        for (int j = 0; j < np; j++) {
            if (myrank == j) {
                System.out.println("myrank = " + j);
                for (int i = 0; i < q.length; i++) {
                    System.out.println(" " + q[i]);
                }
            }
        }
        // завершення параалельної частини
        MPI.Finalize();
    }
}
