package com.mathpar.NAUKMA.course4.Kozachuk.Lab2;

import mpi.MPI;
import mpi.MPIException;

import java.util.Arrays;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 1 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestAllToAllv 2
 */

/*
Output:
myrank = 0; 0
myrank = 0; 1
 */
public class TestAllToAllv {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        int n = Integer.parseInt(args[0]);
        Integer[] a = new Integer[n];
        for (int i = 0; i < n; i++) {
            a[i] = Integer.valueOf(i)*(myrank+1);
        }
        System.out.println(myrank+" "+Arrays.toString(a));
        Integer[] b = new Integer[n*2];
        MPI.COMM_WORLD.allToAllv(a, new int[]{2,2,2,2},
                new int[]{1, 1,2,2}, MPI.INT, b, new int[]{2,2,2,2},
                new int[]{0,2,4,6}, MPI.INT);


            System.out.print("myrank = " + myrank
                    + "; " + Arrays.toString(b)+ "\n");


        // завершення паралельної частини
        MPI.Finalize();
    }
}
