package com.mathpar.NAUKMA.course4.Kozachuk;

import mpi.MPI;
import mpi.MPIException;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 4 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestScan 10
 */

/*
Output:

myrank = 0
 0
 1
 2
 3
 4
 5
 6
 7
 8
 9
myrank = 2myrank = 1
 0

 3 0

 6 2

 9 4

 12 6

 15 8

 10 18

 21
 12
 24 14

 27 16myrank = 3

 18

 0
 4
 8
 12
 16
 20
 24
 28
 32
 36

 */

public class TestScan {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        // визначення числа процесорів в групі
        int np = MPI.COMM_WORLD.getSize();
        int n = Integer.parseInt(args[0]);
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = i;
        }
        int[] q = new int[n];
        MPI.COMM_WORLD.scan(a, q, n, MPI.INT, MPI.SUM);
        for (int j = 0; j < np; j++) {
            if (myrank == j) {
                System.out.println("myrank = " + j);
                for (int i = 0; i < q.length; i++) {
                    System.out.println(" " + q[i]);
                }
            }
        }
        // завершення параельної частини
        MPI.Finalize();
    }
}

