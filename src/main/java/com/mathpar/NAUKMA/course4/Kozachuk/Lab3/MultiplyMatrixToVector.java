package com.mathpar.NAUKMA.course4.Kozachuk.Lab3;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.*;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

/*

mpirun --hostfile /home/akozachuk/hostfile -np 4 java -cp /home/akozachuk/mpi-dap/target/classes com/mathpar/NAUKMA/course4/Kozachuk/Lab3/MultiplyMatrixToVector 4

* I'm processor 2
I'm processor 1I'm processor 3

Matrix A =
[[8,  7,  2,  15]
 [2,  27, 13, 20]
 [19, 7,  4,  4 ]
 [9,  19, 9,  21]]
Vector B = [5, 0, 20, 24]
rank= 0 row = [8, 7, 2, 15]
rank = 1 row = [2, 27, 13, 20]
rank = 1 B = [5, 0, 20, 24]
1 res== 750
send result
rank = 2 row = [19, 7, 4, 4]
rank = 2 B = [5, 0, 20, 24]
2 res== 271
send result
rank = 3 row = [9, 19, 9, 21]
rank = 3 B = [5, 0, 20, 24]
3 res== 729
send result
A * B = [440, 750, 271, 729]

* */


public class MultiplyMatrixToVector {
    public static void main(String[] args) throws MPIException, IOException, ClassNotFoundException {
        Ring ring = new Ring("Z[x]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();
        //розмір матриці
        int ord = Integer.parseInt(args[0]);
        //кількість рядків для процесорів з rank > 0
        int k = ord / size;
        //кількість рядків для 0 процесора
        int n = ord - k * (size - 1);
        if (rank == 0) {
            int den = 10000;
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[]{5, 5}, rnd, NumberZp32.ONE, ring);
            System.out.println("Matrix A = " + A);
            VectorS B = new VectorS(ord, den, new int[]{5, 5}, rnd, ring);
            System.out.println("Vector B = " + B);
            //створюємо масив з результатом  0 процесора
            Element[] res0 = new Element[n];
            for (int i = 0; i < n; i++) {
                res0[i] = B.multiply(A.toSquaredArray(ring)[i], ring);
                System.out.println("rank= " + rank + " row = "
                        + Array.toString(A.toSquaredArray(ring)[i]));

            }
            //розсилка рядків процесорам
            for (int j = 1; j < size; j++) {
                for (int z = 0; z < k; z++) {
                    System.out.println();
                    Transport.sendObject(A.toSquaredArray(ring)[n + (j - 1) * k
                            + z], j, 100 + j);
                }
                //надсилаємо вектор кожному процесору
                Transport.sendObject(B.V, j, 100 + j);
            }
            //масив результату множення матриці на вектор
            Element[] result = new Element[ord];
            System.arraycopy(res0, 0, result, 0, n);
            //отримання результатів від кожного процесора
            for (int t = 1; t < size; t++) {
                Element[] resRank = (Element[])
                        Transport.recvObject(t, 100 + t);
                System.arraycopy(resRank, 0, result, n +
                        (t - 1) * k, resRank.length);
            }
            System.out.println("A * B = " +
                    new VectorS(result).toString(ring));
        } else {
            //програма виконується на процесорі з рангом  = rank
            System.out.println("I'm processor " + rank);
            //отримання рядків матриці від 0 процесора
            Element[][] A = new Element[k][ord];
            for (int i = 0; i < k; i++) {
                A[i] = (Element[])
                        Transport.recvObject(0, 100 + rank);
                System.out.println("rank = " + rank + " row = "
                        + Array.toString(A[i]));
            }
            //отримання вектора від 0 процесора
            Element[] B = (Element[])
                    Transport.recvObject(0, 100 + rank);
            System.out.println("rank = " + rank + " B = "
                    + Array.toString(B));
            // створення масиву результату множення
            // рядків матриці на вектор
            Element[] result = new Element[k];
            for (int j = 0; j < A.length; j++) {
                result[j] = new VectorS(A[j]).multiply(
                        B, ring);
                System.out.println(rank + " res== " + result[j]);
            }
            //надсилаємо результат 0 процесору
            Transport.sendObject(result, 0, 100 + rank);
            System.out.println("send result");
        }
        MPI.Finalize();
    }
}