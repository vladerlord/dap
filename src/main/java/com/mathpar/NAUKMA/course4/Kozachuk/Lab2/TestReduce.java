package com.mathpar.NAUKMA.course4.Kozachuk.Lab2;

import mpi.MPI;
import mpi.MPIException;

import java.util.Arrays;

/*
mpirun --hostfile /home/akozachuk/hostfile -np 4 java -cp /home/akozachuk/mpi-dap-2/target/classes com/mathpar/NAUKMA/course4/Kozachuk/TestReduce 10
*/

/*

Output:

 0
 4
 8
 12
 16
 20
 24
 28
 32
 36
 */
public class TestReduce {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        int n = Integer.parseInt(args[0]);
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = i;
        }
        System.out.println(myrank+" "+ Arrays.toString(a));
        int[] q = new int[n];
        MPI.COMM_WORLD.reduce(a, q, n-1, MPI.INT, MPI.SUM, 0);
        if (myrank == 0) {
            for (int i = 0; i < q.length; i++) {
                System.out.println(" " + q[i]);
            }
        }
        // завершення паралельної частини
        MPI.Finalize();
    }
}
