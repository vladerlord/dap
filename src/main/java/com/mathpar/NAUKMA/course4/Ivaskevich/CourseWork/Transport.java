package com.mathpar.NAUKMA.course4.Ivaskevich.CourseWork;

import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;

import java.io.*;

public class Transport {

    /**
     * Send object method
     *
     * @param a         - object to send
     * @param proc      - processor-receiver number
     * @param tag       - message identification
     * @param intracomm - commutator for transport
     * @throws MPIException - MPIException machining
     * @throws IOException  - IOException machining
     */
    public static void sendObject(Object a, int proc, int tag, Intracomm intracomm)
            throws MPIException, IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);
        oos.writeObject(a);
        byte[] tmp = bos.toByteArray();
        intracomm.send(tmp, tmp.length, MPI.BYTE, proc, tag);
    }

    /**
     * Receive object method
     *
     * @param proc      - processor-sender number
     * @param tag       - message identification
     * @param intracomm - commutator for transport
     * @return - received object
     * @throws MPIException           - MPIException machining
     * @throws IOException            - IOException machining
     * @throws ClassNotFoundException - ClassNotFoundException machining
     */
    public static Object recvObject(int proc, int tag, Intracomm intracomm)
            throws MPIException, IOException, ClassNotFoundException {
        // the command probe() reads the status of the buffer for receiving a message
        // from the processor with number @proc and message identification @tag
        // getCount() - static method of the Status class, which counts the number of elements in the buffer
        int size = intracomm.probe(proc, tag).getCount(MPI.BYTE);
        // creating byte array
        byte[] tmp = new byte[size];
        // recv - blocking array reception from input buffer to tmp array
        intracomm.recv(tmp, size, MPI.BYTE, proc, tag);
        ByteArrayInputStream bis = new ByteArrayInputStream(tmp);
        ObjectInputStream ois = new ObjectInputStream(bis);
        // pass the received object to the procedure exit
        return ois.readObject();
    }

    /**
     * Send array of objects method
     *
     * @param a         - array of objects
     * @param proc      - processor-receiver number
     * @param tag       - message identification
     * @param intracomm - commutator for transport
     * @throws MPIException - MPIException machining
     * @throws IOException  - IOException machining
     */
    public static void sendArrayOfObjects(Object[] a, int proc, int tag, Intracomm intracomm)
            throws MPIException, IOException {

        for (int i = 0; i < a.length; i++) {
            sendObject(a[i], proc, tag + i, intracomm);
        }
    }

    /**
     * Receive array of objects method
     *
     * @param s         - receiving array size
     * @param proc      - processor-receiver number
     * @param tag       - message identification
     * @param intracomm - commutator for transport
     * @throws MPIException - MPIException machining
     * @throws IOException  - IOException machining
     */
    public static Object[] recvArrayOfObjects(int s, int proc, int tag, Intracomm intracomm)
            throws MPIException, IOException, ClassNotFoundException {

        Object[] o = new Object[s];
        for (int i = 0; i < s; i++) {
            o[i] = recvObject(proc, tag + i, intracomm);
        }
        return o;
    }

}
