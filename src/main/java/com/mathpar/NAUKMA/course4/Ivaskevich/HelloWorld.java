package com.mathpar.NAUKMA.course4.Ivaskevich;

import mpi.*;

public class HelloWorld {
//    Result:
//    Processor number in COMM_WORLD group: 0 Hello World
//    Processor number in COMM_WORLD group: 1 Hello World
    public static void main(String[] args)
            throws MPIException {
        MPI.Init(args);
        int myrank = MPI.COMM_WORLD.getRank();
        System.out.println("Processor number in COMM_WORLD group: " + myrank + " Hello World");
        MPI.Finalize();
    }
}

