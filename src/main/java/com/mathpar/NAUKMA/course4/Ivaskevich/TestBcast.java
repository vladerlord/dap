package com.mathpar.NAUKMA.course4.Ivaskevich;

import mpi.MPI;
import mpi.MPIException;

import java.util.Random;

/**
 * Процесор з номером 0 пересилає масив чисел
 * іншим процесорам,
 * використовуючи роздачу за бінарним деревом.
 mpirun --hostfile /home/andriy/hostfile -np 4 java -cp /home/andriy/mpi-dap/target/classes com/mathpar/NAUKMA/course4/Ivaskevich/TestBcast 10
 */


/*
Output:
a[0]= 0.952253083365712
a[1]= 0.21707422894862538
a[2]= 0.035306414137107445
a[3]= 0.5533849087307194
a[4]= 0.671674348383198
a[5]= 0.060043635573789444
a[6]= 0.6040594699964853
a[7]= 0.10103619695219335
a[8]= 0.6368914345518824
a[9]= 0.10456370610728216

a[0]= 0.952253083365712
a[1]= 0.21707422894862538
a[2]= 0.035306414137107445
a[3]= 0.5533849087307194
a[4]= 0.671674348383198
a[5]= 0.060043635573789444
a[6]= 0.6040594699964853
a[7]= 0.10103619695219335
a[8]= 0.6368914345518824
a[9]= 0.10456370610728216

a[0]= 0.952253083365712
a[0]= 0.952253083365712
a[1]= 0.21707422894862538
a[2]= 0.035306414137107445
a[3]= 0.5533849087307194

a[1]= 0.21707422894862538
a[2]= 0.035306414137107445a[4]= 0.671674348383198

a[5]= 0.060043635573789444
a[6]= 0.6040594699964853a[3]= 0.5533849087307194

a[4]= 0.671674348383198
a[5]= 0.060043635573789444a[7]= 0.10103619695219335

a[8]= 0.6368914345518824
a[9]= 0.10456370610728216a[6]= 0.6040594699964853

a[7]= 0.10103619695219335
a[8]= 0.6368914345518824
a[9]= 0.10456370610728216

 */
public class TestBcast {
    public static void main(String[] args)
            throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        int n = Integer.parseInt(args[0]);
        double[] a = new double[n];
        if (myrank == 0) {
            for (int i = 0; i < n; i++) {

                a[i] = new Random().nextDouble();
                System.out.println("a[" + i + "]= " + a[i]);
            }
        }
        MPI.COMM_WORLD.barrier();
        // передача даних від 0 процесора іншим
        MPI.COMM_WORLD.bcast(a, a.length, MPI.DOUBLE, 0);
        if (myrank != 0) {
            for (int i = 0; i < n; i++) {
                System.out.println("a[" + i + "]= " + a[i]);
            }
        }
        // завершення параленьої частини
        MPI.Finalize();
    }
}
