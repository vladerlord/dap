package com.mathpar.NAUKMA.course4.Ivaskevich.CholeskyDecomposition;

import com.mathpar.NAUKMA.course4.Ivaskevich.Transport;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Ring;
import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;

import static com.mathpar.NAUKMA.course4.Ivaskevich.CholeskyDecomposition.CholeskyUtil.createMatrix;
import static com.mathpar.NAUKMA.course4.Ivaskevich.CholeskyDecomposition.CholeskyUtil.fillAndJoinMatrix;
import static com.mathpar.NAUKMA.course4.Ivaskevich.MatrixMul4.multiplyMatrix4;
import static com.mathpar.NAUKMA.course4.Ivaskevich.MatrixMul8.multiplyMatrix8;
import static com.mathpar.NAUKMA.course4.Kozachuk.CholDist.CholDist.choleskyDecomp;

//import static com.mathpar.NAUKMA.course4.Gomilko.Variant6.MatrixMul2.multiplyMatrix2;

/*

cd /home/USER/mpi-dap/target/classes
mpirun --hostfile /home/hunterok2311/hostfile -np 8 java -cp . com/mathpar/NAUKMA/course4/Ivaskevich/CholeskyDecomposition/CholeskyDecomposition MATRIX_SIZE

                        *****
                        *2x2*
                        *****
Inputted matrix : [[4, 1]
                  [1, 5]]

Result L matrix : [[2,   0   ]
                  [0.5, 2.18]]

Result L-inverted  matrix : [[0.5,   0   ]
                            [-0.11, 0.46]]

                        *****
                        *4x4*
                        *****
Inputted matrix : [[4, 1, 1, 1]
                  [1, 5, 1, 1]
                  [1, 1, 6, 1]
                  [1, 1, 1, 7]]

Result L matrix : [[2,   0,    0,    0   ]
                  [0.5, 2.18, 0,    0   ]
                  [0.5, 0.34, 2.37, 0   ]
                  [0.5, 0.34, 0.27, 2.56]]

Result L-inverted  matrix : [[0.5,   0,     0,     0   ]
                            [-0.11, 0.46,  0,     0   ]
                            [-0.09, -0.07, 0.42,  0   ]
                            [-0.07, -0.05, -0.04, 0.39]]

                        *****
                        *8x8*
                        *****
 Inputted matrix : [[4, 1, 1, 1, 1, 1, 1,  1 ]
                   [1, 5, 1, 1, 1, 1, 1,  1 ]
                   [1, 1, 6, 1, 1, 1, 1,  1 ]
                   [1, 1, 1, 7, 1, 1, 1,  1 ]
                   [1, 1, 1, 1, 8, 1, 1,  1 ]
                   [1, 1, 1, 1, 1, 9, 1,  1 ]
                   [1, 1, 1, 1, 1, 1, 10, 1 ]
                   [1, 1, 1, 1, 1, 1, 1,  11]]

Result L matrix : [[2,   0,    0,    0,    0,    0,    0,    0   ]
                  [0.5, 2.18, 0,    0,    0,    0,    0,    0   ]
                  [0.5, 0.34, 2.37, 0,    0,    0,    0,    0   ]
                  [0.5, 0.34, 0.27, 2.56, 0,    0,    0,    0   ]
                  [0.5, 0.34, 0.27, 0.22, 2.74, 0,    0,    0   ]
                  [0.5, 0.34, 0.27, 0.22, 0.19, 2.91, 0,    0   ]
                  [0.5, 0.34, 0.27, 0.22, 0.19, 0.16, 3.07, 0   ]
                  [0.5, 0.34, 0.27, 0.22, 0.19, 0.16, 0.15, 3.23]]

Result L-inverted  matrix : [[0.5,   0,     0,     0,     0,     0,     0,     0   ]
                            [-0.11, 0.46,  0,     0,     0,     0,     0,     0   ]
                            [-0.09, -0.07, 0.42,  0,     0,     0,     0,     0   ]
                            [-0.07, -0.05, -0.04, 0.39,  0,     0,     0,     0   ]
                            [-0.06, -0.05, -0.04, -0.03, 0.36,  0,     0,     0   ]
                            [-0.05, -0.04, -0.03, -0.03, -0.02, 0.34,  0,     0   ]
                            [-0.05, -0.04, -0.03, -0.02, -0.02, -0.02, 0.33,  0   ]
                            [-0.04, -0.03, -0.03, -0.02, -0.02, -0.02, -0.01, 0.31]]



*/
public class CholeskyDecomposition {
    private static final Ring ring = new Ring("R64[x]");

    public static void main(String[] args) throws MPIException, IOException, ClassNotFoundException {
        MPI.Init(new String[0]);

        MatrixS inputtedMatrix = createMatrix(Integer.parseInt(args[0]), ring);
        MatrixS[] result = choleskyDecomposition(inputtedMatrix, MPI.COMM_WORLD);
        if (MPI.COMM_WORLD.getRank() == 0) {
            System.out.println("\nInputted matrix : " + inputtedMatrix);
            System.out.println("\nResult L matrix : " + result[0]
                    + "\n\n" + "Result L-inverted  matrix : " + result[1] + "\n");
        }
        MPI.Finalize();
    }

    public static MatrixS[] choleskyDecomposition(MatrixS A, Intracomm intracomm) throws MPIException, IOException, ClassNotFoundException {
        MatrixS L, LInv;
        MatrixS[] result = new MatrixS[2];

        switch (intracomm.getSize()) {
            case 2: {
                if (intracomm.getRank() == 0) {
                    MatrixS[] ABlocks = A.split();
                    MatrixS[] resA = choleskyDecomp(ABlocks[0]);
                    MatrixS bT = resA[1].multiply(ABlocks[1], ring);
                    MatrixS b = bT.transpose();
                    Transport.sendObjects(new Object[]{b, resA[1]}, 1, 0);
                    MatrixS[] resC = choleskyDecomp(ABlocks[3].subtract(b.multiply(bT, ring), ring));
                    MatrixS recv = (MatrixS) Transport.recvObject(1, 1);
                    MatrixS z = resC[1].multiply(recv, ring).negate(ring);
                    L = fillAndJoinMatrix(resA[0], b, resC[0], ring);
                    LInv = fillAndJoinMatrix(resA[1], z, resC[1], ring);
                    result = new MatrixS[]{L, LInv};
                } else {
                    Object[] recv = Transport.recvObjects(2, 0, 0);
                    MatrixS res = ((MatrixS) recv[0]).multiply((MatrixS) recv[1], ring);
                    Transport.sendObject(res, 0, 1);
                }
            }
            break;
            case 4: {
                int[] group1 = new int[]{0, 1};
                int[] group2 = new int[]{2, 3};
                int[] group3 = new int[]{0, 1, 2, 3};
                Intracomm COMM_1 = intracomm.create(intracomm.getGroup().incl(group1));
                Intracomm COMM_2 = intracomm.create(intracomm.getGroup().incl(group2));

                if (A.size < 4) {
                    if (intracomm.getRank() < 2) return choleskyDecomposition(A, COMM_1);
                    else break;
                }

                MatrixS[] ABlocks = A.split();
                MatrixS[] resA, resC = new MatrixS[2];
                MatrixS bT, b, z = new MatrixS();

                if (intracomm.getRank() == 0) {
                    resA = choleskyDecomposition(ABlocks[0], COMM_1);
                    for (int i = 1; i < intracomm.getSize(); i++) Transport.sendObject(resA, i, i);
                } else {
                    if (intracomm.getRank() == 1) choleskyDecomposition(ABlocks[0], COMM_1);
                    resA = (MatrixS[]) Transport.recvObject(0, intracomm.getRank());
                }

                if (intracomm.getRank() == 0) {
                    bT = multiplyMatrix4(resA[1], ABlocks[1], group3, intracomm);
                    for (int i = 1; i < intracomm.getSize(); i++) Transport.sendObject(bT, i, i);
                } else {
                    multiplyMatrix4(resA[1], ABlocks[1], group3, intracomm);
                    bT = (MatrixS) Transport.recvObject(0, intracomm.getRank());
                }

                b = bT.transpose();

                if (intracomm.getRank() == 0) {
                    Transport.sendObjects(new Object[]{b, resA[1]}, 2, 2);
                    Transport.sendObjects(new Object[]{b, resA[1]}, 3, 3);
                    resC = choleskyDecomposition(ABlocks[3].subtract(b.multiply(bT, ring), ring), COMM_1);
                    MatrixS recv = (MatrixS) Transport.recvObject(2, 2);
                    z = resC[1].multiply(recv, ring).negate(ring);
                }
                if (intracomm.getRank() == 1) {
                    choleskyDecomposition(ABlocks[3].subtract(b.multiply(bT, ring), ring), COMM_1);
                }

                if (intracomm.getRank() == 0) {
                    L = fillAndJoinMatrix(resA[0], b, resC[0], ring);
                    LInv = fillAndJoinMatrix(resA[1], z, resC[1], ring);
                    result = new MatrixS[]{L, LInv};
                }

                if (intracomm.getRank() == 2 || intracomm.getRank() == 3) {
                    MatrixS[] recv = new MatrixS[2];
                    Object[] recvObject = Transport.recvObjects(2, 0, intracomm.getRank());
                    for (int i = 0; i < recv.length; i++) {
                        recv[i] = (MatrixS) recvObject[i];
                    }
                    MatrixS res = null/*multiplyMatrix2(recv[0], recv[1], group2, COMM_2)*/;
                    if (intracomm.getRank() == 2) Transport.sendObject(res, 0, 2);
                }
            }
            break;
            case 8: {
                int[] group1 = new int[]{0, 1, 2, 3};
                int[] group2 = new int[]{4, 5, 6, 7};
                Intracomm COMM_1 = intracomm.create(intracomm.getGroup().incl(group1));
                Intracomm COMM_2 = intracomm.create(intracomm.getGroup().incl(group2));

                if (A.size < 8) {
                    if (intracomm.getRank() < 4) return choleskyDecomposition(A, COMM_1);
                    else break;
                }

                MatrixS[] ABlocks = A.split(), resA, resC = new MatrixS[2];
                MatrixS bT, b, z = new MatrixS();

                if (intracomm.getRank() == 0) {
                    resA = choleskyDecomposition(ABlocks[0], COMM_1);
                    for (int i = 1; i < MPI.COMM_WORLD.getSize(); i++) {
                        Transport.sendObject(resA, i, i);
                    }
                } else {
                    if (intracomm.getRank() < group2[0])
                        choleskyDecomposition(ABlocks[0], COMM_1);
                    resA = (MatrixS[]) Transport.recvObject(0, intracomm.getRank());
                }

                if (intracomm.getRank() == 0) {
                    bT = multiplyMatrix8(resA[1], ABlocks[1], intracomm);
                    for (int i = 1; i < MPI.COMM_WORLD.getSize(); i++) {
                        Transport.sendObject(bT, i, i);
                    }
                } else {
                    multiplyMatrix8(resA[1], ABlocks[1], intracomm);
                    bT = (MatrixS) Transport.recvObject(0, intracomm.getRank());
                }

                b = bT.transpose();

                if (intracomm.getRank() == 0) {
                    for (int i = 4; i < MPI.COMM_WORLD.getSize(); i++) {
                        Transport.sendObjects(new Object[]{b, resA[1]}, i, i);
                    }
                    resC = choleskyDecomposition(ABlocks[3].subtract(b.multiply(bT, ring), ring), COMM_1);
                    MatrixS recv = (MatrixS) Transport.recvObject(4, 4);
                    z = resC[1].multiply(recv, ring).negate(ring);
                } else {
                    if (intracomm.getRank() < group2[0])
                        choleskyDecomposition(ABlocks[3].subtract(b.multiply(bT, ring), ring), COMM_1);
                }

                if (intracomm.getRank() == 0) {
                    L = fillAndJoinMatrix(resA[0], b, resC[0], ring);
                    LInv = fillAndJoinMatrix(resA[1], z, resC[1], ring);
                    result = new MatrixS[]{L, LInv};
                }

                if (intracomm.getRank() >= group2[0]) {
                    MatrixS[] recv = new MatrixS[2];
                    Object[] recvObject = Transport.recvObjects(2, 0, intracomm.getRank());
                    for (int i = 0; i < recv.length; i++) {
                        recv[i] = (MatrixS) recvObject[i];
                    }
                    MatrixS res = multiplyMatrix4(recv[0], recv[1], group2, COMM_2);
                    if (intracomm.getRank() == 4) Transport.sendObject(res, 0, 4);
                }
            }
            break;
            default:
                throw new MPIException("Incorrect processors number.");
        }
        return result;
    }
}
