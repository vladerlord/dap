package com.mathpar.NAUKMA.course4.Ivaskevich;

import mpi.MPI;

/*
mpirun --hostfile /home/andriy/hostfile -np 2 java -cp /home/andriy/mpi-dap/target/classes com/mathpar/NAUKMA/course4/Ivaskevich/TestAllGather 2
 */

/*
* Output:
*
myrank = 0 :
 0
 0
 1
 1

myrank = 1 :
 0
 0
 1
 1
* */

public class TestAllGather {
    public static void main(String[] args)throws Exception {
        // ініціалізація MPI
        MPI.Init(args);
        Thread t = new Thread();
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        // визначення числа процесорів в групі
        int np = MPI.COMM_WORLD.getSize();
        int n = Integer.parseInt(args[0]);
        int[] a = new int[n];
        for(int i = 0; i < n; i++) {
            a[i] = myrank;
        }
        int[] q = new int[n * np];
        MPI.COMM_WORLD.allGather(a, n, MPI.INT, q, n, MPI.INT);
        t.sleep(60 * myrank);
        System.out.println("myrank = " + myrank + " : ");
        for(int i = 0; i < q.length; i++) {
            System.out.println(" " + q[i]);
        }
        System.out.println();
        // завершення паралельної частини
        MPI.Finalize();
    }
}