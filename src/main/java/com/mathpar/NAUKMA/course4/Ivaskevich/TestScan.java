package com.mathpar.NAUKMA.course4.Ivaskevich;

import mpi.MPI;
import mpi.MPIException;

/*
mpirun --hostfile /home/andriy/hostfile -np 4 java -cp /home/andriy/mpi-dap/target/classes com/mathpar/NAUKMA/course4/Ivaskevich/TestScan 10
 */

/*
Output:
myrank = 0
 0
 1
 2
 3
 4
 5
 6
 7
 8
 9
myrank = 1 myrank = 2

 0
 0 3

 6
 2 9


 myrank = 3
 12

 15 0

 4
 18
 8
 21
 24
 27

 12
 4 16
 20
 24
 28
 32

 36 6

 8
 10
 12
 14
 16
 18

 */

public class TestScan {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        // визначення числа процесорів в групі
        int np = MPI.COMM_WORLD.getSize();
        int n = Integer.parseInt(args[0]);
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = i;
        }
        int[] q = new int[n];
        MPI.COMM_WORLD.scan(a, q, n, MPI.INT, MPI.SUM);
        for (int j = 0; j < np; j++) {
            if (myrank == j) {
                System.out.println("myrank = " + j);
                for (int i = 0; i < q.length; i++) {
                    System.out.println(" " + q[i]);
                }
            }
        }
        // завершення параельної частини
        MPI.Finalize();
    }
}

