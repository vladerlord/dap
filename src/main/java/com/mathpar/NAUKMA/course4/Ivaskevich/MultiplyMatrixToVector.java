package com.mathpar.NAUKMA.course4.Ivaskevich;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.*;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;


public class MultiplyMatrixToVector {
    public static void main(String[] args) throws MPIException,
            IOException, ClassNotFoundException {

        Ring ring = new Ring("Z[x]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();
//размер матрицы
        int ord = Integer.parseInt(args[0]);
//количество строк для процессоров c rank > 0
        int k = ord / size;
//количество строк для 0 процессора
        int n = ord - k * (size - 1);
        if (rank == 0) {
            int den = 10000;
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[]{5, 5},
                    rnd, NumberZp32.ONE, ring);
            System.out.println("Matrix A = " + A.toString());
            VectorS B = new VectorS(ord, den, new int[]{5, 5},
                    rnd, ring);
            System.out.println("Vector B = " + B.toString());
//создаем массив с результатом 0 процессора
            Element[] res0 = new Element[n];
            for (int i = 0; i < n; i++) {
//TODO                res0[i] = new VectorS(A.M[i]).multiply(B, ring);
                res0[i] = B.multiply(A.toSquaredArray(ring)[i], ring);

                System.out.println("rank = " + rank + " row = "
//TODO                        + Array.toString(A.M[i]));
                        + Array.toString(A.toSquaredArray(ring)[i]));

            }

//рассылка строк процессорам
            for (int j = 1; j < size; j++) {
                for (int z = 0; z < k; z++) {
//TODO                    Transport.sendObject(A.M[n + (j - 1) * k + j * z], j, 100 + j)
                    Transport.sendObject(A.toSquaredArray(ring)[n + (j - 1) * k], j, 100 + j);
                }
                //отправляем вектор каждому процессору
                Transport.sendObject(B.V, j, 100 + j);
            }
//массив результата умножения матрицы на вектор
            Element[] result = new Element[ord];
            System.arraycopy(res0, 0, result, 0, n);
//приемка результатов от каждого процессора
            for (int t = 1; t < size; t++) {
                Element[] resRank = (Element[])
                        Transport.recvObject(t, 100 + t);
                System.arraycopy(resRank, 0, result, n +
                        (t - 1) * k, resRank.length);

            }
            System.out.println("RESULT A * B = " +
                    new VectorS(result).toString(ring));
        } else {
//программа выполняется на процессоре с рангом = rank
            System.out.println("I’m processor " + rank);
//приемка строк матрицы от 0 процессора
            Element[][] A = new Element[k][ord];
            for (int i = 0; i < k; i++) {
                A[i] = (Element[])
                        Transport.recvObject(0, 100 + rank);
                System.out.println("rank = " + rank + " row = "
                        + Array.toString(A[i]));

            }
//приемка вектора от 0 процессора
            Element[] B = (Element[])
                    Transport.recvObject(0, 100 + rank);
            System.out.println("rank = " + rank + " B = "
                    + Array.toString(B));
//создание массива результата умножения
//строк матрицы на вектор
            Element[] result = new Element[k];
            for (int j = 0; j < A.length; j++) {
                System.out.println("Multiplying " + new VectorS(A[j]) + " to " + new VectorS(B));
                result[j] = new VectorS(A[j]).multiply(
                        new VectorS(B), ring);
                System.out.println("Result = " + result[j]);

            }
            //отсылаем результат 0 процессору
            Transport.sendObject(result, 0, 100 + rank);
            System.out.println("send result");
        }
        MPI.Finalize();
    }
}

