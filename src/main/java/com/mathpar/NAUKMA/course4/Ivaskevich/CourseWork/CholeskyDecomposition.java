package com.mathpar.NAUKMA.course4.Ivaskevich.CourseWork;

import com.mathpar.func.FuncNumberR;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberR;
import com.mathpar.number.Ring;
import mpi.Intracomm;
import mpi.MPIException;

import java.io.IOException;
import java.util.stream.IntStream;

public class CholeskyDecomposition {
    /**
     * This method represents a symmetric positive definite matrix @A
     * in the form A = L*LT, using Cholesky algorithm in algebraic
     * space @ring using commutator @intracomm for objects transport.
     * This method can be used for sequential Cholesky algorithm, or
     * also can be used by 0 processor for block recursive Cholesky
     * algorithm.
     *
     * @param A         - Matrix for decomposition
     * @param ring      - algebraic space of current variables
     * @param intracomm - commutator for transport
     * @return - MatrixS[]{MatrixS L, MatrixS LInv}
     * @throws MPIException           - MPIException machining
     * @throws IOException            - IOException machining
     * @throws ClassNotFoundException - ClassNotFoundException machining
     */
    public static MatrixS[] choleskyDecomposition(MatrixS A, Ring ring, Intracomm intracomm, int leaf)
            throws MPIException, IOException, ClassNotFoundException {
        if (((A.size == leaf && A.colNumb == leaf) && intracomm.getSize() != 1)) {
            for (int i = 1; i < intracomm.getSize(); i++) Transport.sendObject(false, i, 404, intracomm);
            Intracomm comm = intracomm.create(intracomm.getGroup().incl(new int[]{0}));
            return choleskyDecomposition(A, ring, comm, leaf);
        }
        for (int i = 1; i < intracomm.getSize(); i++) Transport.sendObject(true, i, 404, intracomm);

        if (intracomm.getSize() == 1) {
            if (A.size == 1 && A.colNumb == 1) {
                Element value = A.getElement(0, 0, ring);
                if (value.isNegative()) {
                    return new MatrixS[]{new MatrixS(ring.numberZERO), new MatrixS(ring.numberZERO)};
                } else {
                    FuncNumberR funcNumberR = new FuncNumberR(ring);
                    Element sqrtValue = funcNumberR.sqrt((NumberR) value);
                    return new MatrixS[]{
                            new MatrixS(sqrtValue), new MatrixS(ring.numberONE.divide(sqrtValue, ring))};
                }
            } else {
                MatrixS[] AA = A.split();
                MatrixS[] cholA = choleskyDecomposition(AA[0], ring, intracomm, leaf);
                MatrixS bT = cholA[1].multiply(AA[1], ring);
                MatrixS b = bT.transpose();
                MatrixS delta = AA[3].subtract(b.multiply(bT, ring), ring);
                MatrixS[] cholC = choleskyDecomposition(delta, ring, intracomm, leaf);
                MatrixS z = cholC[1].multiply(b, ring).multiply(cholA[1], ring).negate(ring);

                return new MatrixS[]{
                        MatrixS.join(new MatrixS[]{
                                cholA[0], createZeroBlock(cholA[0].size, cholA[0].colNumb, ring), b, cholC[0]
                        }),
                        MatrixS.join(new MatrixS[]{
                                cholA[1], createZeroBlock(cholA[0].size, cholA[0].colNumb, ring), z, cholC[1]
                        })};
            }
        } else {
            int half = intracomm.getSize() / 2;

            Intracomm[] comm = new Intracomm[]{
                    intracomm.create(intracomm.getGroup()
                            .incl(IntStream.rangeClosed(0, half - 1).toArray())),
                    intracomm.create(intracomm.getGroup()
                            .incl(IntStream.rangeClosed(half, intracomm.getSize() - 1).toArray()))};

            MatrixS[] AA = A.split();
            MatrixS[] cholA = choleskyDecomposition(AA[0], ring, comm[0], leaf);
            MatrixS bT = MatrixMul.multiplyMatrix(cholA[1], AA[1], ring, intracomm, leaf);
            MatrixS b = bT.transpose();
            MatrixS delta = MatrixMul.multiplyMatrixWithAdd(b.negate(ring), bT, AA[3], ring, intracomm, leaf);

            Transport.sendArrayOfObjects(new Object[]{b, cholA[1]}, half, 0, intracomm);

            MatrixS[] cholC = choleskyDecomposition(delta, ring, comm[0], leaf);
            MatrixS recv = (MatrixS) Transport.recvObject(half, half, intracomm);
            MatrixS z = MatrixMul.multiplyMatrix(cholC[1], recv, ring, intracomm, leaf).negate(ring);

            return new MatrixS[]{
                    MatrixS.join(new MatrixS[]{
                            cholA[0], createZeroBlock(cholA[0].size, cholA[0].colNumb, ring), b, cholC[0]
                    }),
                    MatrixS.join(new MatrixS[]{
                            cholA[1], createZeroBlock(cholA[0].size, cholA[0].colNumb, ring), z, cholC[1]
                    })};
        }
    }

    /**
     * This method represents a symmetric positive definite matrix @A
     * in the form A = L*LT, using Cholesky algorithm in algebraic
     * space @ring using commutator @intracomm for objects transport.
     * This method can be used only by processors with number higher then 0
     * for block recursive Cholesky algorithm.
     *
     * @param ring      - algebraic space of current variables
     * @param intracomm - commutator for transport
     * @throws MPIException           - MPIException machining
     * @throws IOException            - IOException machining
     * @throws ClassNotFoundException - ClassNotFoundException machining
     */
    public static void choleskyDecomposition(Ring ring, Intracomm intracomm, int leaf)
            throws MPIException, IOException, ClassNotFoundException {
        if (!((Boolean) Transport.recvObject(0, 404, intracomm))) {
            intracomm.create(intracomm.getGroup().incl(new int[]{0}));
            return;
        }

        int rank = intracomm.getRank();
        int half = intracomm.getSize() / 2;

        Intracomm[] comm = new Intracomm[]{
                intracomm.create(intracomm.getGroup()
                        .incl(IntStream.rangeClosed(0, half - 1).toArray())),
                intracomm.create(intracomm.getGroup()
                        .incl(IntStream.rangeClosed(half, intracomm.getSize() - 1).toArray()))};
        if (rank < half) {
            choleskyDecomposition(ring, comm[0], leaf);
            MatrixMul.multiplyMatrix(ring, intracomm, leaf);
            MatrixMul.multiplyMatrixWithAdd(ring, intracomm, leaf);
            choleskyDecomposition(ring, comm[0], leaf);
        } else {
            MatrixMul.multiplyMatrix(ring, intracomm, leaf);
            MatrixMul.multiplyMatrixWithAdd(ring, intracomm, leaf);

            if (rank == half) {
                Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                MatrixS send = MatrixMul.multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[1], leaf);
                Transport.sendObject(send, 0, half, intracomm);
            } else MatrixMul.multiplyMatrix(ring, comm[1], leaf);

        }
        MatrixMul.multiplyMatrix(ring, intracomm, leaf);
    }


    /**
     * This method creates @MatrixS with @r raws and @c
     * columns filled by 0.
     *
     * @param r    - number of raws
     * @param c    - number of columns
     * @param ring - algebraic space of current variables
     * @return - MatrixS
     */
    public static MatrixS createZeroBlock(int r, int c, Ring ring) {
        Element[][] M = new Element[r][c];
        for (int i = 0; i < r; i++) {
            for (int j = 0; j < c; j++) {
                M[i][j] = ring.numberZERO;
            }
        }
        return new MatrixS(M, ring);
    }
}


