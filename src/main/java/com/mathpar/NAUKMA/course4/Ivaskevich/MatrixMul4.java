package com.mathpar.NAUKMA.course4.Ivaskevich;

import java.io.IOException;
import java.util.Random;
import com.mathpar.matrix.*;
import mpi.*;
import com.mathpar.number.*;


public class MatrixMul4 {
    static int tag = 0;
    static int mod = 13;

    private static Ring ring = new Ring("R64[x]");

    public static MatrixS multiply(MatrixS a, MatrixS b, MatrixS c, MatrixS d, Ring ring) {
        return (a.multiply(b, ring)).add(c.multiply(d, ring), ring);
    }

    public static void main(String[] args) throws MPIException, IOException, ClassNotFoundException {
        MPI.Init(new String[0]);
        // отримання номера вузла
        int rank = MPI.COMM_WORLD.getRank();
        if (rank == 0) {
            // програма виконується на нульовому процесорі
            ring.setMOD32(mod);
            int ord = 4;
            int den = 10000;
            // представник класу випадкового генератора
            Random rnd = new Random();
            // ord = розмір матриці, den = щільність
            MatrixS A = new MatrixS(ord, ord, den,
                    new int[]{5, 5}, rnd, NumberZp32.ONE, ring);
            System.out.println("Matrix A = " + A.toString());
            MatrixS B = new MatrixS(ord, ord, den,
                    new int[]{5, 5}, rnd, NumberZp32.ONE, ring);
            System.out.println("Matrix B = " + B.toString());

            MatrixS[] DD = new MatrixS[4];
            MatrixS CC = null;
            // розбиваємо матрицю A на 4 частини
            MatrixS[] AA = A.split();
            // розбиваємо матрицю В на 4 частини
            MatrixS[] BB = B.split();
            //посилка від нульового процесора масиву Object процесору 1 з ідентифікатором tag = 1
            Transport.sendArrayOfObjects(new Object[]{AA[0], BB[1],
                    AA[1], BB[3]}, 1, 1);
            //посилка від нульового процесора масиву Object процесору 2 з ідентифікатором tag = 2
            Transport.sendArrayOfObjects(new Object[]{AA[2], BB[0],
                    AA[3], BB[2]}, 2, 2);
            //посилка від нульового процесора масиву Object процесору 3 з ідентифікатором tag = 3
            Transport.sendArrayOfObjects(new Object[]{AA[2], BB[1],
                    AA[3], BB[3]}, 3, 3);
            //залишаємо один блок нульового процесору для обробки
            DD[0] = (AA[0].multiply(BB[0], ring)).
                    add(AA[1].multiply(BB[2], ring), ring);
            // приймаємо результат від першого процесора
            DD[1] = (MatrixS) Transport.recvObject(1, 1);
            System.out.println("recv 1 to 0");
            // приймаємо результат від другого процесора
            DD[2] = (MatrixS) Transport.recvObject(2, 2);
            System.out.println("recv 2 to 0");
            // приймаємо результат від третього процесора
            DD[3] = (MatrixS) Transport.recvObject(3, 3);
            System.out.println("recv 3 to 0");
            // процедура складання матриці з блоків DD[i] (i=0,...,3)
            CC = MatrixS.join(DD);
            System.out.println("RESULT MATRIX = " + CC.toString());
        } else {
            //програма виконується на процесорі з рангом rank
            System.out.println("I'm processor " + rank);
            ring.setMOD32(mod);
            // отримуємо масив  Object з блоками матриць від нульового процесора
            Object[] n = Transport.recvArrayOfObjects(0, rank);
            MatrixS a = (MatrixS) n[0];
            MatrixS b = (MatrixS) n[1];
            MatrixS c = (MatrixS) n[2];
            MatrixS d = (MatrixS) n[3];
            // перемножуємо i складаємо блоки матриць

            MatrixS res = multiply(a, b, c, d, ring);
            // посилаємо результат обчислень від процесора rank нульовому процесору
            System.out.println("res = " + res);
            Transport.sendObject(res, 0, rank);
            // повідомлення на консоль про те, що
            // результат буде посланий
            System.out.println("send result");
        }
        MPI.Finalize();
    }

    public static MatrixS multiplyMatrix4(MatrixS A, MatrixS B,int prNumbers[], Intracomm intracomm) throws MPIException, IOException, ClassNotFoundException {
        MatrixS result;
        Ring ring = new Ring("R64[x]");
        int rank = intracomm.getRank();
        ring.setMOD32(mod);

        if (rank == 0) {
            MatrixS[] DD = new MatrixS[4];
            MatrixS CC = null;
            MatrixS[] AA = A.split();
            MatrixS[] BB = B.split();
            Transport.sendArrayOfObjects(new Object[]{AA[0], BB[1],
                    AA[1], BB[3]}, prNumbers[1], 1);
            Transport.sendArrayOfObjects(new Object[]{AA[2], BB[0],
                    AA[3], BB[2]}, prNumbers[2], 2);
            Transport.sendArrayOfObjects(new Object[]{AA[2], BB[1],
                    AA[3], BB[3]}, prNumbers[3], 3);
            DD[0] = (AA[0].multiply(BB[0], ring)).
                    add(AA[1].multiply(BB[2], ring), ring);
            DD[1] = (MatrixS) Transport.recvObject(prNumbers[1], 1);
            DD[2] = (MatrixS) Transport.recvObject(prNumbers[2], 2);
            DD[3] = (MatrixS) Transport.recvObject(prNumbers[3], 3);
            CC = MatrixS.join(DD);
            result = CC;
        } else {
            Object[] n = Transport.recvArrayOfObjects(prNumbers[0], rank);
            MatrixS a = (MatrixS) n[0];
            MatrixS b = (MatrixS) n[1];
            MatrixS c = (MatrixS) n[2];
            MatrixS d = (MatrixS) n[3];

            MatrixS res = multiply(a, b, c, d, ring);
            Transport.sendObject(res, prNumbers[0], rank);
            result = res;
        }
        return result;
    }
}
