package com.mathpar.NAUKMA.course4.Ivaskevich.CourseWork;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Ring;
import mpi.Intracomm;
import mpi.MPIException;

import java.io.IOException;
import java.util.stream.IntStream;

public class MatrixMul {
    /**
     * This method multiplies matrix @A by matrix @B in algebraic
     * space @ring using commutator @intracomm for objects transport.
     * This method can be used for sequential algorithm, or also can
     * be used by 0 processor for parallel algorithm.
     *
     * @param A         - the first matrix for multiplication
     * @param B         - the second matrix for multiplication
     * @param ring      - algebraic space of current variables
     * @param intracomm - commutator for transport
     * @return - MatrixS object
     * @throws MPIException           - MPIException machining
     * @throws IOException            - IOException machining
     * @throws ClassNotFoundException - ClassNotFoundException machining
     */
    public static MatrixS multiplyMatrix(MatrixS A, MatrixS B, Ring ring, Intracomm intracomm, int leaf)
            throws MPIException, IOException, ClassNotFoundException {
        if (((A.size == leaf && A.colNumb == leaf)) && intracomm.getSize() != 1) {
            for (int i = 1; i < intracomm.getSize(); i++) Transport.sendObject(false, i, 404, intracomm);
            Intracomm comm = intracomm.create(intracomm.getGroup().incl(new int[]{0}));
            return multiplyMatrix(A, B, ring, comm, leaf);
        }
        for (int i = 1; i < intracomm.getSize(); i++) Transport.sendObject(true, i, 404, intracomm);

        switch (intracomm.getSize()) {
            case 1: {
                return A.multiply(B, ring);
            }
            case 2: {
                MatrixS[] AA = A.split();
                MatrixS[] BB = B.split();

                Transport.sendArrayOfObjects(new Object[]{
                        AA[2], AA[3], BB[0], BB[1], BB[2], BB[3]}, 1, 1, intracomm);
                MatrixS join0 = AA[0].multiply(BB[0], ring).add(AA[1].multiply(BB[2], ring), ring);
                MatrixS join1 = AA[0].multiply(BB[1], ring).add(AA[1].multiply(BB[3], ring), ring);
                Object[] recv = Transport.recvArrayOfObjects(2, 1, 0, intracomm);

                return MatrixS.join(new MatrixS[]{join0, join1, (MatrixS) recv[0], (MatrixS) recv[1]});
            }
            case 4: {
                MatrixS[] AA = A.split();
                MatrixS[] BB = B.split();

                Transport.sendArrayOfObjects(new Object[]{AA[0], AA[1], BB[1], BB[3]}, 1, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[2], AA[3], BB[0], BB[2]}, 2, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[2], AA[3], BB[1], BB[3]}, 3, 0, intracomm);

                return MatrixS.join(new MatrixS[]{
                        AA[0].multiply(BB[0], ring).add(AA[1].multiply(BB[2], ring), ring),
                        (MatrixS) Transport.recvObject(1, 1, intracomm),
                        (MatrixS) Transport.recvObject(2, 2, intracomm),
                        (MatrixS) Transport.recvObject(3, 3, intracomm)
                });
            }
            default:
                int splitProc = 8;
                int p = intracomm.getSize() / splitProc;

                Intracomm[] comm = new Intracomm[splitProc];

                for (int i = 0; i < splitProc; i++) {
                    comm[i] = intracomm.create(intracomm.getGroup().incl(IntStream
                            .rangeClosed(i * p, (i + 1) * p - 1).toArray()));
                }

                MatrixS[] AA = A.split();
                MatrixS[] BB = B.split();

                Transport.sendArrayOfObjects(new Object[]{AA[1], BB[2]}, p, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[0], BB[1]}, p * 2, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[1], BB[3]}, p * 3, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[2], BB[0]}, p * 4, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[3], BB[2]}, p * 5, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[2], BB[1]}, p * 6, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[3], BB[3]}, p * 7, 0, intracomm);

                return MatrixS.join(new MatrixS[]{
                        multiplyMatrix(AA[0], BB[0], ring, comm[0], leaf)
                                .add((MatrixS) Transport.recvObject(p, p, intracomm), ring),
                        (MatrixS) Transport.recvObject(p * 2, p * 2, intracomm),
                        (MatrixS) Transport.recvObject(p * 4, p * 4, intracomm),
                        (MatrixS) Transport.recvObject(p * 6, p * 6, intracomm)
                });
        }
    }

    /**
     * This method multiplies matrix @A by matrix @B in algebraic
     * space @ring using commutator @intracomm for objects transport.
     * This method can be used only by processors higher then 0 in
     * parallel algorithm.Commutator should consist of more then 1
     * processor.
     *
     * @param ring      - algebraic space of current variables
     * @param intracomm - commutator for transport
     * @throws MPIException           - MPIException machining
     * @throws IOException            - IOException machining
     * @throws ClassNotFoundException - ClassNotFoundException machining
     */
    public static void multiplyMatrix(Ring ring, Intracomm intracomm, int leaf)
            throws MPIException, IOException, ClassNotFoundException {
        if (!((Boolean) Transport.recvObject(0, 404, intracomm))) {
            intracomm.create(intracomm.getGroup().incl(new int[]{0}));
            return;
        }

        int rank = intracomm.getRank();
        switch (intracomm.getSize()) {
            case 2: {
                Object[] recv = Transport.recvArrayOfObjects(6, 0, 1, intracomm);
                MatrixS send1 = ((MatrixS) recv[0]).multiply((MatrixS) recv[2], ring)
                        .add(((MatrixS) recv[1]).multiply((MatrixS) recv[4], ring), ring);
                MatrixS send2 = ((MatrixS) recv[0]).multiply((MatrixS) recv[3], ring)
                        .add(((MatrixS) recv[1]).multiply((MatrixS) recv[5], ring), ring);
                Transport.sendArrayOfObjects(new Object[]{send1, send2}, 0, 0, intracomm);
            }
            break;
            case 4: {
                Object[] recv = Transport.recvArrayOfObjects(4, 0, 0, intracomm);
                MatrixS send = ((MatrixS) recv[0]).multiply((MatrixS) recv[2], ring)
                        .add(((MatrixS) recv[1]).multiply((MatrixS) recv[3], ring), ring);
                Transport.sendObject(send, 0, rank, intracomm);
            }
            break;
            default:
                int splitProc = 8;
                int p = intracomm.getSize() / splitProc;

                Intracomm[] comm = new Intracomm[splitProc];

                for (int i = 0; i < splitProc; i++) {
                    comm[i] = intracomm.create(intracomm.getGroup().incl(IntStream
                            .rangeClosed(i * p, (i + 1) * p - 1).toArray()));
                }

                if (rank > 0 && rank < p) {
                    multiplyMatrix(ring, comm[0], leaf);
                } else if (rank >= p && rank < p * 2) {
                    if (rank > p) {
                        multiplyMatrix(ring, comm[1], leaf);
                    } else {
                        Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[1], leaf);
                        Transport.sendObject(tempRes, 0, p, intracomm);
                    }
                } else if (rank >= p * 2 && rank < p * 3) {
                    if (rank > p * 2) {
                        multiplyMatrix(ring, comm[2], leaf);
                    } else {
                        Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[2], leaf);
                        MatrixS sendRes = tempRes.
                                add((MatrixS) Transport.recvObject(p * 3, p * 3, intracomm), ring);
                        Transport.sendObject(sendRes, 0, p * 2, intracomm);
                    }
                } else if (rank >= p * 3 && rank < p * 4) {
                    if (rank > p * 3) {
                        multiplyMatrix(ring, comm[3], leaf);
                    } else {
                        Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[3], leaf);
                        Transport.sendObject(tempRes, p * 2, p * 3, intracomm);
                    }
                } else if (rank >= p * 4 && rank < p * 5) {
                    if (rank > p * 4) {
                        multiplyMatrix(ring, comm[4], leaf);
                    } else {
                        Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[4], leaf);
                        MatrixS sendRes = tempRes
                                .add((MatrixS) Transport.recvObject(p * 5, p * 5, intracomm), ring);
                        Transport.sendObject(sendRes, 0, p * 4, intracomm);
                    }
                } else if (rank >= p * 5 && rank < p * 6) {
                    if (rank > p * 5) {
                        multiplyMatrix(ring, comm[5], leaf);
                    } else {
                        Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[5], leaf);
                        Transport.sendObject(tempRes, p * 4, p * 5, intracomm);
                    }
                } else if (rank >= p * 6 && rank < p * 7) {
                    if (rank > p * 6) {
                        multiplyMatrix(ring, comm[6], leaf);
                    } else {
                        Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[6], leaf);
                        MatrixS sendRes = tempRes.
                                add((MatrixS) Transport.recvObject(p * 7, p * 7, intracomm), ring);
                        Transport.sendObject(sendRes, 0, p * 6, intracomm);
                    }
                } else if (rank >= p * 7 && rank < p * 8) {
                    if (rank > p * 7) {
                        multiplyMatrix(ring, comm[7], leaf);
                    } else {
                        Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[7], leaf);
                        Transport.sendObject(tempRes, p * 6, p * 7, intracomm);
                    }
                }

        }
    }

    /**
     * This method multiplies matrix @A by matrix @B and adds a matrix C to the
     * result of multiplication in algebraic space @ring using commutator @intracomm
     * for objects transport.This method can be used for sequential algorithm, or
     * also can be used by 0 processor for parallel algorithm.
     *
     * @param A         - the first matrix for multiplication
     * @param B         - the second matrix for multiplication
     * @param C         - the matrix for the sum with the result of multiplication
     * @param ring      - algebraic space of current variables
     * @param intracomm - commutator for transport
     * @return - MatrixS object
     * @throws MPIException           - MPIException machining
     * @throws IOException            - IOException machining
     * @throws ClassNotFoundException - ClassNotFoundException machining
     */
    public static MatrixS multiplyMatrixWithAdd(MatrixS A, MatrixS B, MatrixS C, Ring ring, Intracomm intracomm, int leaf)
            throws MPIException, IOException, ClassNotFoundException {
        if (((A.size == leaf && A.colNumb == leaf)) && intracomm.getSize() != 1) {
            for (int i = 1; i < intracomm.getSize(); i++) Transport.sendObject(false, i, 404, intracomm);
            Intracomm comm = intracomm.create(intracomm.getGroup().incl(new int[]{0}));
            return multiplyMatrixWithAdd(A, B, C, ring, comm, leaf);
        }
        for (int i = 1; i < intracomm.getSize(); i++) Transport.sendObject(true, i, 404, intracomm);

        switch (intracomm.getSize()) {
            case 1: {
                return A.multiply(B, ring).add(C, ring);
            }
            case 2: {
                MatrixS[] AA = A.split();
                MatrixS[] BB = B.split();
                MatrixS[] CC = C.split();

                Transport.sendArrayOfObjects(new Object[]{
                        AA[2], AA[3], BB[0], BB[1], BB[2], BB[3], CC[2], CC[3]}, 1, 1, intracomm);
                MatrixS join0 = AA[0].multiply(BB[0], ring).add(AA[1].multiply(BB[2], ring), ring).add(CC[0], ring);
                MatrixS join1 = AA[0].multiply(BB[1], ring).add(AA[1].multiply(BB[3], ring), ring).add(CC[1], ring);
                Object[] recv = Transport.recvArrayOfObjects(2, 1, 0, intracomm);

                return MatrixS.join(new MatrixS[]{
                        join0, join1, (MatrixS) recv[0], (MatrixS) recv[1]
                });
            }
            case 4: {
                MatrixS[] AA = A.split();
                MatrixS[] BB = B.split();
                MatrixS[] CC = C.split();

                Transport.sendArrayOfObjects(
                        new Object[]{AA[0], AA[1], BB[1], BB[3], CC[1]}, 1, 0, intracomm);
                Transport.sendArrayOfObjects(
                        new Object[]{AA[2], AA[3], BB[0], BB[2], CC[2]}, 2, 0, intracomm);
                Transport.sendArrayOfObjects(
                        new Object[]{AA[2], AA[3], BB[1], BB[3], CC[3]}, 3, 0, intracomm);

                return MatrixS.join(new MatrixS[]{
                        AA[0].multiply(BB[0], ring).add(AA[1].multiply(BB[2], ring), ring).add(CC[0], ring),
                        (MatrixS) Transport.recvObject(1, 1, intracomm),
                        (MatrixS) Transport.recvObject(2, 2, intracomm),
                        (MatrixS) Transport.recvObject(3, 3, intracomm)
                });
            }
            default:
                int splitProc = 8;
                int p = intracomm.getSize() / splitProc;

                Intracomm[] comm = new Intracomm[splitProc];

                for (int i = 0; i < splitProc; i++) {
                    comm[i] = intracomm.create(intracomm.getGroup().incl(IntStream
                            .rangeClosed(i * p, (i + 1) * p - 1).toArray()));
                }

                MatrixS[] AA = A.split();
                MatrixS[] BB = B.split();
                MatrixS[] CC = C.split();
                MatrixS[] join = new MatrixS[4];

                Transport.sendArrayOfObjects(new Object[]{AA[1], BB[2], CC[0]}, p, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[0], BB[1]}, p * 2, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[1], BB[3], CC[1]}, p * 3, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[2], BB[0]}, p * 4, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[3], BB[2], CC[2]}, p * 5, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[2], BB[1]}, p * 6, 0, intracomm);
                Transport.sendArrayOfObjects(new Object[]{AA[3], BB[3], CC[3]}, p * 7, 0, intracomm);

                join[0] = multiplyMatrix(AA[0], BB[0], ring, comm[0], leaf)
                        .add((MatrixS) Transport.recvObject(p, p, intracomm), ring);
                join[1] = (MatrixS) Transport.recvObject(p * 2, p * 2, intracomm);
                join[2] = (MatrixS) Transport.recvObject(p * 4, p * 4, intracomm);
                join[3] = (MatrixS) Transport.recvObject(p * 6, p * 6, intracomm);

                return MatrixS.join(join);
        }
    }

    /**
     * This method multiplies matrix @A by matrix @B and adds a matrix C to the
     * result of multiplication in algebraic space @ring using commutator @intracomm
     * for objects transport. This method can be used only by processors higher then
     * 0 in parallel algorithm. Commutator should consist of more then 1 processor.
     *
     * @param ring      - algebraic space of current variables
     * @param intracomm - commutator for transport
     * @throws MPIException           - MPIException machining
     * @throws IOException            - IOException machining
     * @throws ClassNotFoundException - ClassNotFoundException machining
     */
    public static void multiplyMatrixWithAdd(Ring ring, Intracomm intracomm, int leaf)
            throws MPIException, IOException, ClassNotFoundException {
        if (!((Boolean) Transport.recvObject(0, 404, intracomm))) {
            intracomm.create(intracomm.getGroup().incl(new int[]{0}));
            return;
        }

        int rank = intracomm.getRank();
        switch (intracomm.getSize()) {
            case 2: {
                Object[] recv = Transport.recvArrayOfObjects(8, 0, 1, intracomm);
                MatrixS send1 = ((MatrixS) recv[0]).multiply((MatrixS) recv[2], ring).add(((MatrixS) recv[1])
                        .multiply((MatrixS) recv[4], ring), ring).add((MatrixS) recv[6], ring);
                MatrixS send2 = ((MatrixS) recv[0]).multiply((MatrixS) recv[3], ring).add(((MatrixS) recv[1])
                        .multiply((MatrixS) recv[5], ring), ring).add((MatrixS) recv[7], ring);
                Transport.sendArrayOfObjects(new Object[]{send1, send2}, 0, 0, intracomm);
            }
            break;
            case 4: {
                Object[] recv = Transport.recvArrayOfObjects(5, 0, 0, intracomm);
                MatrixS send = ((MatrixS) recv[0]).multiply((MatrixS) recv[2], ring).add(((MatrixS) recv[1])
                        .multiply((MatrixS) recv[3], ring), ring).add((MatrixS) recv[4], ring);
                Transport.sendObject(send, 0, rank, intracomm);
            }
            break;
            default:
                int splitProc = 8;
                int p = intracomm.getSize() / splitProc;

                Intracomm[] comm = new Intracomm[splitProc];

                for (int i = 0; i < splitProc; i++) {
                    comm[i] = intracomm.create(intracomm.getGroup().incl(IntStream
                            .rangeClosed(i * p, (i + 1) * p - 1).toArray()));
                }
            {
                if (rank > 0 && rank < p) {
                    multiplyMatrix(ring, comm[0], leaf);
                } else if (rank >= p && rank < p * 2) {
                    if (rank == p) {
                        Object[] recv = Transport.recvArrayOfObjects(3, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrixWithAdd(
                                (MatrixS) recv[0], (MatrixS) recv[1], (MatrixS) recv[2], ring, comm[1], leaf);
                        Transport.sendObject(tempRes, 0, p, intracomm);
                    } else multiplyMatrixWithAdd(ring, comm[1], leaf);


                } else if (rank >= p * 2 && rank < p * 3) {
                    if (rank == p * 2) {
                        Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[2], leaf);
                        MatrixS sendRes = tempRes
                                .add((MatrixS) Transport.recvObject(p * 3, p * 3, intracomm), ring);
                        Transport.sendObject(sendRes, 0, p * 2, intracomm);
                    } else multiplyMatrix(ring, comm[2], leaf);

                } else if (rank >= p * 3 && rank < p * 4) {
                    if (rank == p * 3) {
                        Object[] recv = Transport.recvArrayOfObjects(3, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrixWithAdd(
                                (MatrixS) recv[0], (MatrixS) recv[1], (MatrixS) recv[2], ring, comm[3], leaf);
                        Transport.sendObject(tempRes, p * 2, p * 3, intracomm);
                    } else multiplyMatrixWithAdd(ring, comm[3], leaf);

                } else if (rank >= p * 4 && rank < p * 5) {
                    if (rank == p * 4) {
                        Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[4], leaf);
                        MatrixS sendRes = tempRes
                                .add((MatrixS) Transport.recvObject(p * 5, p * 5, intracomm), ring);
                        Transport.sendObject(sendRes, 0, p * 4, intracomm);
                    } else multiplyMatrix(ring, comm[4], leaf);
                } else if (rank >= p * 5 && rank < p * 6) {
                    if (rank == p * 5) {
                        Object[] recv = Transport.recvArrayOfObjects(3, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrixWithAdd(
                                (MatrixS) recv[0], (MatrixS) recv[1], (MatrixS) recv[2], ring, comm[5], leaf);
                        Transport.sendObject(tempRes, p * 4, p * 5, intracomm);
                    } else multiplyMatrixWithAdd(ring, comm[5], leaf);

                } else if (rank >= p * 6 && rank < p * 7) {
                    if (rank == p * 6) {
                        Object[] recv = Transport.recvArrayOfObjects(2, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrix((MatrixS) recv[0], (MatrixS) recv[1], ring, comm[6], leaf);
                        MatrixS sendRes = tempRes
                                .add((MatrixS) Transport.recvObject(p * 7, p * 7, intracomm), ring);
                        Transport.sendObject(sendRes, 0, p * 6, intracomm);

                    } else multiplyMatrix(ring, comm[6], leaf);
                } else if (rank >= p * 7 && rank < p * 8) {
                    if (rank == p * 7) {
                        Object[] recv = Transport.recvArrayOfObjects(3, 0, 0, intracomm);
                        MatrixS tempRes = multiplyMatrixWithAdd(
                                (MatrixS) recv[0], (MatrixS) recv[1], (MatrixS) recv[2], ring, comm[7], leaf);
                        Transport.sendObject(tempRes, p * 6, p * 7, intracomm);

                    } else multiplyMatrixWithAdd(ring, comm[7], leaf);
                }
            }
        }
    }
}
