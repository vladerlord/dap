package com.mathpar.NAUKMA.course4.Ivaskevich;

import mpi.MPI;
import mpi.MPIException;

/*
mpirun --hostfile /home/andriy/hostfile -np 4 java -cp /home/andriy/mpi-dap/target/classes com/mathpar/NAUKMA/course4/Ivaskevich/TestReduce 10
*/

/*

Output:

 0
 4
 8
 12
 16
 20
 24
 28
 32
 36
 */
public class TestReduce {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        int n = Integer.parseInt(args[0]);
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = i;
        }
        int[] q = new int[n];
        MPI.COMM_WORLD.reduce(a, q, n, MPI.INT, MPI.SUM, 0);
        if (myrank == 0) {
            for (int i = 0; i < q.length; i++) {
                System.out.println(" " + q[i]);
            }
        }
        // завершення паралельної частини
        MPI.Finalize();
    }
}
