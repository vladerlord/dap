package com.mathpar.NAUKMA.course4.Ivaskevich;

import java.io.IOException;
import java.util.Random;

import com.mathpar.matrix.*;
import mpi.*;
import com.mathpar.number.*;


public class MatrixMul8 {
    public static  Ring ring = new Ring("R64[x]");

    public static void main(String[] args)
            throws MPIException, IOException,
            ClassNotFoundException {
// инициализация MPI
        MPI.Init(new String[0]);
// получение номера узла
        int rank = MPI.COMM_WORLD.getRank();
        if (rank == 0) {
            // программа выполняется на нулевом процессоре
            int ord = 4;
            int den = 10000;
// представитель класса случайного генератора
            Random rnd = new Random();
// ord = размер матрицы, den = плотность
            MatrixS A = new MatrixS(ord, ord, den, new int[] {5, 3},
                    rnd, NumberZp32.ONE, ring);
            System.out.println("Matrix A = " + A.toString());
            MatrixS B = new MatrixS(ord, ord, den, new int[] {5, 3},
                    rnd, NumberZp32.ONE, ring);
            System.out.println("Matrix B = " + B.toString());
            MatrixS D = null;
// разбиваем матрицу A на 4 части
            MatrixS[] AA = A.split();
            System.out.println("Split matrix A");
// разбиваем матрицу B на 4 части
            MatrixS[] BB = B.split();
            System.out.println("Split matrix B");
            int tag = 0;
// посылка от нулевого процессора массива Object процессору rank с идентификатором tag
            System.out.println("Sending objects to proc 1");
            System.out.println(AA[1] + " " + BB[2]);
            Transport.sendObjects(new Object[] {AA[1], BB[2]},
                    1, tag);
            System.out.println("Sending objects to proc 2");
            System.out.println(AA[0] + " " + BB[1]);
            Transport.sendObjects(new Object[] {AA[0], BB[1]},
                    2, tag);
            System.out.println("Sending objects to proc 3");
            System.out.println(AA[1] + " " + BB[3]);
            Transport.sendObjects(new Object[] {AA[1], BB[3]},
                    3, tag);
            System.out.println("Sending objects to proc 4");
            System.out.println(AA[2] + " " + BB[0]);
            Transport.sendObjects(new Object[] {AA[2], BB[0]},
                    4, tag);
            System.out.println("Sending objects to proc 5");
            System.out.println(AA[3] + " " + BB[2]);
            Transport.sendObjects(new Object[] {AA[3], BB[2]},
                    5, tag);
            System.out.println("Sending objects to proc 6");
            System.out.println(AA[2] + " " + BB[1]);
            Transport.sendObjects(new Object[] {AA[2], BB[1]},
                    6, tag);
            System.out.println("Sending objects to proc 7");
            System.out.println(AA[3] + " " + BB[3]);
            Transport.sendObjects(new Object[] {AA[3], BB[3]},
                    7, tag);

            MatrixS[] DD = new MatrixS[4];
// оставляем один блок нулевому процессору для обработки
            DD[0] = (AA[0].multiply(BB[0], ring)).add((MatrixS) Transport.recvObject(1, 3),
                    ring);
            DD[1] = (MatrixS) Transport.recvObject(2, 3);
            DD[2] = (MatrixS) Transport.recvObject(4, 3);
            DD[3] = (MatrixS) Transport.recvObject(6, 3);
            D = MatrixS.join(DD);
            System.out.println("RES = " + D.toString());
        } else {
// программа выполняется на процессоре с рангом rank
            System.out.println("I’m processor " + rank);
            Object[] b = Transport.recvObjects(2, 0, 0);
            System.out.println("Receiving objects from rank 0");
            MatrixS[] a = new MatrixS[b.length];
            for (int i = 0; i < b.length; i++) {
                a[i] = (MatrixS) b[i];
            }
            MatrixS res = a[0].multiply(a[1], ring);
            if (rank % 2 == 0) {
                MatrixS p = res.add((MatrixS) Transport.
                        recvObject(rank + 1, 3), ring);
                System.out.println("Rank % 2: " + rank);
                System.out.println("Send to proc 0: " + p);
                Transport.sendObject(p, 0, 3);
            } else {
                System.out.println("Rank: " + rank + " Send to proc 0: " + res);
                Transport.sendObject(res, rank - 1, 3);
            }
        }
        MPI.Finalize();
    }

    public static MatrixS multiplyMatrix8(MatrixS A, MatrixS B, Intracomm intracomm)
            throws MPIException, IOException, ClassNotFoundException {
        int rank = intracomm.getRank();
        MatrixS result = null;
        if (rank == 0) {
            int ord = 4;
            int den = 10000;
            Random rnd = new Random();
            MatrixS D = null;
            MatrixS[] AA = A.split();
            MatrixS[] BB = B.split();
            int tag = 0;
            Transport.sendObjects(new Object[] {AA[1], BB[2]},
                    1, tag);
            Transport.sendObjects(new Object[] {AA[0], BB[1]},
                    2, tag);
            Transport.sendObjects(new Object[] {AA[1], BB[3]},
                    3, tag);
            Transport.sendObjects(new Object[] {AA[2], BB[0]},
                    4, tag);
            Transport.sendObjects(new Object[] {AA[3], BB[2]},
                    5, tag);
            Transport.sendObjects(new Object[] {AA[2], BB[1]},
                    6, tag);
            Transport.sendObjects(new Object[] {AA[3], BB[3]},
                    7, tag);

            MatrixS[] DD = new MatrixS[4];
            DD[0] = (AA[0].multiply(BB[0], ring)).add((MatrixS) Transport.recvObject(1, 3),
                    ring);
            DD[1] = (MatrixS) Transport.recvObject(2, 3);
            DD[2] = (MatrixS) Transport.recvObject(4, 3);
            DD[3] = (MatrixS) Transport.recvObject(6, 3);
            D = MatrixS.join(DD);
            result = D;
        } else {
            Object[] b = Transport.recvObjects(2, 0, 0);
            MatrixS[] a = new MatrixS[b.length];
            for (int i = 0; i < b.length; i++) {
                a[i] = (MatrixS) b[i];
            }
            MatrixS res = a[0].multiply(a[1], ring);
            if (rank % 2 == 0) {
                MatrixS p = res.add((MatrixS) Transport.
                        recvObject(rank + 1, 3), ring);
                Transport.sendObject(p, 0, 3);
            } else {
                Transport.sendObject(res, rank - 1, 3);
            }
        }
        return result;
    }
}