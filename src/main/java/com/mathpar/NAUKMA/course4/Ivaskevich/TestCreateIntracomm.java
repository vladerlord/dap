package com.mathpar.NAUKMA.course4.Ivaskevich;

import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;

import java.util.Random;

/*
mpirun --hostfile /home/andriy/hostfile -np 2 java -cp /home/andriy/mpi-dap/target/classes com/mathpar/NAUKMA/course4/Ivaskevich/TestCreateIntracomm 10
 */

/*
a[0]= 0.6505796334310785
a[1]= 0.4921194982204462
a[2]= 0.2145289846750128
a[3]= 0.49388853537393296
a[4]= 0.36475439265016785
a[5]= 0.03383919530752333
a[6]= 0.41370766991291663
a[7]= 0.9676883760622704
a[8]= 0.08074556968208246
a[9]= 0.2897540825958328
a[0]= 0.6505796334310785
a[1]= 0.4921194982204462
a[2]= 0.2145289846750128
a[3]= 0.49388853537393296
a[4]= 0.36475439265016785
a[5]= 0.03383919530752333
a[6]= 0.41370766991291663
a[7]= 0.9676883760622704
a[8]= 0.08074556968208246
a[9]= 0.2897540825958328

 */

public class TestCreateIntracomm {
    public static void main(String[] args) throws MPIException {
        // ініціалізація MPI
        MPI.Init(args);
        // визначаємо нову групу працюючих процесорів
        mpi.Group g = MPI.COMM_WORLD.getGroup().incl(
                new int[]{0, 1});
        // створюємо новий комунікатор
        Intracomm COMM_NEW = MPI.COMM_WORLD.create(g);
        int myrank = COMM_NEW.getRank();
        int n = Integer.parseInt(args[0]);
        double[] a = new double[n];
        if (myrank == 0) {
            for (int i = 0; i < n; i++) {
                a[i] = new Random().nextDouble();
                System.out.println("a[" + i + "]= " + a[i]);
            }
        }
        COMM_NEW.barrier();
        // застосовуємо до нового комунікатора функцію bcast
        COMM_NEW.bcast(a, a.length, MPI.DOUBLE, 0);
        if (myrank != 0) {
            for (int i = 0; i < n; i++) {
                System.out.println("a[" + i + "]= " + a[i]);
            }
        }
        // завершення парарельної частини
        MPI.Finalize();
    }
}