package com.mathpar.NAUKMA.course4.Ivaskevich.CholeskyDecomposition;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberR;
import com.mathpar.number.Ring;

import java.util.Random;

public class CholeskyUtil {
    public static MatrixS createMatrix(int size, Ring ring) {
        Element[][] result = new Element[size][size];
        int start = 4;
        for (int i = 0; i < result.length; i++)
            for (int j = 0; j < result.length; j++) result[i][j] = new NumberR(i == j ? start++ : 1);
        return new MatrixS(result, ring);
    }

    public static MatrixS createLMatrix(int size, int density, Ring ring) {
        MatrixS matrix = new MatrixS(size, size, density, new int[]{3}, new Random(), ring.numberONE(), ring);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (i < j) {
                    matrix.putElement(ring.numberZERO, i, j);
                } else if (i == j) {
                    if (matrix.getElement(i, j, ring).isZero(ring)) {
                        matrix.putElement(ring.numberONE, i, j);
                    }
                }
            }
        }
        return matrix;
    }

    public static MatrixS fillAndJoinMatrix(MatrixS a, MatrixS b, MatrixS c, Ring ring) {
        return MatrixS.join(new MatrixS[]{a, createSquareZeroBlock(a.size, ring), b, c});
    }

    public static MatrixS createSquareZeroBlock(int n, Ring ring) {
        Element[][] m = new Element[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++) m[i][j] = ring.numberZERO;
        return new MatrixS(m, ring);
    }

    public static Element checkResult(MatrixS init, MatrixS result, Ring ring) {
        MatrixS LLT = result.multiply(result.transpose(), ring);
        MatrixS res = LLT.subtract(init, ring);
        return res.getMaxElement(ring);
    }

    public static long checkMemory() {
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }
}
