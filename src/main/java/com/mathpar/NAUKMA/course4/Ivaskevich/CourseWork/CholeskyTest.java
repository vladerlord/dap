package com.mathpar.NAUKMA.course4.Ivaskevich.CourseWork;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Ring;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;

import static com.mathpar.NAUKMA.course4.Ivaskevich.CholeskyDecomposition.CholeskyUtil.checkMemory;
import static com.mathpar.NAUKMA.course4.Ivaskevich.CholeskyDecomposition.CholeskyUtil.createLMatrix;
import static com.mathpar.NAUKMA.course4.Ivaskevich.CourseWork.CholeskyDecomposition.choleskyDecomposition;

public class CholeskyTest {
    private static final Ring ring = new Ring("R[]");

    public static void main(String[] args) throws MPIException, IOException, ClassNotFoundException {
        ring.setAccuracy(Integer.parseInt(args[5]));
        ring.setMachineEpsilonR(Integer.parseInt(args[5]) - 1);
        ring.setFLOATPOS(Integer.parseInt(args[5]) + 10);

        MPI.Init(new String[0]);
        int stepSize = Integer.parseInt(args[0]);
        while (stepSize <= Integer.parseInt(args[1])) {
            long testTime = 0;
            if (MPI.COMM_WORLD.getRank() == 0) testTime = System.nanoTime();

            MatrixS LMatrix = createLMatrix(stepSize, Integer.parseInt(args[4]), ring);
            MatrixS inputtedMatrix = LMatrix.multiply(LMatrix.transpose(), ring);

            long[] maxUsedMemoryArr = new long[log2(Integer.parseInt(args[3]) / Integer.parseInt(args[2])) + 1];
            long[] leafArr = new long[log2(Integer.parseInt(args[3]) / Integer.parseInt(args[2])) + 1];

            long usedMemoryBefore, usedMemoryAfter;

            String resultHead = " Matrix size | Proc numb | Leaf | Density |  Exec time  |    Max mistake    ";
            String memoryHead = " Leaf |  Max used memory ";

            StringBuilder out = new StringBuilder().append("\n|").append(resultHead).append("|");

            int stepLeaf = Integer.parseInt(args[2]);
            int inc = 0;
            while (stepLeaf <= Integer.parseInt(args[3])) {
                if (MPI.COMM_WORLD.getRank() == 0) {
                    long startTime = System.nanoTime();
                    usedMemoryBefore = checkMemory();
                    MatrixS[] result = choleskyDecomposition(inputtedMatrix, ring, MPI.COMM_WORLD, stepLeaf);
                    usedMemoryAfter = checkMemory();
                    long endTime = System.nanoTime();
                    long duration = endTime - startTime;

                    MatrixS miscalculations = result[0].subtract(LMatrix, ring);

                    String execTime = ((duration / 1000000000) == 0) ?
                            "0." + String.valueOf(duration).substring(0, 1) + " sec" :
                            duration / 1000000000 + " sec";

                    String mis = miscalculations.getMaxElement(ring).toString();
                    if (mis.length() >= "    Max mistake    ".length()) {
                        if (mis.contains("E")) {
                            String[] arr = mis.split("[E]");
                            if (arr[0].contains(".")) {
                                String[] arr1 = arr[0].split("\\.");
                                mis = arr1[0] + (arr1[1].charAt(0) == '0' ? "" : "." + arr1[1].charAt(0)) + "E" + arr[1];
                            }
                        } else {
                            if (mis.contains(".")) {
                                String[] arr = mis.split("\\.");
                                mis = arr[0] + (arr[1].charAt(0) == '0' ? "" : arr[1].charAt(0));
                            }
                        }
                    }
                    out.append("\n").append(fillTableRow(resultHead,
                            new String[]{stepSize + "x" + stepSize, String.valueOf(MPI.COMM_WORLD.getSize()),
                                    String.valueOf(stepLeaf), args[4], execTime, mis},
                            "\\|", "|"));
//                    System.out.println(out);
//                    System.out.println("\nUsed time after " + (inc + 1) + " stepLeaf : " + (System.nanoTime() - testTime) / 1000000000);
                } else {
                    usedMemoryBefore = checkMemory();
                    choleskyDecomposition(ring, MPI.COMM_WORLD, stepLeaf);
                    usedMemoryAfter = checkMemory();
                }
                maxUsedMemoryArr[inc] = Math.max(usedMemoryBefore, usedMemoryAfter);
                leafArr[inc] = stepLeaf;
                stepLeaf *= 2;
                inc++;
            }

            StringBuilder outMemory = new StringBuilder(
                    "\n#  Used memory by proc " + MPI.COMM_WORLD.getRank() + "  #\n");
            outMemory.append("|").append(memoryHead).append("|");
            for (int i = 0; i < maxUsedMemoryArr.length; i++) {
                outMemory.append("\n").append(fillTableRow(memoryHead, new String[]{
                        String.valueOf(leafArr[i]), maxUsedMemoryArr[i] / (1024 * 1024) + " Mb"}, "\\|", "|"));
            }

            System.out.println(outMemory);
            if (MPI.COMM_WORLD.getRank() == 0) {
                System.out.println(out.append("\n"));
                System.out.println("Accuracy : " + Integer.parseInt(args[5]));
                System.out.println("\nTotal test time : " + (System.nanoTime() - testTime) / 1000000000);
            }
            stepSize *= 2;
        }

        MPI.Finalize();
    }


    public static int log2(int x) {
        return (int) (Math.log(x) / Math.log(2));
    }

    public static String fillTableRow(String thead, String[] params, String regex, String line) {
        StringBuilder row = new StringBuilder().append(line);
        String[] theadParts = thead.split(regex);
        for (int i = 0; i < theadParts.length; i++) {
            int diff = theadParts[i].length() - params[i].length();
            if (diff < 0) {
                row.append("  ")
                        .append(params[i]).append("  ").append(line);
            } else {
                if (diff % 2 == 0)
                    row.append(repeat(" ", diff / 2))
                            .append(params[i]).append(repeat(" ", diff / 2)).append(line);
                else
                    row.append(repeat(" ", diff / 2))
                            .append(params[i]).append(repeat(" ", diff / 2 + 1)).append(line);
            }

        }
        return row.toString();
    }

    public static String repeat(String s, int n) {
        StringBuilder sb = new StringBuilder(s.length() * n);
        for (int i = 0; i < n; i++)
            sb.append(s);
        return sb.toString();
    }
}
