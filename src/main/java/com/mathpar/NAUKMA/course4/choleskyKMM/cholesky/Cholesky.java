package com.mathpar.NAUKMA.course4.choleskyKMM.cholesky;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.*;
import mpi.MPIException;
import java.io.IOException;
import java.util.Random;


public class Cholesky {
    private static final Ring ring = new Ring("R64[x]");

    public static MatrixS[] cholesky(MatrixS arr) {
        int N = arr.size;
        MatrixS a, bT, a1, b, c, c1, z0, z1, z, tmp, del;
        if (N == 1) {
            double x = arr.getElement(0, 0, ring).value;
            MatrixS l = new MatrixS(new NumberR64(Math.sqrt(x)));
            MatrixS l1 = new MatrixS(new NumberR64(1. / Math.sqrt(x)));
            MatrixS[] res = new MatrixS[]{l, l1};
            return res;
        } else {
            // split A into 4 parts (A, B, D),  B^T not interested
            MatrixS[] matrices = arr.split();
            MatrixS A = matrices[0];
            MatrixS B = matrices[1];
            MatrixS D = matrices[3];
            MatrixS[] cholMatrix1 = cholesky(A);
            a = cholMatrix1[0];
            a1 = cholMatrix1[1];
            bT = a1.multiply(B, ring);
            b = bT.transpose();
            tmp = b.multiply(bT, ring);
            del = D.subtract(tmp, ring);
            MatrixS[] cholMatrix2 = cholesky(del);
            c = cholMatrix2[0];
            c1 = cholMatrix2[1];
            Element el = new Element(-1.0);
            z0 = c1.negate(ring);
            z1 = z0.multiply(b, ring);
            z = z1.multiply(a1, ring);
        }
        MatrixS[] fst = new MatrixS[4];
        MatrixS[] snd = new MatrixS[4];

        Element[][] filledZero = new Element[N / 2][N / 2];
        for (int i = 0; i < N / 2; i++) {
            for (int j = 0; j < N / 2; j++) {
                filledZero[i][j] = new NumberR64(0.0);
            }
        }
        MatrixS zeroes = new MatrixS(filledZero, ring);

        fst[0] = a;
        fst[1] = zeroes;
        fst[2] = b;
        fst[3] = c;
        snd[0] = a1;
        snd[1] = zeroes;
        snd[2] = z;
        snd[3] = c1;

        MatrixS L = MatrixS.join(fst);
        MatrixS L1 = MatrixS.join(snd);

        MatrixS[] result = new MatrixS[]{L, L1};
        return result;
    }

    // метод, що генерує позитивно-визначену симетричну матрицю
    public static MatrixS generate(int n) {
        Random rnd = new Random();
        Element[][] elements = new Element[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j <= i)
                    elements[i][j] = new NumberR64(rnd.nextDouble() * 50);
                else
                    elements[i][j] = new NumberR64(0);
            }
        }
        MatrixS m = new MatrixS(elements, ring);
        MatrixS mT = m.transpose();
        return m.multiply(mT, ring);
    }

    public static void main(String[] args)
            throws MPIException, IOException,
            ClassNotFoundException {
        int n = Integer.parseInt(args[0]);
        MatrixS m = generate(n);
        System.out.println(m.toString());
        MatrixS l = cholesky(m)[0];
        System.out.println("L = " + l.toString());
        MatrixS lT = l.transpose();
        System.out.println("L^T = " + lT.toString());
        MatrixS init = l.multiply(lT, ring);
        System.out.println("L*L^T = " + init.toString());
    }
}