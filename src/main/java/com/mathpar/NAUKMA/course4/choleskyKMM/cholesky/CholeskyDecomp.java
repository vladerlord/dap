package com.mathpar.NAUKMA.course4.choleskyKMM.cholesky;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberR64;
import com.mathpar.number.Ring;
import java.io.IOException;
import java.util.Random;
import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;

/**
 * @author mykola (Mykola Yeshchenko)
 */
//mpirun -hostfile /home/mykola/hostfile -np 8 java -cp /home/mykola/mpi-dap/target/classes com/mathpar/NAUKMA/course4/choleskyKMM/cholesky/CholeskyDecomp 2

public class CholeskyDecomp {
    private static final Ring ring = new Ring("R64[x]");

    public static void main(String[] args) throws MPIException, IOException, ClassNotFoundException {
        MPI.Init(new String[0]);

        MatrixS inputtedMatrix = generate(Integer.parseInt(args[0]), ring);
        MatrixS[] result = choleskyDecomposition(inputtedMatrix, MPI.COMM_WORLD);
        if (MPI.COMM_WORLD.getRank() == 0) {
            System.out.println("\nInputted matrix : " + inputtedMatrix);
            System.out.println("\nResult L matrix : " + result[0]
                    + "\n\n" + "Result L-inverted  matrix : " + result[1]
                    + "\n\n" + "Result L-transposed  matrix : " + result[0].transpose()
                    + "\n\n" + "Result L*LT matrix : " + result[0].multiply(result[0].transpose(), ring)
                    + "\n\n" + "Result L*LT - input matrix : " + (result[0].multiply(result[0].transpose(), ring)).subtract(inputtedMatrix,ring) + "\n");
        }
        MPI.Finalize();
    }

    public static MatrixS[] choleskyDecomposition(MatrixS A, Intracomm intracomm) throws MPIException, IOException, ClassNotFoundException {
        MatrixS L, LInv;
        MatrixS[] result = new MatrixS[2];

        switch (intracomm.getSize()) {
            case 2: {
                if (intracomm.getRank() == 0) {
                    MatrixS[] ABlocks = A.split();
                    MatrixS[] resA = Cholesky.cholesky(ABlocks[0]);
                    MatrixS bT = resA[1].multiply(ABlocks[1], ring);
                    MatrixS b = bT.transpose();
                    Transport.sendObjects(new Object[]{b, resA[1]}, 1, 0);
                    MatrixS[] resC = Cholesky.cholesky(ABlocks[3].subtract(b.multiply(bT, ring), ring));
                    MatrixS recv = (MatrixS) Transport.recvObject(1, 1);
                    MatrixS z = resC[1].multiply(recv, ring).negate(ring);
                    L = fillAndJoinMatrix(resA[0], b, resC[0], ring);
                    LInv = fillAndJoinMatrix(resA[1], z, resC[1], ring);
                    result = new MatrixS[]{L, LInv};
                } else {
                    Object[] recv = Transport.recvObjects(2, 0, 0);
                    MatrixS res = ((MatrixS) recv[0]).multiply((MatrixS) recv[1], ring);
                    Transport.sendObject(res, 0, 1);
                }
            }
            break;
            case 4: {
                int[] group1 = new int[]{0, 1};
                int[] group2 = new int[]{2, 3};
                int[] group3 = new int[]{0, 1, 2, 3};
                Intracomm COMM_1 = intracomm.create(intracomm.getGroup().incl(group1));
                Intracomm COMM_2 = intracomm.create(intracomm.getGroup().incl(group2));

                if (A.size < 4) {
                    if (intracomm.getRank() < 2) return choleskyDecomposition(A, COMM_1);
                    else break;
                }

                MatrixS[] ABlocks = A.split();
                MatrixS[] resA, resC = new MatrixS[2];
                MatrixS bT, b, z = new MatrixS();

                if (intracomm.getRank() == 0) {
                    resA = choleskyDecomposition(ABlocks[0], COMM_1);
                    for (int i = 1; i < intracomm.getSize(); i++) Transport.sendObject(resA, i, i);
                } else {
                    if (intracomm.getRank() == 1) choleskyDecomposition(ABlocks[0], COMM_1);
                    resA = (MatrixS[]) Transport.recvObject(0, intracomm.getRank());
                }

                if (intracomm.getRank() == 0) {
                    bT = MatrixMul4.multiplyMatrix4(resA[1], ABlocks[1], group3, intracomm);
                    System.out.println("***bt (res of 4x4 multiplication is): " + bT + " on processor " + intracomm.getRank() + "***" + "\n");
                    for (int i = 1; i < intracomm.getSize(); i++) Transport.sendObject(bT, i, i);
                } else {
                    MatrixMul4.multiplyMatrix4(resA[1], ABlocks[1], group3, intracomm);
                    bT = (MatrixS) Transport.recvObject(0, intracomm.getRank());
                    System.out.println("***bt (res of 4x4 multiplication is): " + bT + " on processor " + intracomm.getRank() + "***" + "\n");
                }

                b = bT.transpose();

                if (intracomm.getRank() == 0) {
                    Transport.sendObjects(new Object[]{b, resA[1]}, 2, 2);
                    Transport.sendObjects(new Object[]{b, resA[1]}, 3, 3);
                    resC = choleskyDecomposition(ABlocks[3].subtract(b.multiply(bT, ring), ring), COMM_1);
                    MatrixS recv = (MatrixS) Transport.recvObject(2, 2);
                    z = resC[1].multiply(recv, ring).negate(ring);
                }
                if (intracomm.getRank() == 1) {
                    choleskyDecomposition(ABlocks[3].subtract(b.multiply(bT, ring), ring), COMM_1);
                }

                if (intracomm.getRank() == 0) {
                    L = fillAndJoinMatrix(resA[0], b, resC[0], ring);
                    LInv = fillAndJoinMatrix(resA[1], z, resC[1], ring);
                    result = new MatrixS[]{L, LInv};
                }

                if (intracomm.getRank() == 2 || intracomm.getRank() == 3) {
                    MatrixS[] recv = new MatrixS[2];
                    Object[] recvObject = Transport.recvObjects(2, 0, intracomm.getRank());
                    for (int i = 0; i < recv.length; i++) {
                        recv[i] = (MatrixS) recvObject[i];
                    }
                    MatrixS res = MatrixMul2.multiply(recv[0], recv[1], group2, COMM_2);
//                    if (res != null){
//                        System.out.println("***Matrices mult res (of 2x2 multiplication) is: " + res + " on processor " + intracomm.getRank() + "***" + "\n");
//                    }
                    if (intracomm.getRank() == 2) Transport.sendObject(res, 0, 2);
                }
            }
            break;
            case 8: {
                int[] group1 = new int[]{0, 1, 2, 3};
                int[] group2 = new int[]{4, 5, 6, 7};
                Intracomm COMM_1 = intracomm.create(intracomm.getGroup().incl(group1));
                Intracomm COMM_2 = intracomm.create(intracomm.getGroup().incl(group2));

                if (A.size < 8) {
                    if (intracomm.getRank() < 4) return choleskyDecomposition(A, COMM_1);
                    else break;
                }

                MatrixS[] ABlocks = A.split(), resA, resC = new MatrixS[2];
                MatrixS bT, b, z = new MatrixS();

                if (intracomm.getRank() == 0) {
                    resA = choleskyDecomposition(ABlocks[0], COMM_1);
                    for (int i = 1; i < MPI.COMM_WORLD.getSize(); i++) {
                        Transport.sendObject(resA, i, i);
                    }
                } else {
                    if (intracomm.getRank() < group2[0])
                        choleskyDecomposition(ABlocks[0], COMM_1);
                    resA = (MatrixS[]) Transport.recvObject(0, intracomm.getRank());
                }

                if (intracomm.getRank() == 0) {
                    bT = MatrixMul8.multiplyMatrix8(resA[1], ABlocks[1], intracomm);
                    for (int i = 1; i < MPI.COMM_WORLD.getSize(); i++) {
                        Transport.sendObject(bT, i, i);
                    System.out.println("***bt (res of 8x8 multiplication is): " + bT + " on processor " + intracomm.getRank() + "***" + "\n");
                    }
                } else {
                    MatrixMul8.multiplyMatrix8(resA[1], ABlocks[1], intracomm);
                    bT = (MatrixS) Transport.recvObject(0, intracomm.getRank());
                    System.out.println("***bt (res of 8x8 multiplication is): " + bT + " on processor " + intracomm.getRank() + "***" + "\n");
                }

                b = bT.transpose();

                if (intracomm.getRank() == 0) {
                    for (int i = 4; i < MPI.COMM_WORLD.getSize(); i++) {
                        Transport.sendObjects(new Object[]{b, resA[1]}, i, i);
                    }
                    resC = choleskyDecomposition(ABlocks[3].subtract(b.multiply(bT, ring), ring), COMM_1);
                    MatrixS recv = (MatrixS) Transport.recvObject(4, 4);
                    z = resC[1].multiply(recv, ring).negate(ring);
                } else {
                    if (intracomm.getRank() < group2[0])
                        choleskyDecomposition(ABlocks[3].subtract(b.multiply(bT, ring), ring), COMM_1);
                }

                if (intracomm.getRank() == 0) {
                    L = fillAndJoinMatrix(resA[0], b, resC[0], ring);
                    LInv = fillAndJoinMatrix(resA[1], z, resC[1], ring);
                    result = new MatrixS[]{L, LInv};
                }

                if (intracomm.getRank() >= group2[0]) {
                    MatrixS[] recv = new MatrixS[2];
                    Object[] recvObject = Transport.recvObjects(2, 0, intracomm.getRank());
                    for (int i = 0; i < recv.length; i++) {
                        recv[i] = (MatrixS) recvObject[i];
                    }
                    MatrixS res = MatrixMul4.multiplyMatrix4(recv[0], recv[1], group2, COMM_2);
                    System.out.println("***Multiplied matrices res (4x4): " + res + " on processor " + intracomm.getRank() + "***" + "\n");
                    if (intracomm.getRank() == 4) Transport.sendObject(res, 0, 4);
                }
            }
            break;
            default:
                throw new MPIException("Incorrect processors number.");
        }
        return result;
    }
    
   // метод, що генерує позитивно-визначену симетричну матрицю with given ring
    public static MatrixS generate(int n, Ring ring) {
        Random rnd = new Random();
        Element[][] elements = new Element[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j <= i)
                    elements[i][j] = new NumberR64(rnd.nextDouble() * 50);
                else
                    elements[i][j] = new NumberR64(0);
            }
        }
        MatrixS m = new MatrixS(elements, ring);
        MatrixS mT = m.transpose();
        return m.multiply(mT, ring);
    }
    
    public static MatrixS fillAndJoinMatrix(MatrixS a, MatrixS b, MatrixS c, Ring ring) {
        return MatrixS.join(new MatrixS[]{a, MatrixS.zeroMatrix(a.size), b, c});
    }
}