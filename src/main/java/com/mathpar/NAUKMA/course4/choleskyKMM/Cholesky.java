package com.mathpar.NAUKMA.course4.choleskyKMM;

import javax.swing.plaf.synth.SynthDesktopIconUI;
import java.lang.Math;
import java.util.Arrays;

// mpirun -hostfile /home/mariia/hostfile -np 1 java -cp /home/mariia/mpi-dap/target/classes com/mathpar/NAUKMA/course4/choleskyKMM/Cholesky

public class Cholesky {
    public static class CholMatrix {
        private double[][] L;
        private double[][] L1;
        public CholMatrix(double[][] a, double[][] b) {
            this.L = a;
            this.L1 = b;
        }
        public double[][] getL() {return L; }
        public double[][] getL1() {return L1; }
    }

    public static void printArr(double[][] arr) {
        int h = arr.length;
        int w = arr[0].length;
        for (int i = 0; i < h; i++) {
            System.out.println();
            for (int j = 0; j < w; j++) {
                System.out.print(arr[i][j]+" ");
            }
        }
        System.out.println();
    }
    public static double multiplyMatricesCell(double[][] firstMatrix, double[][] secondMatrix, int row, int col) {
        double cell = 0;
        for (int i = 0; i < secondMatrix.length; i++) {
            cell += firstMatrix[row][i] * secondMatrix[i][col];
        }
        return cell;
    }

    public static double[][] multiplyM(double[][] firstMatrix, double[][] secondMatrix) {
        double[][] result = new double[firstMatrix.length][secondMatrix[0].length];
        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                result[row][col] = multiplyMatricesCell(firstMatrix, secondMatrix, row, col);
            }
        }
    return result;
    }

    public static double[][] multiplyMByNumber(double[][] arr, double x) {
        double[][] result = new double[arr.length][arr[0].length];
        for (int row = 0; row < result.length; row++) {
            for (int col = 0; col < result[row].length; col++) {
                result[row][col] = arr[row][col]*x;
            }
        }
        return result;
    }

    public static double[][] substractM(double[][] a, double[][] b) {
        double[][] c = new double[a.length][a[0].length];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[0].length; j++) {
                c[i][j] = a[i][j] - b[i][j];
            }
        }
        return c;
    }

    public static double[][] transposeArr(double[][] arr) {
        int h = arr.length;
        int w = arr[0].length;
        int w_ = h;
        int h_ = w;
        double[][] transposedArr = new double[h_][w_];
        for (int i = 0; i < h_; i++) {
            for (int j = 0; j < w_; j++) {
                System.out.println(i + " " + j);
                transposedArr[i][j] = arr[j][i];
            }
        }
        printArr(transposedArr);
        return transposedArr;
    }
    static private final int N = 2;
    double[][] L = new double[N][N];

    public static double[][] choleskyForTwo(double[][] Arr) {
        int N  = Arr.length;
        double a, bT, a1, b, c, c1, z;
        if (N == 1) {
            double[][] tmp = {{Math.sqrt(Arr[0][0])}};
            return tmp;
        } else {
            // split A into 3 parts (A, B, D),  B^T not interested
            double[][] A = new double[N/2][N/2];
            double[][] B = new double[N/2][N/2];
            double[][] D = new double[N/2][N/2];
            for (int i = 0; i < N/2; i++) {
                for (int j = 0; j < N/2; j++) {
                    A[i][j] = Arr[i][j];
                }
            }
            System.out.println("Array A = ");
            printArr(A);
            for (int i = 0; i < N/2; i++) {
                for (int j = N/2; j < N; j++) {
                    B[i][j-N/2] = Arr[i][j];
                }
            }
            System.out.println("Array B = ");
            printArr(B);
            for (int i = N/2; i < N; i++) {
                for (int j = N/2; j < N; j++) {
                    D[i-N/2][j-N/2] = Arr[i][j];
                }
            }
            System.out.println("Array D = ");
            printArr(D);

            a = choleskyForTwo(A)[0][0];
            a1 = 1.0/a;
            bT = a1*B[0][0];
            b = bT;
            double[][] del = {{D[0][0] - b*bT}};
            c = choleskyForTwo(del)[0][0];
            c1 = 1.0/c;
            z = -c1*b*a1;
        }
        double[][] L = {{a, 0}, {b, c}};
        return L;
    }

    static double[][] appendArraysHorizontal(double[][] array1, double[][] array2){

        double[][] result = new double[Math.max(array1.length,array2.length)][];

        //append the rows, where both arrays have information
        int i;
        for (i = 0; i < array1.length && i < array2.length; i++) {
            result[i] = new double[array1[i].length+array2[i].length];
            System.arraycopy(array1[i], 0, result[i], 0, array1[i].length);
            System.arraycopy(array2[i], 0, result[i], array1[i].length, array2[i].length);
        }

        //Fill out the rest
        //only one of the following loops will actually run.
        for (; i < array1.length; i++) {
            result[i] = new double[array1[i].length];
            System.arraycopy(array1[i], 0, result[i], 0, array1[i].length);
        }

        for (; i < array2.length; i++) {
            result[i] = new double[array2[i].length];
            System.arraycopy(array2[i], 0, result[i], 0, array2[i].length);
        }

        return result;
    }

    static double[][] appendArraysVertical(double[][] array1, double[][] array2) {
        double[][] ret = new double[array1.length + array2.length][];
        int i = 0;
        for (;i<array1.length;i++) {
            ret[i] = array1[i];
        }
        for (int j = 0;j<array2.length;j++) {
            ret[i++] = array2[j];
        }
        return ret;
    }

    public static CholMatrix cholesky(double[][] Arr) {
        int N  = Arr.length;
        double[][] a, bT, a1, b, c, c1, z0, z1, z, tmp, del;
        if (N == 1) {
            double[][] l = new double[][]{{Math.sqrt(Arr[0][0])}};
            double[][] l1 = new double[][]{{1.0/Math.sqrt(Arr[0][0])}};
            CholMatrix chm = new CholMatrix(l, l1);
            return chm;
        } else {
            // split A into 3 parts (A, B, D),  B^T not interested
            double[][] A = new double[N/2][N/2];
            double[][] B = new double[N/2][N/2];
            double[][] D = new double[N/2][N/2];
            for (int i = 0; i < N/2; i++) {
                for (int j = 0; j < N/2; j++) {
                    A[i][j] = Arr[i][j];
                }
            }
            System.out.println("Array A = ");
            printArr(A);
            for (int i = 0; i < N/2; i++) {
                for (int j = N/2; j < N; j++) {
                    B[i][j-N/2] = Arr[i][j];
                }
            }
            System.out.println("Array B = ");
            printArr(B);
            for (int i = N/2; i < N; i++) {
                for (int j = N/2; j < N; j++) {
                    D[i-N/2][j-N/2] = Arr[i][j];
                }
            }
            System.out.println("Array D = ");
            printArr(D);

            CholMatrix cholMatrix1 = cholesky(A);
            a = cholMatrix1.getL();
            a1 = cholMatrix1.getL1();
            bT = multiplyM(a1, B);
            b = transposeArr(bT);
            tmp = multiplyM(b, bT);
            del = substractM(D, tmp);
            CholMatrix cholMatrix2 = cholesky(del);
            c = cholMatrix2.getL();
            c1 = cholMatrix2.getL1();
            z0 = multiplyMByNumber(c1, -1);
            z1 = multiplyM(z0, b);
            z = multiplyM(z1, a1);
        }

        double[][] zeroes = new double[N/2][N/2];
        for (int i = 0; i < zeroes.length; i++) {
            for (int j = 0; j < zeroes.length; j++) {
                zeroes[i][j] = 0;
            }
        }

        double[][] up1 = appendArraysHorizontal(a, zeroes);
        double[][] down1 = appendArraysHorizontal(b, c);
        double[][] L = appendArraysVertical(up1, down1);
        double[][] up2 = appendArraysHorizontal(a1, zeroes);
        double[][] down2 = appendArraysHorizontal(z, c1);
        double[][] L1 = appendArraysVertical(up2, down2);
        CholMatrix result = new CholMatrix(L, L1);
        return result;
    }
    public static void main(String[] args) throws Exception {
       // double[][] test = {{5, 11}, {11, 25}};

        double[][] test = new double[4][4];
        test[0][0] = 1;
        test[0][1] = 2;
        test[0][2] = 4;
        test[0][3] = 7;
        test[1][0] = 2;
        test[1][1] = 13;
        test[1][2] = 23;
        test[1][3] = 38;
        test[2][0] = 4;
        test[2][1] = 23;
        test[2][2] = 77;
        test[2][3] = 122;
        test[3][0] = 7;
        test[3][1] = 38;
        test[3][2] = 122;
        test[3][3] = 294;
     /*  double[][] test = new double[3][4];
        test[0][0] = 1;
        test[0][1] = 2;
        test[0][2] = 3;
        test[0][3] = 4;
        test[1][0] = 5;
        test[1][1] = 6;
        test[1][2] = 7;
        test[1][3] = 8;
        test[2][0] = 9;
        test[2][1] = 10;
        test[2][2] = 11;
        test[2][3] = 12;*/
        CholMatrix testCh = cholesky(test);
        double[][] L = testCh.getL();
        printArr(L);
    }
}
