package com.mathpar.NAUKMA.course4.choleskyKMM.eight;

import com.mathpar.parallel.dap.core.Transport;
import com.mathpar.NAUKMA.course4.choleskyKMM.CholeskyM;
import com.mathpar.NAUKMA.course4.choleskyKMM.two.SquareMatrixMul2;
import com.mathpar.number.Element;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.NumberR64;
import com.mathpar.number.Ring;
import mpi.MPI;
import mpi.MPIException;
import mpi.*;

import java.util.Random;

/**
 * @author mykola (Mykola Yeshchenko)
 */
//mpirun -hostfile /home/mykola/hostfile -np 8 java -cp /home/mykola/mpi-dap/target/classes com/mathpar/NAUKMA/course4/choleskyKMM/eight/Cholesky 4
public class Cholesky {

    private static final int RING_MOD = 13;
    private static final int DEFAULT_DENSITY = 10_000;
    private static final int RESULT1 = 5001;
    private static Ring ring;

    public static void main(String[] args) throws Exception {
        initMPI();
        ring = getRingMod32();
        int processorNumber = getProcessorNumber();
        int matrixSize = Integer.parseInt(args[0]);

        if (processorNumber == 0) {
            //generate pos def matrix with a given size
            MatrixS matrixA = CholeskyM.generate(matrixSize);
//            Element[][] elements = {{new NumberR64(1),new NumberR64(2),new NumberR64(4),new NumberR64(7)}, {new NumberR64(2),new NumberR64(13),new NumberR64(23),new NumberR64(38)}, {new NumberR64(4),new NumberR64(23),new NumberR64(77),new NumberR64(122)}, {new NumberR64(7),new NumberR64(38),new NumberR64(122),new NumberR64(294)}};
//            MatrixS matrixA = new MatrixS(elements, ring);
//            Element[][] elements = {{new NumberR64(1), new NumberR64(2)}, {new NumberR64(2), new NumberR64(5)}};
//            MatrixS matrixA = new MatrixS(elements, ring);
            System.out.println("A = " + matrixA);

            //decompose matrix into 4 separate blocks and send them do different processors
            MatrixS[] AA = matrixA.split();

            System.out.println("Splitted matrix A:");
            for (MatrixS m : AA) {
                System.out.println(m);
            }

            //1. Find L and L^-1 (via Chol sequential) for AA[0] (or 'X') ////// Get [X, X^-1] = chol(A)
            CholeskyM.CholMatrix XX = CholeskyM.cholesky(AA[0]);
            MatrixS[] matricesXX = new MatrixS[]{XX.getL(), XX.getL1()};
            System.out.println("X: " + matricesXX[0].toString() + System.lineSeparator() + "X^-1: " + matricesXX[1].toString());

            //send matrices for multiplying to the 1 and 2 processors
           // Transport.sendObject(AA[1], 1, 10);
           // Transport.sendObject(matricesXX[1], 1, 11);
           // Transport.sendObject(AA[1], 2, 20);
           // Transport.sendObject(matricesXX[1], 2, 21);

            //2. Get Z^T = multiply AA[1] by X^-1 (or matricesXX[1])
            MatrixS ZT = (MatrixS) Transport.recvObject(1, Transport.Tag.RESULT);
            System.out.println("Multiplication result (ZT) is: " + ZT.toString());

            MatrixS Z = ZT.transpose();
            System.out.println("Z = " + Z);
            MatrixS minusZ = Z.negate(ring);
            System.out.println("-Z = " + minusZ);

            //3. Calculate F = -Z * Z^T + D
            //send matrices for multiplying to the 1 and 2 processors
          // Transport.sendObject(minusZ, 1, 30);
          //  Transport.sendObject(ZT, 1, 31);
           // Transport.sendObject(minusZ, 2, 40);
           // Transport.sendObject(ZT, 2, 41);
            MatrixS multResult = (MatrixS) Transport.recvObject(1,  Transport.Tag.RESULT);
            System.out.println("Multiplication result (-Z * Z^T) is: " + multResult.toString());
            MatrixS F = multResult.add(AA[3], ring);
            System.out.println("F is: " + F.toString());

            //4.1 Get [Y,Y^-1] = chol(F)
            CholeskyM.CholMatrix YY = CholeskyM.cholesky(F);
            MatrixS[] matricesYY =
                    new MatrixS[]{YY.getL(), YY.getL1()};
            System.out.println("Y: " + matricesYY[0].toString() + System.lineSeparator() + "Y^-1: " + matricesYY[1].toString());

            //4.2 TMP_MATRIX = Z * X^-1
            //send matrices for multiplying to the 1 and 2 processors
          //  Transport.sendObject(Z, 1, 50);
          //  Transport.sendObject(matricesXX[1], 1, 51);
          //  Transport.sendObject(Z, 2, 60);
          //  Transport.sendObject(matricesXX[1], 2, 61);

            MatrixS tmpM = (MatrixS) Transport.recvObject(1,  Transport.Tag.RESULT);
            System.out.println("tmpM is: " + tmpM.toString());

            //5. L,L^-1 = (X,0,Z,Y) , LT.
            MatrixS zeroes = MatrixS.zeroMatrix(matricesXX[0].size);
            MatrixS[] toBeJoined = new MatrixS[]{matricesXX[0], zeroes, Z, matricesYY[0]};
            MatrixS L = MatrixS.join(toBeJoined);

            System.out.println("L is: " + L.toString());

            MatrixS LT = L.transpose();
            System.out.println("LT is: " + LT.toString());

            //check if calculations are correct
            System.out.println("Conducting verification (L*LT=A)");

            //send matrices for multiplying to the 1 and 2 processors
        //    Transport.sendObject(L, 1, 90);
        //    Transport.sendObject(LT, 1, 91);
        //    Transport.sendObject(L, 2, 100);
       //     Transport.sendObject(LT, 2, 101);

            MatrixS ACheckLLT = (MatrixS) Transport.recvObject(1,  Transport.Tag.RESULT);
            System.out.println("ACheckLLT is: " + ACheckLLT.toString());

        } else if (getProcessorNumber() == 1) {
            //1.
            MatrixS AA = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            MatrixS XX = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            System.out.println("xyz" + AA.toString() + XX.toString());
            // SquareMatrixMul2.multiply(XX, AA, 1, 2, 0);

            //2.
            MatrixS minusZ = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            MatrixS ZT = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            //System.out.println("xyz" + minusZ.toString() + ZT.toString());
            // SquareMatrixMul2.multiply(minusZ, ZT, 1, 2, 0);

            //4.2
            MatrixS Z = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            MatrixS XMinusOne = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            //System.out.println("xyz" + Z.toString() + XMinusOne.toString());
            // SquareMatrixMul2.multiply(Z, XMinusOne, 1, 2, 0);

            //verification
            MatrixS L = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            MatrixS LT = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            //System.out.println("xyz" + L.toString() + LT.toString());
            // SquareMatrixMul2.multiply(L, LT, 1, 2, 0);
        } else if (getProcessorNumber() == 2) {
            //1.
            MatrixS AA = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            MatrixS XX = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            System.out.println("xyz" + AA.toString() + XX.toString());
            // SquareMatrixMul2.multiply(XX, AA, 1, 2, 0);

            //2.
            MatrixS minusZ = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            MatrixS ZT = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            //System.out.println("xyz" + minusZ.toString() + ZT.toString());
            // SquareMatrixMul2.multiply(minusZ, ZT, 1, 2, 0);

            //4.2
            MatrixS Z = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            MatrixS XMinusOne = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            //System.out.println("xyz" + Z.toString() + XMinusOne.toString());
            // SquareMatrixMul2.multiply(Z, XMinusOne, 1, 2, 0);

            //verification
            MatrixS L = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            MatrixS LT = (MatrixS) Transport.recvObject(0,  Transport.Tag.RESULT);
            //System.out.println("xyz" + L.toString() + LT.toString());
            // SquareMatrixMul2.multiply(L, LT, 1, 2, 0);
        }


        MPI.Finalize();
    }
    
//    private static MatrixS[] cholRecursive(MatrixS A, MPI.Group processors) throws MPIException{
//        MPI.COMM_WORLD.getGroup();
//        mpi.Group g = MPI.COMM_WORLD.getGroup().incl(new int[]{0,1,2});
//        //MPI.COMM_WORLD.create(new mpi.Comm.);
//        //Group z = new mpi.Group(5);
//        //mpi.MPI.COMM_SELF.create(new Group(3));
//        //COMM_WORLD;
//        Intracomm x = MPI.COMM_WORLD.split(1, 1);
//        System.out.println(x.toString());
//        //MPI.COMM_WORLD.create(group);
//    }

    private static void initMPI() throws MPIException {
        MPI.Init(new String[0]);
    }

    private static Ring getRingMod32() {
        Ring result = new Ring("R64[x]");
        result.setMOD32(RING_MOD);
        return result;
    }

    private static int getProcessorNumber() throws MPIException {
        return MPI.COMM_WORLD.getRank();
    }

    private static MatrixS buildRandomSquareMatrix(int size) {
        int maxPowerFirst = 5;
        int maxPowerLast = 5;
        return new MatrixS(
                size,
                size,
                DEFAULT_DENSITY,
                new int[]{maxPowerFirst, maxPowerLast},
                new Random(),
                NumberR64.ONE,
                ring
        );
    }
}