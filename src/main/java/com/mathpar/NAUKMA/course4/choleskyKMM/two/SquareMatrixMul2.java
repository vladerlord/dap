package com.mathpar.NAUKMA.course4.choleskyKMM.two;

import com.mathpar.NAUKMA.course4.choleskyKMM.eight.Transport;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.NumberR64;
import com.mathpar.number.NumberZp32;
import com.mathpar.number.Ring;
import mpi.Intracomm;
import mpi.MPI;
import mpi.MPIException;

import java.util.Objects;
import java.util.Random;

/**
 * @author bellkross (Kyrylo Vasylenko)
 */
// mpirun --hostfile /home/kyrylo/hostfile -np 2 java -cp /home/kyrylo/Desktop/mpi-dap/target/classes com/mathpar/NAUKMA/course4/choleskyKMM/two/SquareMatrixMul2 4
public class SquareMatrixMul2 {

    private static final int RING_MOD = 13;
    private static final int DEFAULT_DENSITY = 10_000;
    private static final int MATRIX_A_TAG = 10, MATRIX_B_TAG = 20;

    public static void main(String[] args) throws Exception {
        initMPI();
        int matrixSize = Integer.parseInt(args[0]);
        mpi.Group group = MPI.COMM_WORLD.getGroup().incl(new int[]{0,1});
        Intracomm intracomm = MPI.COMM_WORLD.create(group);

        MatrixS matrixA = buildRandomSquareMatrix(matrixSize);
        MatrixS matrixB = buildRandomSquareMatrix(matrixSize);

        MatrixS result = multiply(matrixA, matrixB, intracomm);
        if (intracomm.getRank() == 0) {
            System.out.println("Result: " + result + System.lineSeparator() +
                    "Check: " + add(negate(result), mul(matrixA, matrixB)));
        }

        MPI.Finalize();
    }

    public static MatrixS multiply(MatrixS matrixA, MatrixS matrixB, Intracomm intracomm) throws Exception {
        MatrixS result = null;
        int matrixSize = matrixA != null ? matrixA.size : 2;
        if (intracomm.getRank() == 0) {

            if (matrixSize == 1) {
                result = mul(matrixA, matrixB);
            } else {
                MatrixS[] splitMatrixA = Objects.requireNonNull(matrixA).split();
                MatrixS firstPartOfA = MatrixS.join(
                        new MatrixS[]{
                                splitMatrixA[0],
                                splitMatrixA[1],
                                generateSquaredZeroBlock(matrixA.size / 2),
                                generateSquaredZeroBlock(matrixA.size / 2)
                        }
                );

                MatrixS secondPartOfA = MatrixS.join(
                        new MatrixS[]{
                                splitMatrixA[2],
                                splitMatrixA[3],
                                generateSquaredZeroBlock(matrixA.size / 2),
                                generateSquaredZeroBlock(matrixA.size / 2)
                        }
                );

                Transport.sendObject(secondPartOfA, 1, MATRIX_A_TAG, intracomm);
                Transport.sendObject(matrixB, 1, MATRIX_B_TAG, intracomm);

                MatrixS resultingMatrix1 = mul(firstPartOfA, matrixB);
                MatrixS resultingMatrix2 = (MatrixS) Transport.recvObject(1, MATRIX_B_TAG, intracomm);

                MatrixS[] resultingBlocks1 = resultingMatrix1.split();
                MatrixS[] resultingBlocks2 = resultingMatrix2.split();

                result = MatrixS.join(
                        new MatrixS[]{
                                resultingBlocks1[0],
                                resultingBlocks1[1],
                                resultingBlocks2[0],
                                resultingBlocks2[1]
                        }
                );
            }
        } else if (matrixSize > 1) {
            MatrixS secondPartOfA = (MatrixS) Transport.recvObject(0, MATRIX_A_TAG, intracomm);
            MatrixS matrB = (MatrixS) Transport.recvObject(0, MATRIX_B_TAG, intracomm);

            MatrixS res = mul(secondPartOfA, matrB);

            Transport.sendObject(res, 0, MATRIX_B_TAG, intracomm);
        }

        return result;
    }

    private static void initMPI() throws MPIException {
        MPI.Init(new String[0]);
    }

    private static Ring getRingMod32() {
        Ring result = new Ring("R64[x]");
        result.setMOD32(RING_MOD);
        return result;
    }

    private static MatrixS buildRandomSquareMatrix(int size) {
        int maxPowerFirst = 5;
        int maxPowerLast = 5;
        return new MatrixS(
                size,
                size,
                DEFAULT_DENSITY,
                new int[]{maxPowerFirst, maxPowerLast},
                new Random(),
                NumberZp32.ONE,
                getRingMod32()
        );
    }

    private static MatrixS mul(MatrixS left, MatrixS right) {
        return left.multiply(right, getRingMod32());
    }

    private static MatrixS add(MatrixS left, MatrixS right) {
        return left.add(right, getRingMod32());
    }

    private static MatrixS generateSquaredZeroBlock(int n) {
        Element[][] block = new Element[n][n];
        for (int row = 0; row < n; row++) {
            for (int col = 0; col < n; col++) {
                block[row][col] = new NumberR64(0);
            }
        }
        return new MatrixS(block, getRingMod32());
    }

    private static MatrixS negate(MatrixS matrixS) {
        return matrixS.negate(getRingMod32());
    }
}