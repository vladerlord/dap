package com.mathpar.NAUKMA.course4.choleskyKMM.cholesky;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Ring;
import mpi.Intracomm;
import mpi.MPIException;
import java.io.IOException;

public class MatrixMul4 {
    static int tag = 0;
    static int mod = 13;

    private static Ring ring = new Ring("R64[x]");

    public static MatrixS multiply(MatrixS a, MatrixS b, MatrixS c, MatrixS d, Ring ring) {
        return (a.multiply(b, ring)).add(c.multiply(d, ring), ring);
    }

    public static MatrixS multiplyMatrix4(MatrixS A, MatrixS B,int prNumbers[], Intracomm intracomm) throws MPIException, IOException, ClassNotFoundException {
        MatrixS result;
        Ring ring = new Ring("R64[x]");
        int rank = intracomm.getRank();
        ring.setMOD32(mod);

        if (rank == 0) {
            MatrixS[] DD = new MatrixS[4];
            MatrixS CC = null;
            MatrixS[] AA = A.split();
            MatrixS[] BB = B.split();
            Transport.sendArrayOfObjects(new Object[]{AA[0], BB[1],
                    AA[1], BB[3]}, prNumbers[1], 1);
            Transport.sendArrayOfObjects(new Object[]{AA[2], BB[0],
                    AA[3], BB[2]}, prNumbers[2], 2);
            Transport.sendArrayOfObjects(new Object[]{AA[2], BB[1],
                    AA[3], BB[3]}, prNumbers[3], 3);
            DD[0] = (AA[0].multiply(BB[0], ring)).
                    add(AA[1].multiply(BB[2], ring), ring);
            DD[1] = (MatrixS) Transport.recvObject(prNumbers[1], 1);
            DD[2] = (MatrixS) Transport.recvObject(prNumbers[2], 2);
            DD[3] = (MatrixS) Transport.recvObject(prNumbers[3], 3);
            CC = MatrixS.join(DD);
            result = CC;
        } else {
            Object[] n = Transport.recvArrayOfObjects(prNumbers[0], rank);
            MatrixS a = (MatrixS) n[0];
            MatrixS b = (MatrixS) n[1];
            MatrixS c = (MatrixS) n[2];
            MatrixS d = (MatrixS) n[3];

            MatrixS res = multiply(a, b, c, d, ring);
            Transport.sendObject(res, prNumbers[0], rank);
            result = res;
        }
        return result;
    }
}
