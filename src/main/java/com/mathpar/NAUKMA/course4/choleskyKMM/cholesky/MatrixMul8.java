package com.mathpar.NAUKMA.course4.choleskyKMM.cholesky;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Ring;
import mpi.Intracomm;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

public class MatrixMul8 {
    public static  Ring ring = new Ring("R64[x]");

    public static MatrixS multiplyMatrix8(MatrixS A, MatrixS B, Intracomm intracomm)
            throws MPIException, IOException, ClassNotFoundException {
        int rank = intracomm.getRank();
        MatrixS result = null;
        if (rank == 0) {
            int ord = 4;
            int den = 10000;
            Random rnd = new Random();
            MatrixS D = null;
            MatrixS[] AA = A.split();
            MatrixS[] BB = B.split();
            int tag = 0;
            Transport.sendObjects(new Object[] {AA[1], BB[2]},
                    1, tag);
            Transport.sendObjects(new Object[] {AA[0], BB[1]},
                    2, tag);
            Transport.sendObjects(new Object[] {AA[1], BB[3]},
                    3, tag);
            Transport.sendObjects(new Object[] {AA[2], BB[0]},
                    4, tag);
            Transport.sendObjects(new Object[] {AA[3], BB[2]},
                    5, tag);
            Transport.sendObjects(new Object[] {AA[2], BB[1]},
                    6, tag);
            Transport.sendObjects(new Object[] {AA[3], BB[3]},
                    7, tag);

            MatrixS[] DD = new MatrixS[4];
            DD[0] = (AA[0].multiply(BB[0], ring)).add((MatrixS) Transport.recvObject(1, 3),
                    ring);
            DD[1] = (MatrixS) Transport.recvObject(2, 3);
            DD[2] = (MatrixS) Transport.recvObject(4, 3);
            DD[3] = (MatrixS) Transport.recvObject(6, 3);
            D = MatrixS.join(DD);
            result = D;
        } else {
            Object[] b = Transport.recvObjects(2, 0, 0);
            MatrixS[] a = new MatrixS[b.length];
//            System.out.println("***Received matrix: " + a + " on processor " + intracomm.getRank() + "***" + "\n");
            for (int i = 0; i < b.length; i++) {
                a[i] = (MatrixS) b[i];
            }
            MatrixS res = a[0].multiply(a[1], ring);
//            System.out.println("***Multiplied matrices res: " + res + " on processor " + intracomm.getRank() + "***" + "\n");
            if (rank % 2 == 0) {
                MatrixS p = res.add((MatrixS) Transport.
                        recvObject(rank + 1, 3), ring);
                Transport.sendObject(p, 0, 3);
            } else {
                Transport.sendObject(res, rank - 1, 3);
            }
        }
        return result;
    }
}