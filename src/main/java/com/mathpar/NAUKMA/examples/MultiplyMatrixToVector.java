package com.mathpar.NAUKMA.examples;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.*;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

public class MultiplyMatrixToVector {
    public static void main(String[] args) throws MPIException,
            IOException, ClassNotFoundException {

        Ring ring = new Ring("Z[x]");
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize();

        int ord = Integer.parseInt(args[0]);

        int k = ord / size;

        int n = ord - k * (size - 1);
        if (rank == 0) {
            int den = 10000;
            Random rnd = new Random();
            MatrixS A = new MatrixS(ord, ord, den, new int[]{5, 5},
                    rnd, NumberZp32.ONE, ring);
            System.out.println("Matrix A = " + A.toString());
            VectorS B = new VectorS(ord, den, new int[]{5, 5},
                    rnd, ring);
            System.out.println("Vector B = " + B.toString());

            Element[] res0 = new Element[n];
            for (int i = 0; i < n; i++) {
                res0[i] = new VectorS(A.M[i]).multiply(B, ring);
                res0[i] = B.multiply(A.toSquaredArray(ring)[i], ring);

                System.out.println("rank = " + rank + " row = "
                        + Array.toString(A.M[i]) + Array.toString(A.toSquaredArray(ring)[i]));
            }
            for (int j = 1; j < size; j++) {
                for (int z = 0; z < k; z++) {
                  Transport.sendObject(A.M[n + (j - 1) * k + j * z], j, 100 + j);
                    Transport.sendObject(A.toSquaredArray(ring)[n + (j - 1) * k], j, 100 + j);
                }

                Transport.sendObject(B.V, j, 100 + j);
            }

            Element[] result = new Element[ord];
            System.arraycopy(res0, 0, result, 0, n);

            for (int t = 1; t < size; t++) {
                Element[] resRank = (Element[])
                        Transport.recvObject(t, 100 + t);
                System.arraycopy(resRank, 0, result, n +
                        (t - 1) * k, resRank.length);

            }
            System.out.println("RESULT A * B = " +
                    new VectorS(result).toString(ring));
        } else {

            System.out.println("I’m processor " + rank);

            Element[][] A = new Element[k][ord];
            for (int i = 0; i < k; i++) {
                A[i] = (Element[])
                        Transport.recvObject(0, 100 + rank);
                System.out.println("rank = " + rank + " row = "
                        + Array.toString(A[i]));

            }

            Element[] B = (Element[])
                    Transport.recvObject(0, 100 + rank);
            System.out.println("rank = " + rank + " B = "
                    + Array.toString(B));

            Element[] result = new Element[k];
            for (int j = 0; j < A.length; j++) {
                System.out.println("Multiplying " + new VectorS(A[j]) + " to " + new VectorS(B));
                result[j] = new VectorS(A[j]).multiply(new VectorS(B), ring);
                System.out.println("Result = " + result[j]);

            }

            Transport.sendObject(result, 0, 100 + rank);
            System.out.println("send result");
        }
        MPI.Finalize();


    }
}

