package com.mathpar.NAUKMA.examples;

import mpi.MPI;
import mpi.MPIException;

import java.util.Arrays;

public class TestScatter {
    public static void main(String[] args) throws MPIException{
        // ініціалізація MPI
        MPI.Init(args);
        // визначення номера процесора
        int myrank = MPI.COMM_WORLD.getRank();
        // визначенння числа процесорів в групі
        int np = MPI.COMM_WORLD.getSize();
        int n = Integer.parseInt(args[0]);
        // оголошуємо масив об'єктів
        int[] a = new int[2];
        // заповнюємо цей масив на нульовому процесорі
        if (myrank == 0){
            for (int i = 0; i < 2; i++){
                a[i] = i ;
                System.out.println("myrank = " + myrank + ": a = " + Arrays.toString(a));
            }
            System.out.println("rank = " + myrank);
        }
        // оголошуємо масив, в який будуть записуватись
        // прийняті процесором елементи
        int[] q = new int[n];
        MPI.COMM_WORLD.barrier();
        MPI.COMM_WORLD.scatter(a, 1, MPI.INT, q, n, MPI.INT, 0);
        // роздруковуємо отримані масиви і номера процесорів
        System.out.println("myrank = " + myrank + ": q = " + Arrays.toString(q));

        // завершення паралельної частини
        MPI.Finalize();
    }
}
