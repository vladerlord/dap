package com.mathpar.NAUKMA.masters_2.Denysenko;

import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.core.DropTask;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class SparseShtrassenMultiplication extends DropTask {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(SparseShtrassenMultiplication.class);
    private static int leafSize = 4;

    private final static int[][] _arcs = {
            {       3, 0, 0,    4, 1, 0,    2, 2, 1,    5, 3, 1,
                    1, 4, 0,    1, 5, 1,    2, 6, 0,    3, 7, 1,
                    4, 8, 1,    5, 9, 0,    6, 10, 0,   6, 11, 1,
                    7, 12, 0,   7, 13, 1}, // input function
            {8, 0, 0}, // P1
            {8, 0, 1}, // P2
            {8, 0, 2}, // P3
            {8, 0, 3}, // P4
            {8, 0, 4}, // P5
            {8, 0, 5}, // P6
            {8, 0, 6}, // P7
            {} // output function
    };

    public SparseShtrassenMultiplication() {
        inData = new Element[2];
        outData = new Element[1];
        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 14;
        type = 10;
        resultForOutFunctionLength = 7;
        inputDataLength = 2;
        number = cnum++;
        arcs = _arcs;
    }

    @Override
    public ArrayList<DropTask> doAmin() {
        ArrayList<DropTask> amin = new ArrayList<>();
        amin.add(new SparseShtrassenMultiplication()); // P1
        amin.add(new SparseShtrassenMultiplication()); // P2
        amin.add(new SparseShtrassenMultiplication()); // P3
        amin.add(new SparseShtrassenMultiplication()); // P4
        amin.add(new SparseShtrassenMultiplication()); // P5
        amin.add(new SparseShtrassenMultiplication()); // P6
        amin.add(new SparseShtrassenMultiplication()); // P7
        return amin;
    }

    @Override
    public void sequentialCalc(Ring r) {
        Element a = inData[0];
        Element b = inData[1];
        outData[0] = a.multiply(b, r);
    }

    @Override
    public MatrixS[] inputFunction(Element[] input, int inputKey, Ring r) {
        MatrixS a = (MatrixS) input[0];
        MatrixS b = (MatrixS) input[1];

        MatrixS[] aSplit = a.split();
        MatrixS A11 = aSplit[0];
        MatrixS A12 = aSplit[1];
        MatrixS A21 = aSplit[2];
        MatrixS A22 = aSplit[3];

        MatrixS[] bSplit = b.split();
        MatrixS B11 = bSplit[0];
        MatrixS B12 = bSplit[1];
        MatrixS B21 = bSplit[2];
        MatrixS B22 = bSplit[3];

        MatrixS[] inputMatr = new MatrixS[14];
        inputMatr[0] = A11;
        inputMatr[1] = A22;
        inputMatr[2] = B11;
        inputMatr[3] = B22;
        inputMatr[4] = A11.add(A22, r); // S1
        inputMatr[5] = B11.add(B22, r); // S2
        inputMatr[6] = A21.add(A22, r); // S3
        inputMatr[7] = B12.subtract(B22, r); // S4
        inputMatr[8] = B21.subtract(B11, r); // S5
        inputMatr[9] = A11.add(A12, r); // S6
        inputMatr[10] = A21.subtract(A11, r); // S7
        inputMatr[11] = B11.add(B12, r); // S8
        inputMatr[12] = A12.subtract(A22, r); // S9
        inputMatr[13] = B21.add(B22, r); // S10

        return inputMatr;
    }

    @Override
    public MatrixS[] outputFunction(Element[] input, Ring r) {
        MatrixS P1 = (MatrixS)input[0];
        MatrixS P2 = (MatrixS)input[1];
        MatrixS P3 = (MatrixS)input[2];
        MatrixS P4 = (MatrixS)input[3];
        MatrixS P5 = (MatrixS)input[4];
        MatrixS P6 = (MatrixS)input[5];
        MatrixS P7 = (MatrixS)input[6];

        MatrixS[] C_matrix = new MatrixS[]{
                P1.add(P4, r).subtract(P5, r).add(P7, r), // C11
                P3.add(P5, r), // C12
                P2.add(P4, r), // C21
                P1.subtract(P2, r).add(P3, r).add(P6, r) // C22
        };

        return new MatrixS[]{ MatrixS.join(C_matrix) };
    }

    @Override
    public boolean isItLeaf() {
        return ((MatrixS) inData[0]).M.length <= leafSize;
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();
        final int n = 16;
        final int maxRandomValue = 100;
        final int dropType = 10;
        Ring ring = new Ring("R[]");
        MatrixS a = new MatrixS(getRandomMatrix(n, 1, maxRandomValue), ring);
        MatrixS b = new MatrixS(getRandomMatrix(n, 3, maxRandomValue), ring);
        DispThread disp = new DispThread(1, args, ring);

        if (rank == 0) {
            System.out.println("A=");
            printMatr(a);
            System.out.println("B=");
            printMatr(b);
        }

        Element[] input = new MatrixS[]{ a, b };
        disp.execute(dropType, args, input, ring);

        if (rank == 0) {
            MatrixS result = (MatrixS) disp.getResult()[0];
            System.out.println("Result=");
            printMatr(result);
        }

        disp.counter.DoneThread();
        disp.counter.thread.join();

        MPI.Finalize();
    }

    private static int[][] getRandomMatrix(int n, int randomSeed, int maxValue) {
        int[][] matr = new int[n][n];
        Random r = new Random(randomSeed);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matr[i][j] = r.nextBoolean() ? r.nextInt(maxValue) : -r.nextInt(maxValue);
            }
        }

        return matr;
    }

    private static void printMatr(MatrixS matr) {
        if (matr.M.length <= 16)
            System.out.println(matr);
        else
            System.out.println(matr.toString().substring(0, 100) + "...");
        System.out.println();
    }
}