package com.mathpar.NAUKMA.masters_2.Kyian;

import com.mathpar.matrix.MatrixD;
import com.mathpar.number.Ring;
import com.mathpar.parallel.utils.MPITransport;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.Random;

public class StrassenMultiplication {
    private static final int strassenUsageLengthLimit = 64;

    public static void main(String... args) throws MPIException, IOException, ClassNotFoundException {
        MPI.Init(args);
        final int n = 512;
        final int maxRandomValue = 100;
        int[][] a = new int[n][n];
        int[][] b = new int[n][n];
        randomizeMatrix(a, 1, maxRandomValue);
        randomizeMatrix(b, 2, maxRandomValue);

        Ring ring = Ring.ringR64xyzt;
        MatrixD aMD = new MatrixD(a, ring);
        MatrixD bMD = new MatrixD(b, ring);

        int curRank = MPI.COMM_WORLD.getRank();

        MatrixD resParallelMult = multStrassenParallel(aMD, bMD, ring);

        if (curRank == 0) {
            System.out.println("Parallel mult result:");
            printResult(resParallelMult, n);
            System.out.println("Synchronous mult result:");
            MatrixD resSyncMult = multStrassen(aMD, bMD, ring);
            printResult(resSyncMult, n);
            System.out.println("Do results equal? " + resParallelMult.equals(resSyncMult));
        }

        MPI.Finalize();
    }

    private static void printResult(MatrixD res, int n) {
        if (n <= 16)
            System.out.println(res);
        else
            System.out.println(res.toString().substring(0, 100) + "...");
        System.out.println();
    }

    private static void randomizeMatrix(int[][] a, int randomSeed, int maxValue) {
        Random r = new Random(randomSeed);
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                a[i][j] = r.nextInt(maxValue);
            }
        }
    }

    private static MatrixD multStrassen(MatrixD a, MatrixD b, Ring ring) {
        if (a.M.length <= strassenUsageLengthLimit) {
            return a.multCU(b, ring);
        }

        MatrixD[] aSplit = a.splitTo4();
        MatrixD A11 = aSplit[0];
        MatrixD A12 = aSplit[1];
        MatrixD A21 = aSplit[2];
        MatrixD A22 = aSplit[3];

        MatrixD[] bSplit = b.splitTo4();
        MatrixD B11 = bSplit[0];
        MatrixD B12 = bSplit[1];
        MatrixD B21 = bSplit[2];
        MatrixD B22 = bSplit[3];

        MatrixD P1 = multStrassen(A11.add(A22, ring), B11.add(B22, ring), ring);
        MatrixD P2 = multStrassen(A21.add(A22, ring), B11, ring);
        MatrixD P3 = multStrassen(A11, B12.subtract(B22, ring), ring);
        MatrixD P4 = multStrassen(A22, B21.subtract(B11, ring), ring);
        MatrixD P5 = multStrassen(A11.add(A12, ring), B22, ring);
        MatrixD P6 = multStrassen(A21.subtract(A11, ring), B11.add(B12, ring), ring);
        MatrixD P7 = multStrassen(A12.subtract(A22, ring), B21.add(B22, ring), ring);

        MatrixD C11 = P1.add(P4, ring).subtract(P5, ring).add(P7, ring);
        MatrixD C12 = P3.add(P5, ring);
        MatrixD C21 = P2.add(P4, ring);
        MatrixD C22 = P1.subtract(P2, ring).add(P3, ring).add(P6, ring);

        MatrixD[] res = new MatrixD[4];
        res[0] = C11;
        res[1] = C12;
        res[2] = C21;
        res[3] = C22;

        return MatrixD.joinMatr(res);
    }

    private static MatrixD multStrassenParallel(MatrixD a, MatrixD b, Ring ring) throws IOException, MPIException, ClassNotFoundException {
        if (a.M.length <= strassenUsageLengthLimit) {
            return a.multCU(b, ring);
        }

        final int curRank = MPI.COMM_WORLD.getRank();
        System.out.println("Thread " + curRank);

        MatrixD[] aSplit = a.split(a);
        MatrixD A11 = aSplit[0];
        MatrixD A12 = aSplit[1];
        MatrixD A21 = aSplit[2];
        MatrixD A22 = aSplit[3];

        MatrixD[] bSplit = b.split(b);
        MatrixD B11 = bSplit[0];
        MatrixD B12 = bSplit[1];
        MatrixD B21 = bSplit[2];
        MatrixD B22 = bSplit[3];

        switch (curRank) {
            case 0:
                MatrixD P1 = (MatrixD) MPITransport.recvObject(1, 1);
                MatrixD P2 = (MatrixD) MPITransport.recvObject(2, 2);
                MatrixD P3 = (MatrixD) MPITransport.recvObject(3, 3);
                MatrixD P4 = (MatrixD) MPITransport.recvObject(4, 4);
                MatrixD P5 = (MatrixD) MPITransport.recvObject(5, 5);
                MatrixD P6 = (MatrixD) MPITransport.recvObject(6, 6);
                MatrixD P7 = (MatrixD) MPITransport.recvObject(7, 7);

                MatrixD C11 = P1.add(P4, ring).subtract(P5, ring).add(P7, ring);
                MatrixD C12 = P3.add(P5, ring);
                MatrixD C21 = P2.add(P4, ring);
                MatrixD C22 = P1.subtract(P2, ring).add(P3, ring).add(P6, ring);

                MatrixD[] res = new MatrixD[4];
                res[0] = C11;
                res[1] = C12;
                res[2] = C21;
                res[3] = C22;

                return MatrixD.join(res);
            case 1:
                MatrixD p1 = multStrassen(A11.add(A22, ring), B11.add(B22, ring), ring);
                MPITransport.sendObject(p1, 0, curRank);
                break;
            case 2:
                MatrixD p2 = multStrassen(A21.add(A22, ring), B11, ring);
                MPITransport.sendObject(p2, 0, curRank);
                break;
            case 3:
                MatrixD p3 = multStrassen(A11, B12.subtract(B22, ring), ring);
                MPITransport.sendObject(p3, 0, curRank);
                break;
            case 4:
                MatrixD p4 = multStrassen(A22, B21.subtract(B11, ring), ring);
                MPITransport.sendObject(p4, 0, curRank);
                break;
            case 5:
                MatrixD p5 = multStrassen(A11.add(A12, ring), B22, ring);
                MPITransport.sendObject(p5, 0, curRank);
                break;
            case 6:
                MatrixD p6 = multStrassen(A21.subtract(A11, ring), B11.add(B12, ring), ring);
                MPITransport.sendObject(p6, 0, curRank);
                break;
            case 7:
                MatrixD p7 = multStrassen(A12.subtract(A22, ring), B21.add(B22, ring), ring);
                MPITransport.sendObject(p7, 0, curRank);
                break;
        }

        return null;
    }
}
