/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mathpar.NAUKMA.masters_2.Baranov;

import com.mathpar.NAUKMA.masters_2.Denysenko.SparseShtrassenMultiplication;
import com.mathpar.log.MpiLogger;
import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DropTask;
import com.mathpar.parallel.utils.MPITransport;
import java.util.ArrayList;
import java.util.Random;
import java.util.function.Supplier;
import mpi.MPI;

/**
 *
 * @author kostia
 */
public class matrSTriangInvStrass extends DropTask {
    private final static MpiLogger LOGGER = MpiLogger.getLogger(SparseShtrassenMultiplication.class);
    
    private static int leafSize = 4;

    private final static int[][] _arcs = {
            {       3, 0, 0,    4, 1, 0,    2, 2, 1,    5, 3, 1,
                    1, 4, 0,    1, 5, 1,    2, 6, 0,    3, 7, 1,
                    4, 8, 1,    5, 9, 0,    6, 10, 0,   6, 11, 1,
                    7, 12, 0,   7, 13, 1},
            {8, 0, 0},
            {8, 0, 1},
            {8, 0, 2},
            {8, 0, 3},
            {}
    };
    
    private static final int RAND_SEED = 13;
    private static final int RAND_MAX_VALUE = 9;
    private static final Ring RING = Ring.ringZxyz;
    private static final int NIL_PARENT = -1;
    
    public static void main(String args[]) throws Exception {
        MPI.Init(args);
        int m_size = Integer.parseInt(args[0]);
        double density = Double.parseDouble(args[1]);
        
        int[][] m_rand = new int[m_size][m_size];
        Utils.randTriangleMatrix(m_rand, density);
        
        MatrixS m = new MatrixS(m_rand, RING);
        
        int cores = MPI.COMM_WORLD.getSize();
        Supplier<MatrixS> inverse = () -> cores == 1
            ? Utils.sequenceInverse(m)
            : distributedInverse(m);
        
        long start = System.currentTimeMillis();
        inverse.get();
        long end = System.currentTimeMillis();
        
        int myRank = MPI.COMM_WORLD.getRank();
        if (myRank == 0) {
            System.out.println("DONE [time: " + (end - start) + "]");
        }
        
        MPI.Finalize();
    }

    @Override
    public ArrayList<DropTask> doAmin() {
        ArrayList<DropTask> amin = new ArrayList<>();
        amin.add(new matrSTriangInvStrass());
        amin.add(new matrSTriangInvStrass());
        amin.add(new matrSTriangInvStrass());
        amin.add(new SparseShtrassenMultiplication());
        amin.add(new SparseShtrassenMultiplication());
        return amin;
    }

    @Override
    public void sequentialCalc(Ring r) {
        Element a = inData[0];
        Element b = inData[1];
        outData[0] = a.multiply(b, r);
    }

    @Override
    public MatrixS[] inputFunction(Element[] input, int inputKey, Ring r) {
        MatrixS m = (MatrixS) input[0];

        MatrixS[] splits = m.split();
        MatrixS a = splits[0];
        MatrixS zeros = splits[1];
        MatrixS b = splits[2];
        MatrixS c = splits[3];

        MatrixS[] inputMatr = new MatrixS[14];
        inputMatr[0] = a;
        inputMatr[1] = b;
        inputMatr[2] = zeros;
        inputMatr[3] = c;
        inputMatr[4] = c.negate(r).multiply(b, r).multiply(a, r);

        return inputMatr;
    }

    @Override
    public MatrixS[] outputFunction(Element[] input, Ring r) {
        MatrixS a_inv = ((MatrixS)input[0]).inverseInFractions(r);
        MatrixS zeros = (MatrixS)input[2];
        MatrixS c_inv = ((MatrixS)input[3]).inverseInFractions(r);
        MatrixS b_inv = c_inv
                .negate(r)
                .multiply((MatrixS)input[1], r)
                .multiply(a_inv, r);

        return new MatrixS[] { MatrixS.join(new MatrixS[] {
            a_inv, zeros,
            b_inv, c_inv
        })};
    }

    @Override
    public boolean isItLeaf() {
        return ((MatrixS) inData[0]).M.length <= leafSize;
    }

    @Override
    public void setLeafSize(int dataSize) {
        leafSize = dataSize;
    }
    
    public static MatrixS distributedInverse(MatrixS m) {
        try {
            int cores = MPI.COMM_WORLD.getSize();
            int rangeStart = 0;
            int rangeEnd = cores - 1;
            int iterationDepth = 0;
            return distributedInverse(m, rangeStart, rangeEnd, iterationDepth, NIL_PARENT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    
    private static MatrixS distributedInverse(MatrixS m, int rangeStart, int rangeEnd, int depth, int parent) throws Exception {
        int myRank = MPI.COMM_WORLD.getRank();
        if (rangeStart == rangeEnd && myRank == rangeStart) {
            MatrixS linearResult = Utils.inverse(m);
            if (myRank == parent) {
                return linearResult;
            }
            MPITransport.sendObject(linearResult, parent, depth);
        } else {
            int oneHalfEnd = rangeStart + (rangeEnd - rangeStart) / 2;
            int twoHalfStart = oneHalfEnd + 1;
            int nextDepth = depth + 1;
            int iterationMain = rangeStart;
            
            MatrixS[] splits = m.split();
            MatrixS a = splits[0];
            MatrixS b = splits[2];
            MatrixS c = splits[3];
            MatrixS zeros = splits[1];
            
            MatrixS a_inv_imm = null;
            
            if (myRank <= oneHalfEnd) {
                a_inv_imm = distributedInverse(a, rangeStart, oneHalfEnd, nextDepth, iterationMain);
            } else {
                distributedInverse(c, twoHalfStart, rangeEnd, nextDepth, iterationMain);
            }
            
            if (myRank == iterationMain) {
                MatrixS a_inv = a_inv_imm;
                MatrixS c_inv = (MatrixS) MPITransport.recvObject(twoHalfStart, nextDepth);
                MatrixS b_inv = c_inv
                        .negate(RING)
                        .multiply(b, RING)
                        .multiply(a_inv, RING);
                
                MatrixS concurrentResult = MatrixS.join(new MatrixS[] {
                    a_inv, zeros,
                    b_inv, c_inv
                });
                
                if (parent == myRank || parent == NIL_PARENT) {
                    return concurrentResult;
                } else {
                    MPITransport.sendObject(concurrentResult, parent, depth);
                }
            }
        }
        return null;
    }
    
    private static class Utils {
        
        static MatrixS inverse(MatrixS m) {
            return m.inverseInFractions(RING);
        }
        
        static MatrixS sequenceInverse(MatrixS m) {
            MatrixS[] splits = m.split();
            MatrixS a = splits[0];
            MatrixS b = splits[2];
            MatrixS c = splits[3];
            MatrixS zeros = splits[1];
            
            MatrixS a_inv = Utils.inverse(a);
            MatrixS c_inv = Utils.inverse(c);
            MatrixS b_inv = c_inv
                .negate(RING)
                .multiply(b, RING)
                .multiply(a_inv, RING);
            return MatrixS.join(new MatrixS[] {
                a_inv, zeros,
                b_inv, c_inv
            });
        }
        
        static void randTriangleMatrix(int[][] a, double density) {
            Random r = new Random(RAND_SEED);
            for (int i = 0; i < a.length; i++) {
                int incI = i + 1;
                for (int j = 0; j < incI; j++) {
                    if (i != j) {
                        a[i][j] = r.nextDouble() < density
                            ? r.nextInt(RAND_MAX_VALUE)
                            : 0;
                    } else {
                        a[i][j] = r.nextInt(RAND_MAX_VALUE - 1) + 1;
                    }
                }
            }
        }
        
        static void printMatrix(MatrixS m) {
            for (int i = 0; i < m.size; i++) {
                for (int j = 0; j < m.size; j++) {
                    System.out.print(m.getElement(i, j, RING).toString());
                    System.out.print(" ");
                }
                System.out.println("");
            }
        }
        
    }
    
}
