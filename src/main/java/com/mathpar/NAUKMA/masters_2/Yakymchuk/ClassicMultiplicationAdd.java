package com.mathpar.NAUKMA.masters_2.Yakymchuk;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Array;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;

public class ClassicMultiplicationAdd extends ClassicMultiplication
{
    public static int Drop_Number = 16;
    private static int inputMatrixSize = 9;

    private static int[][] Matrix_Arcs = new int[][]
            {
                    {
                            1, 0, 0,   1, 4, 1,   2, 1, 0,   2, 6, 1,   3, 0, 0,   3, 5, 1,   4, 1, 0,   4, 7, 1,
                            5, 2, 0,   5, 4, 1,   6, 3, 0,   6, 6, 1,   7, 2, 0,   7, 5, 1,   8, 3, 0,   8, 7, 1,
                            9, 8, 4
                    },
                    {2, 0, 2}, {9, 0, 0}, {4, 0, 2}, {9, 0, 1},
                    {6, 0, 2}, {9, 0, 2}, {8, 0, 2}, {9, 0, 3},
                    {}
            };

    public ClassicMultiplicationAdd()
    {
        inData = new Element[3];
        outData = new Element[1];

        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 16;

        number = cnum++;
        type = Drop_Number;

        resultForOutFunctionLength = 5;
        inputDataLength = 3;

        arcs = Matrix_Arcs;
    }

    @Override
    public void sequentialCalc(Ring ring)
    {
        outData[0] = inData[0].multiply(inData[1], ring).add(inData[2], ring);
    }
    @Override
    public Element[] inputFunction(Element[] input, int inputKey, Ring ring)
    {
        MatrixS[] c = new MatrixS[inputMatrixSize];
        if (inputKey == 3)
        {
            c[8] = (MatrixS) input[0];
        }
        else
        {
            MatrixS [] a = ((MatrixS) input[0]).splitExtended();
            MatrixS [] b = ((MatrixS) input[1]).splitExtended();

            Array.concatTwoArrays(a, b, c);

            if (inputKey == 2)
            {
                c[8] = null;
            }
            if (inputKey == 1)
            {
                c[8] = (MatrixS) input[2];
            }
        }
        return c;
    }

    @Override
    public Element[] outputFunction(Element[] input, Ring ring)
    {
        MatrixS[] c = new MatrixS[input.length];
        for (int i = 0; i < input.length; i++)
        {
            c[i] = (MatrixS) input[i];
        }

        return new MatrixS[] {MatrixS.join(c).add((MatrixS)input[4], ring)};
    }
}