package com.mathpar.NAUKMA.masters_2.Yakymchuk;

import com.mathpar.matrix.MatrixS;
import com.mathpar.number.Array;
import com.mathpar.number.Element;
import com.mathpar.number.Ring;
import com.mathpar.parallel.dap.core.DispThread;
import com.mathpar.parallel.dap.core.DropTask;
import mpi.MPI;
import mpi.MPIException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class ClassicMultiplication extends DropTask
{
    public static int Drop_Number = 15;
    private static int Leaf_Size = 4;
    private static int Matrix_Size = 16;

    private static int Input_Matrix_Size = 8;
    private static int Max_Random_Value = 5;

    private static int[][] Matrix_Arcs = new int[][]
            {
                    {
                            1, 0, 0,   1, 4, 1,   2, 1, 0,   2, 6, 1,   3, 0, 0,   3, 5, 1,   4, 1, 0,   4, 7, 1,
                            5, 2, 0,   5, 4, 1,   6, 3, 0,   6, 6, 1,   7, 2, 0,   7, 5, 1,   8, 3, 0,   8, 7, 1
                    },
                    {2, 0, 2},{9, 0, 0},{4, 0, 2},{9, 0, 1},
                    {6, 0, 2},{9, 0, 2},{8, 0, 2},{9, 0, 3},
                    {}
            };

    public ClassicMultiplication()
    {
        inData = new Element[2];
        outData = new Element[1];

        numberOfMainComponents = 2;
        numberOfMainComponentsAtOutput = 16;

        number = cnum++;
        type = Drop_Number;

        resultForOutFunctionLength = 4;
        inputDataLength = 2;

        arcs = Matrix_Arcs;
    }


    @Override
    public Element[] inputFunction(Element[] input, int inputKey, Ring ring)
    {
        MatrixS[] c = new MatrixS[Input_Matrix_Size];
        MatrixS[] a = ((MatrixS) input[0]).splitExtended();
        MatrixS[] b = ((MatrixS) input[1]).splitExtended();
        Array.concatTwoArrays(a, b, c);
        return c;
    }

    @Override
    public Element[] outputFunction(Element[] input, Ring ring)
    {
        MatrixS[] c = new MatrixS[input.length];
        for (int a = 0; a < input.length; a++)
        {
            c[a] = (MatrixS) input[a];
        }

        return new MatrixS[] {MatrixS.join(c)};
    }

    @Override
    public boolean isItLeaf() {
        return ((MatrixS)inData[0]).M.length <= Leaf_Size;
    }

    @Override
    public ArrayList<DropTask> doAmin()
    {
        ArrayList<DropTask> amin = new ArrayList<DropTask>();

        amin.add(new ClassicMultiplication());
        amin.add(new ClassicMultiplicationAdd());
        amin.add(new ClassicMultiplication());
        amin.add(new ClassicMultiplicationAdd());
        amin.add(new ClassicMultiplication());
        amin.add(new ClassicMultiplicationAdd());
        amin.add(new ClassicMultiplication());
        amin.add(new ClassicMultiplicationAdd());

        return amin;
    }

    @Override
    public void sequentialCalc(Ring ring)
    {
        outData[0] = inData[0].multiply(inData[1], ring);
    }


    @Override
    public void setLeafSize(int dataSize)
    {
        Leaf_Size = dataSize;
    }

    public static void main(String[] args) throws MPIException, InterruptedException, ClassNotFoundException, IOException
    {
        MPI.Init(args);
        int rank = MPI.COMM_WORLD.getRank();

        Ring ring = new Ring("R[]");
        MatrixS matrix_1 = new MatrixS(GenerateMatrix(Matrix_Size, 1, Max_Random_Value), ring);
        MatrixS matrix_2 = new MatrixS(GenerateMatrix(Matrix_Size, 2, Max_Random_Value), ring);
        DispThread d_Thread = new DispThread(1, args, ring);

        if (rank == 0) {
            System.out.println("Matrix 1 = ");
            System.out.println(matrix_1);
            System.out.println();

            System.out.println("Matrix 2 = ");
            System.out.println(matrix_2);
            System.out.println();
        }

        Element[] input = new MatrixS[]{ matrix_1, matrix_2 };
        d_Thread.execute(Drop_Number, args, input, ring);

        if (rank == 0)
        {
            MatrixS result = (MatrixS) d_Thread.getResult()[0];
            System.out.println();
            System.out.println("Result Matrix =");
            System.out.println(result);
            System.out.println();
        }

        d_Thread.counter.DoneThread();
        d_Thread.counter.thread.join();
        MPI.Finalize();
    }

    private static int[][] GenerateMatrix(int m_size, int seed, int maxValue)
    {
        int[][] new_matrix = new int[m_size][m_size];
        Random rand = new Random(seed);
        for (int i = 0; i < m_size; i++)
        {
            for(int j = 0; j < m_size; j++)
            {
                new_matrix[i][j] = rand.nextInt(maxValue);
            }
        }

        return new_matrix;
    }

}