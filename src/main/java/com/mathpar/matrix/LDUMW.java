
package com.mathpar.matrix;
import java.util.Random;
import com.mathpar.number.Element;
import com.mathpar.number.Fraction;
import com.mathpar.number.Ring;

import static com.mathpar.matrix.LDUMW.invForD;

public class LDUMW {
    int n; // size of matrix
    MatrixS L;
    MatrixS D; // ==Ddenom   new sense of this matrix! (denom-of-each-elems)
    MatrixS Dhat;  // d^{-1}=a_n*Dhat; (It is  D=Denom + (I block))
    MatrixS Dbar;// Dhat-D
    MatrixS U;
    MatrixS M;
    MatrixS W;
    MatrixS I;
    MatrixS Ibar;
    MatrixS J;
    MatrixS Jbar;    
    Element a_n; // determinant 
     
    public LDUMW(MatrixS T){
        n = T.size;
        L = new MatrixS();
        D = new MatrixS();
        U = new MatrixS();
        M = new MatrixS();
        Dhat = new MatrixS();
        Dbar = new MatrixS();
        W = new MatrixS();
        I = new MatrixS();
        J = new MatrixS();
    };
    
    
        
    /** LDUMW -- are such factors of A and A^{+}:  A=LDU, A^{+}= WDM;
     *  All the elemenys of A,L,D,U,M,W are integer numbers
     * @param A - is a matrix  for the decomposition
     * @param ring Ring
     * @return  MatrixS[] {L,U,M,W } -resulting multipliers     
     */
   public static MatrixS[] LDUMW(MatrixS A, Ring ring){
            int r=Math.min(A.size,A.colNumb);  Boolean flag;
            int s=Math.max(A.size,A.colNumb); int b=s;
            for (int i = 0; i < s; i++) {b=b>>1; if (b==0){b=1<<i; break;}}
            if(b!=A.size){flag=false;b=b<<1;}else {flag=true;}
           s= A.size; int clmn= A.colNumb; 
            A.size=b; A.colNumb=b;
        LDUMW FF = new LDUMW(A); FF.getLDU(A, ring.numberONE, ring);
        MatrixS M=FF.M.divideByNumber(FF.a_n, ring);
        MatrixS W=FF.W.divideByNumber(FF.a_n, ring);
        if(!flag){ FF.L.size=s; FF.L.colNumb=r;  FF.D.size=r; FF.D.colNumb=r;FF.U.size=r; FF.U.colNumb=clmn; 
           M.size=s; M.colNumb=s;  W.size=s; W.colNumb=s; }
        return new MatrixS[]{FF.L, invForD(FF.D, ring), FF.U, M, W};
        }   
        
             
    /** Factors of GenInverse Matrix
     * Factors of  A^{+} are tree matrices:  A^{+}= WDM;
     *  All the elemenys of A,L,D,U,M,W are integer numbers
     * @param A - is a matrix  for the decomposition
     * @param ring Ring
     * @return  MatrixS[] { W , D, M} -resulting multipliers     
     */
       public static MatrixS[] GenInverseFactors(MatrixS A, Ring ring){
            MatrixS[] res=LDUMW(A, ring);
        return new MatrixS[]{res[4], res[1],res[3]};
        }  
      public static MatrixS GenInverse(MatrixS A, Ring ring){
            MatrixS[] res=LDUMW(A, ring);
        return  res[4].multiply(res[1], ring).multiply(res[3], ring);
        } 
      
      public static MatrixS[] LDU(MatrixS A, Ring ring){
            int r=Math.min(A.size,A.colNumb);  Boolean flag;
            int s=Math.max(A.size,A.colNumb); int b=s;
            for (int i = 0; i < s; i++) {b=b>>1; if (b==0){b=1<<i; break;}}
            if(b!=A.size){flag=false;b=b<<1;}else {flag=true;}
           s= A.size; int clmn= A.colNumb; 
            A.size=b; A.colNumb=b;
        LDUMW FF = new LDUMW(A); FF.getLDU(A, ring.numberONE, ring);
        if(!flag){FF.L.size=s; FF.L.colNumb=r;  FF.D.size=r; FF.D.colNumb=r;FF.U.size=r; FF.U.colNumb=clmn; }
        return new MatrixS[]{FF.L, invForD(FF.D, ring), FF.U };
      }
    
        
      
    
    public void getLDU(MatrixS T, Element a,   Ring ring){
        Element ONE=ring.numberONE;
    if (T.isZero(ring)){
                D = MatrixS.zeroMatrix(n); 
                L = MatrixS.scalarMatrix(n, ONE, ring);
                U = MatrixS.scalarMatrix(n, ONE, ring); 
                M = MatrixS.scalarMatrix(n, a, ring);
                W = MatrixS.scalarMatrix(n, a, ring); 
                Dhat=MatrixS.scalarMatrix(n, a, ring);
                a_n = a;  
                Dbar=MatrixS.scalarMatrix(n, ONE, ring); 
                I=MatrixS.zeroMatrix(n);
                J=MatrixS.zeroMatrix(n);
                Jbar=MatrixS.scalarMatrix(n, ONE, ring);           
                Ibar=MatrixS.scalarMatrix(n, ONE, ring);
                return;
        }
        if(n==1){
                a_n = T.getElement(0, 0, ring);
                Element aan=a_n.multiply(a, ring);
                Element an_an=a_n.multiply(a_n, ring);  
                L = new MatrixS(a_n);  
                D = new MatrixS(aan);  
                Dhat=new MatrixS(an_an);
                Dbar = MatrixS.zeroMatrix(n);             
                U = new MatrixS(a_n);  
                M = new MatrixS(a_n);
                W = new MatrixS(a_n);
                Jbar=Dbar;
                Ibar=Dbar;
                I=new MatrixS(ONE);
                J=I;
                return;
        }
        MatrixS[] A = T.split();
        MatrixS A11 = A[0];
        MatrixS A12 = A[1];
        MatrixS A21 = A[2];
        MatrixS A22 = A[3];
 
  
        LDUMW F11 = new LDUMW(A11);       
        F11.getLDU(A11,a,ring); 
     
         Element ak=F11.a_n;
         Element ak2=ak.multiply(ak, ring);
    
      MatrixS invD11hat= invForD(F11.Dhat, ring);
      MatrixS A12_0= F11.M.multiply(A12, ring); 
      MatrixS A12_1= invD11hat.multiplyByNumber(ak, ring).multiply(A12_0, ring);
      MatrixS A21_0=A21.multiply(F11.W,ring);  
 
 
     MatrixS A21_1 =A21_0.multiplyByNumber(ak, ring).multiply(invD11hat, ring); 
     MatrixS A12_2=F11.Dbar.multiply(A12_0, ring).divideByNumber(a, ring); 
     MatrixS A21_2= A21_0.multiply(F11.Dbar, ring).divideByNumber(a, ring);
     LDUMW F21 = new LDUMW(A21_2);     
         F21.getLDU(A21_2,  ak , ring);
     Element al=F21.a_n; 
     LDUMW F12 = new LDUMW(A12_2);
        F12.getLDU(A12_2, ak , ring); 
     Element am= F12.a_n;
  
     Element lambda= al.divideToFraction(ak, ring); 
     Element as=lambda.multiply(am, ring);  
     MatrixS  D11PLUS=F11.D.transpose(); 

     MatrixS A22_0= A21_1.multiply(D11PLUS.multiply(A12_1, ring), ring);
     MatrixS A22_1= (A22.multiplyByNumber(ak2, ring).multiplyByNumber(a, ring) // 21-10-2020  .divideByNumber(a, ring)
         .subtract(A22_0,ring)).divideByNumber(ak, ring).divideByNumber(a, ring);
 
     MatrixS  A22_2=(F21.Dbar.multiply(F21.M, ring)).multiply(A22_1, ring);
              A22_2=A22_2.multiply(F12.W.multiply(F12.Dbar, ring), ring);  
     MatrixS A22_3=A22_2.divideByNumber(ak2, ring).divideByNumber(a, ring);      
        //====================================4========================
     LDUMW F22 = new LDUMW(A22_3);
     F22.getLDU(A22_3, as, ring); 
     a_n=F22.a_n;         
  
     MatrixS J12lambda=(F12.J.multiplyByNumber(lambda, ring)).add(F12.Jbar, ring);
     MatrixS I12lambda=(F12.I.multiplyByNumber(lambda, ring)).add(F12.Ibar, ring);
     MatrixS L12tilde=F12.L.multiply(I12lambda, ring);
     MatrixS U12tilde= J12lambda.multiply(F12.U, ring);        
     Element lambda2=lambda.multiply(lambda, ring);
   
     MatrixS U2= (F11.J.multiply(F11.M, ring)).multiply(A12, ring);  
             U2= U2.divideByNumber(ak, ring); 
     MatrixS U2H= F21.J.multiply(F21.M, ring).multiply(A22_1,ring);
             U2H=U2H.divideByNumber(al, ring).divideByNumber(a, ring); 
             U2= U2.add(U2H, ring);
     MatrixS L3H2= (A22_1.multiply(F12.W.multiply(F12.I, ring), ring)); 
     MatrixS L3H1= F21.Dbar.multiply(F21.M, ring).multiply(L3H2, ring);
             L3H1= L3H1.divideByNumber(am, ring)
                .divideByNumber(ak, ring).divideByNumber(a, ring);
     MatrixS L3= (A21.multiply(F11.W.multiply(F11.I, ring), ring));        
             L3= (L3.divideByNumber(ak, ring)).add(L3H1, ring);
     MatrixS[] LL=new MatrixS[]{F11.L.multiply(L12tilde, ring),
                  MatrixS.zeroMatrix(), L3, F21.L.multiply(F22.L, ring) }; 
     L=MatrixS.join(LL);
           
     MatrixS[] UU=new MatrixS[]{F21.U.multiply(F11.U, ring), U2,
                  MatrixS.zeroMatrix(), F22.U.multiply(U12tilde, ring) }; 
     U=MatrixS.join(UU);          
     D=MatrixS.join(new MatrixS[]{F11.D,  
               F12.D.multiplyByNumber(lambda2, ring), F21.D, F22.D});
     IJMap(a,ring);
     Element[] LDet = new Element[]{ring.numberONE};
     MatrixS Linv =L.adjointDet(LDet, ring);
     Element[] UDet = new Element[]{ring.numberONE};
     MatrixS Uinv =U.adjointDet(UDet, ring);
     MatrixS dInverse= Dhat.transpose(); 
     M=(dInverse.multiply(Linv, ring))
            .divideByNumbertoFraction(LDet[0], ring);
     W=(Uinv.multiply(dInverse, ring))
            .divideByNumbertoFraction(UDet[0], ring);
    }; 
    
 
    /**Using matrix D we are constructed I, J, Ibar, Jbar, Dhat.
     * 
     * @param ring  - Ring
     */
    void IJMap (Element a, Ring ring){ MatrixS A=D;
    Element[][] MI=new Element[A.M.length][];
    Element[] one = new Element[]{ring.numberONE};
    Element[] zero = new Element[0];
    int[] zeroI = new int[0];
    for (int i = 0; i<A.M.length; i++){MI[i]= (A.M[i].length>0)? one: zero;}
    int[][] colI = new int[A.M.length][];
    int maxCol=0, maxColOut=0;
    for (int i = 0; i<A.col.length; i++){
        if (A.col[i].length>0){ colI[i]=new int[]{i}; 
              maxColOut=Math.max (maxCol, i );  
              MI[i]=one; maxCol=Math.max (maxCol, A.col[i][0]);}
        else {colI[i]=zeroI; MI[i]=zero;}
    };   maxCol++; maxColOut++;int mmax=Math.max (maxCol,maxColOut);
    Element[][] MJ=new Element[maxCol][0];
    int[][] colJ = new int[maxCol][0];
    for (int i = 0; i<A.col.length; i++){
        if (A.col[i].length>0){  int cc=A.col[i][0];  
          colJ[cc]=new int[]{cc}; MJ[cc]=one;} }
    I=new MatrixS(A.size, mmax,  MI, colI);
    J=new MatrixS(A.size, mmax,  MJ, colJ);
    Ibar=makeIbar(I, ring); 
    Jbar=makeIbar(J, ring);
     maxCol=Math.max(maxCol, A.col.length);
    Element[][] Md=new Element[maxCol][];
    int[][] cold = new int[maxCol][];
    Element[][] MdB=new Element[maxCol][0];
    int[][] coldB = new int[maxCol][0];
    System.arraycopy(A.col, 0, cold, 0, A.col.length);
    for (int i = 0; i<A.M.length; i++) if(A.M[i].length>0){
        Md[i]=new Element[]{A.M[i][0]
                .multiply(a_n, ring).divideToFraction(a, ring)};    
    }
    if (!Ibar.isZero(ring)){  int maxColN=0;
      int j=0; while(Jbar.col[j].length==0)j++;
      for (int i = 0; i<Ibar.col.length; i++){
        if (Ibar.col[i].length>0){ cold[i]=new int[]{j}; 
          Md[i]=new Element[]{a_n};
          coldB[i]=new int[]{j}; 
          MdB[i]=new Element[]{ring.numberONE};
          j++; maxColN=j;
          while((j<Jbar.col.length)&&(Jbar.col[j].length==0)) j++;}
     }  
       Dbar=new MatrixS(A.size, maxColN, MdB, coldB);
    } else Dbar=MatrixS.zeroMatrix(A.size); 
    Dhat=new MatrixS(A.size, maxCol, Md, cold); 
  }
    /** It makes Ibar from I
     * 
     * @param II - MatrixS I
     * @param ring - Ring
     * @return  Ibar
     */
public static MatrixS  makeIbar(MatrixS II, Ring ring){ int colNumb=0;
    int len=(II.M.length<II.size)? II.size:II.M.length;
    Element[][] MI=new Element[len][];
    int[][] colI=new int[len][];
    Element[] one = new Element[]{ring.numberONE};
    Element[] zero = new Element[0];
    int[] zeroI = new int[0];
    int i = 0;
    for (; i<II.M.length; i++){
        if(II.col[i].length>0){MI[i]= zero; colI[i]=zeroI;} 
    else {MI[i]= one; colI[i]=new int[]{i};colNumb=i; }
    }
    for (; i<II.size; i++){MI[i]= one; colI[i]=new int[]{i}; colNumb=i; }
    return new MatrixS(II.size, colNumb+1, MI, colI);
}   
    
/** Приводим D-матрицу из "компактной формы" к стандартной
 * (Знаменатели хранились как целые числа, теперь их заменят дроби,
 * и эти целые числа пойдут в знаменатель)
 * нулевая матрица вернет нулевую!
 * @param Di - компактная диагональная матрица
 * @return диагональная матрица в обычном виде
 */
   static MatrixS invForD(MatrixS Di, Ring ring) {
    int len=Di.M.length;
    Element[][] MI=new Element[len][];
    for (int i = 0; i<len; i++){
        if(Di.col[i].length>0){ Element ee=Di.M[i][0]; Element  enew;
         if(ee.isNegative()){enew=(ee.isMinusOne(ring))?ee:
             new Fraction(ring.numberMINUS_ONE, ee.negate(ring));  }
         else{ enew=(ee.isOne(ring))?ee:
             new Fraction(ring.numberONE, ee);  }
         MI[i]=new Element[]{enew};
        }else {MI[i]=Di.M[i];}
    }
    return new MatrixS(Di.size, Di.colNumb, MI, Di.col);     
    }
   
 
    
 // *********************************************
    public static void main(String[] args) {
   
       Ring ring=new Ring("Q[]");
  int[][] qq=new int   [][]  {{4, 0, 2, 0, 0, 7, 0},
 {0, 0, 6, 5, 4, 0, 0},
 {4, 0, 4, 3, 0, 0, 2},
 {0, 0, 0, 0, 0, 5, 0},
 {0, 0, 1, 0, 0, 3, 0},
 {0, 0, 0, 0, 0, 0, 0},
 {0, 2, 0, 2, 0, 0, 0}};
       
       int r=7; int c=7; int density=40;
       int[] randomType= new int[]{3}; 
       boolean good=true; int w=1;
    while ((w<2)  && (good)) {w++;
       MatrixS tmp =     new MatrixS(qq, ring);
       //      new MatrixS( r, c, density, randomType, new  Random(),ring.numberONE, ring);
        System.out.println("tmp="+tmp); 
     //  ring=new Ring("R[]");
        MatrixS[] res=LDUMW(tmp,ring);
        MatrixS L=res[0]; MatrixS D=res[1]; 
        MatrixS U=res[2];  MatrixS M=res[3]; MatrixS W=res[4];
        System.out.println("L="+L);
        System.out.println("D="+D);
        System.out.println("U="+U);
        System.out.println("M="+M);
        System.out.println("W="+W);
        MatrixS B =L.multiply(D, ring).multiply(U, ring);
        MatrixS CheckZero= tmp.subtract(B,ring);
        MatrixS GI=W.multiply(D, ring).multiply(M, ring) ;
        MatrixS EE= tmp.multiply(GI, ring).multiply(tmp, ring).subtract(tmp, ring);

          if (CheckZero.isZero(ring)&& EE.isZero(ring)) { good=true;  
              System.out.println("All VERY GOOD !!!!!!!!!!!!!!!!");} 
         else  { System.out.println(" ??????????????"); good=false; }
       System.out.println(  tmp.multiply(GenInverse(tmp,ring).multiply(tmp, ring), ring) );
       MatrixS[] www=GenInverseFactors(tmp,ring);
        System.out.println("WDM="+www[0]+www[1]+www[2]);
        www=LDU(tmp,ring);
        System.out.println("LDU="+www[0]+www[1]+www[2]);
    }}
}

/*[[1, -2, 10,  28,   240,  0, 0 ]
 [0, 0,  0,   -336, -320, 0, 32]
 [0, 4,  -20, -280, -480, 0, 0 ]
 [0, 0,  24,  336,  320,  0, 0 ]
 [0, 0,  0,   0,    320,  0, 0 ]
 [0, 0,  0,   64,   0,    0, 0 ]
 [0, 0,  0,   0,    0,    1, 0 ]] */
  /*     */